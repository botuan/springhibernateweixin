package cn.com.weixin.sdk.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.com.weixin.sdk.api.ApiConfig;
import cn.com.weixin.sdk.msg.in.InImageMsg;
import cn.com.weixin.sdk.msg.in.InLinkMsg;
import cn.com.weixin.sdk.msg.in.InLocationMsg;
import cn.com.weixin.sdk.msg.in.InShortVideoMsg;
import cn.com.weixin.sdk.msg.in.InTextMsg;
import cn.com.weixin.sdk.msg.in.InVideoMsg;
import cn.com.weixin.sdk.msg.in.InVoiceMsg;
import cn.com.weixin.sdk.msg.in.event.InCustomEvent;
import cn.com.weixin.sdk.msg.in.event.InFollowEvent;
import cn.com.weixin.sdk.msg.in.event.InLocationEvent;
import cn.com.weixin.sdk.msg.in.event.InMassEvent;
import cn.com.weixin.sdk.msg.in.event.InMenuEvent;
import cn.com.weixin.sdk.msg.in.event.InPoiCheckNotifyEvent;
import cn.com.weixin.sdk.msg.in.event.InQrCodeEvent;
import cn.com.weixin.sdk.msg.in.event.InShakearoundUserShakeEvent;
import cn.com.weixin.sdk.msg.in.event.InTemplateMsgEvent;
import cn.com.weixin.sdk.msg.in.event.InVerifyFailEvent;
import cn.com.weixin.sdk.msg.in.event.InVerifySuccessEvent;
import cn.com.weixin.sdk.msg.in.event.InWifiEvent;
import cn.com.weixin.sdk.msg.in.speech_recognition.InSpeechRecognitionResults;
import cn.com.weixin.sdk.msg.out.OutMsg;

public interface WxMsgService {

	public void render(OutMsg outMsg, String contentType,
			HttpServletRequest request, HttpServletResponse response);

	public void render(String text, HttpServletResponse response);

	boolean checkSignature(HttpServletRequest request,
			HttpServletResponse response);

	void configServer(HttpServletRequest request, HttpServletResponse response);

	void dispacth(HttpServletRequest request, HttpServletResponse response);

	// 处理接收到的文本消息
	OutMsg processInTextMsg(InTextMsg inTextMsg);

	// 处理接收到的图片消息
	OutMsg processInImageMsg(InImageMsg inImageMsg);

	// 处理接收到的语音消息
	OutMsg processInVoiceMsg(InVoiceMsg inVoiceMsg);

	// 处理接收到的视频消息
	OutMsg processInVideoMsg(InVideoMsg inVideoMsg);

	// 处理接收到的视频消息
	OutMsg processInShortVideoMsg(InShortVideoMsg inShortVideoMsg);

	// 处理接收到的地址位置消息
	OutMsg processInLocationMsg(InLocationMsg inLocationMsg);

	// 处理接收到的链接消息
	OutMsg processInLinkMsg(InLinkMsg inLinkMsg);

	// 处理接收到的多客服管理事件
	OutMsg processInCustomEvent(InCustomEvent inCustomEvent);

	// 处理接收到的关注/取消关注事件
	OutMsg processInFollowEvent(InFollowEvent inFollowEvent);

	// 处理接收到的扫描带参数二维码事件
	OutMsg processInQrCodeEvent(InQrCodeEvent inQrCodeEvent);

	// 处理接收到的上报地理位置事件
	OutMsg processInLocationEvent(InLocationEvent inLocationEvent);

	// 处理接收到的群发任务结束时通知事件
	OutMsg processInMassEvent(InMassEvent inMassEvent);

	// 处理接收到的自定义菜单事件
	OutMsg processInMenuEvent(InMenuEvent inMenuEvent);

	// 处理接收到的语音识别结果
	OutMsg processInSpeechRecognitionResults(
			InSpeechRecognitionResults inSpeechRecognitionResults);

	// 处理接收到的模板消息是否送达成功通知事件
	OutMsg processInTemplateMsgEvent(InTemplateMsgEvent inTemplateMsgEvent);

	// 处理微信摇一摇事件
	OutMsg processInShakearoundUserShakeEvent(
			InShakearoundUserShakeEvent inShakearoundUserShakeEvent);

	// 资质认证成功 || 名称认证成功 || 年审通知 || 认证过期失效通知
	OutMsg processInVerifySuccessEvent(InVerifySuccessEvent inVerifySuccessEvent);

	// 资质认证失败 || 名称认证失败
	OutMsg processInVerifyFailEvent(InVerifyFailEvent inVerifyFailEvent);

	// 门店在审核事件消息
	OutMsg processInPoiCheckNotifyEvent(
			InPoiCheckNotifyEvent inPoiCheckNotifyEvent);

	// WIFI连网后下发消息 by unas at 2016-1-29
	OutMsg processInWifiEvent(InWifiEvent inWifiEvent);
}
