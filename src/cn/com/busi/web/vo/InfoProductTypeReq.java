package cn.com.busi.web.vo;

import cn.com.base.sys.common.ReqModel;
import cn.com.busi.model.InfoProductType;

public class InfoProductTypeReq extends ReqModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private InfoProductType infoProductType;
	private InfoProductTypeRS infoProductTypeRS;
	public InfoProductType getInfoProductType() {
		return infoProductType;
	}
	public void setInfoProductType(InfoProductType infoProductType) {
		this.infoProductType = infoProductType;
	}
	public InfoProductTypeRS getInfoProductTypeRS() {
		return infoProductTypeRS;
	}
	public void setInfoProductTypeRS(InfoProductTypeRS infoProductTypeRS) {
		this.infoProductTypeRS = infoProductTypeRS;
	}
	

}
