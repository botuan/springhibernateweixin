package cn.com.base.sys.web.ctl;

import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.com.base.sys.model.BusinessCode;
import cn.com.base.sys.service.BusinessCodeService;
import cn.com.base.sys.service.CodeInputService;
import cn.com.base.sys.web.vo.BusinessCodeRS;

@Controller
public class CodeInputCtl {
	
	static Logger logger=Logger.getLogger(CodeInputCtl.class);
	private Random rand=new Random(100);
	@Autowired
	private BusinessCodeService businessCodeService;
	@RequestMapping("/codeInput")
	public  String getMenu(HttpServletRequest request,HttpServletResponse response, ModelMap modelMap) { 
		String codeType=request.getParameter("codeType");
		System.out.println("codeType=="+codeType);
		String json="{"+
				"identifier: \"value\","+
				"label: \"label\","+
				"items: ["+
				"	{value: \"AL\", label: \"Alabama\"},"+
				"	{value: \"AK\", label: \"Alaska\"},"+
				"	{value: \"AZ\", label: \"Arizona\"},"+
				"	{value: \"AR\", label: \"Arkansas\"},"+
				"	{value: \"CA\", label: \"California\"},"+
				"	{value: \"CO\", label: \"Colorado\"},"+
				"	{value: \"CT\", label: \"Connecticut\"}"+
				"]"+
			"}";
		if("Sys2".equals(codeType)){
			json="{"+
					"identifier: \"value\","+
					"label: \"label\","+
					"items: ["+
					"	{value: \"AL\", label: \"Alabama"+rand.nextInt()+"\"},"+
					"	{value: \"AK\", label: \"Alaska"+rand.nextInt()+"\"},"+
					"	{value: \"AZ\", label: \"Arizona"+rand.nextInt()+"\"},"+
					"	{value: \"AR\", label: \"Arkansas"+rand.nextInt()+"\"},"+
					"	{value: \"CA\", label: \"California"+rand.nextInt()+"\"},"+
					"	{value: \"CO\", label: \"Colorado"+rand.nextInt()+"\"},"+
					"	{value: \"CT\", label: \"Connecticut"+rand.nextInt()+"\"}"+
					"]"+
				"}";
		}
		
		System.out.println("完全取数、、、");
		this.writeJson(response, json);
		return null;
    } 
	@Autowired
	private CodeInputService codeInputService;
	@RequestMapping("/codeQuery")
	public  String codeQuery(@ModelAttribute BusinessCodeRS businessCodeRS,HttpServletRequest request,HttpServletResponse response, ModelMap modelMap) { 
		logger.info("codeQuery begin");
		List<BusinessCode> list=codeInputService.codeQuery(businessCodeRS);
		//创建json数据
        JSONObject json = new JSONObject();
        JSONArray jsonFiles = new JSONArray();
		if(list!=null&&list.size()>0){
			for(BusinessCode item:list){
				if(item!=null){
					json=new JSONObject();
					json.put("value", item.getCodeCode());
					json.put("label", item.getCodeCName());
					jsonFiles.put(json);
				}
			}
		}
		logger.info("jsonString:"+jsonFiles.toString());
		this.writeJson(response, jsonFiles.toString());
		logger.info("codeQuery end");
		return null;
    } 
	
	private void writeJson(HttpServletResponse response,String json){
		try {
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/json; charset=utf-8");
			response.getWriter().write(json);
			response.getWriter().flush();
			response.getWriter().close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}
