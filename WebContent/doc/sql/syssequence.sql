/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50620
Source Host           : localhost:3306
Source Database       : mvc

Target Server Type    : MYSQL
Target Server Version : 50620
File Encoding         : 65001

Date: 2016-07-21 19:44:06
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `syssequence`
-- ----------------------------
DROP TABLE IF EXISTS `syssequence`;
CREATE TABLE `syssequence` (
  `GenName` varchar(255) DEFAULT NULL,
  `GenValue` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of syssequence
-- ----------------------------
INSERT INTO `syssequence` VALUES ('InfoNews_PK', '21');
INSERT INTO `syssequence` VALUES ('WeiXinNewList_PK', '12');
INSERT INTO `syssequence` VALUES ('InfoImages_PK', '21');
INSERT INTO `syssequence` VALUES ('WeiXinNews_PK', '13');
