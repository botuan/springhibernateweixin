package cn.com.busi.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;







import cn.com.base.sys.common.BaseJpaDao;
import cn.com.base.sys.common.Page;
import cn.com.base.sys.common.Query;
import cn.com.base.sys.util.Strings;
import cn.com.busi.model.InfoImages;
import cn.com.busi.model.InfoNews;
import cn.com.busi.service.InfoImagesService;
import cn.com.busi.web.vo.InfoImagesRS;
/**
 * 图片的业务逻辑类
 * @author 谢林
 */
@Service(value="infoImagesService")
public class InfoImagesServiceImpl implements InfoImagesService {
	@Autowired
	/**
	 * jpa的操作类
	 */
	private BaseJpaDao baseJpaDao;
    static Logger log=Logger.getLogger(InfoImagesServiceImpl.class);
	@Override
	/**
	 * jpa分页
	 */
	public Page<InfoImages> findInfoImagesByPage(int pageNo,
			int pageSize, InfoImagesRS infoImagesRS) {
		log.info("正在执行  getInfoImagesByPage()");
		Query query=baseJpaDao.createQuery(InfoImages.class);
		this.initQuery(query, infoImagesRS);
		Page page=baseJpaDao.queryPage(query, pageNo, pageSize);
		log.info("执行 getInfoImagesByPage()完毕，正在返回");
		return page;
	}
   /**
	 * 初始化查询条件
	 */
	private void initQuery(Query query,InfoImagesRS infoImagesRS){
		log.info("正在执行  initQuery()");
		if(infoImagesRS!=null){
							if(!Strings.isNullOrEmpty(infoImagesRS.getId())){
				query.eq("id",infoImagesRS.getId());
			}
					if(!Strings.isNullOrEmpty(infoImagesRS.getImageDesc())){
				query.eq("imageDesc",infoImagesRS.getImageDesc());
			}
					if(!Strings.isNullOrEmpty(infoImagesRS.getCreateUser())){
				query.eq("createUser",infoImagesRS.getCreateUser());
			}
					if(!Strings.isNullOrEmpty(infoImagesRS.getImageUrl())){
				query.eq("imageUrl",infoImagesRS.getImageUrl());
			}
					if(!Strings.isNullOrEmpty(infoImagesRS.getCreateDate())){
				query.eq("createDate",infoImagesRS.getCreateDate());
			}
						}
		
		log.info("执行 initQuery完毕，正在返回");
	}

	@Override
	/**
	 * 修改操作
	 */
	public InfoImages infoImagesUpdate(InfoImages infoImages) {
		log.info("正在执行  infoImagesUpdate(InfoImages)");
		baseJpaDao.update(infoImages);
		log.info("执行 infoImagesUpdate完毕，正在返回");
		return infoImages;
	}

	@Override
	
	/**
	 * 新增操作
	 */
	public InfoImages infoImagesSave(InfoImages infoImages) {
		log.info("正在执行  infoImagesSave(infoImages)");
		baseJpaDao.save(infoImages);
		log.info("执行 infoImagesSave完毕，正在返回");
		return infoImages;
	}

	@Override
	
	/**
	 * 删除操作
	 */
	public void infoImagesDelete(InfoImages infoImages) {
		log.info("正在执行  infoImagesDelete(InfoImages)");
		infoNewsDelete(infoImages.getId());
		log.info("执行 Delete()完毕，正在返回");
	}
	@Override
	/**
	 * 通过主键删除操作
	 */
	public void infoNewsDelete(Long id) {
		List<Long> list = new ArrayList<Long>();
		list.add(id);
		baseJpaDao.delete(InfoImages.class, list);
	}
	@Override
	public InfoImages infoImagesGet(InfoImages infoImages) {
		// TODO Auto-generated method stub
		InfoImages info=baseJpaDao.get(InfoImages.class, infoImages.getId());
		return info;
	}
	@Override
	public InfoImages infoImagesGet(Long id) {
		// TODO Auto-generated method stub
		InfoImages info=baseJpaDao.get(InfoImages.class, id);
		return info;
	}
			
}
