package cn.com.busi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.base.sys.common.Page;
import cn.com.base.sys.model.BusinessCode;
import cn.com.base.sys.service.BusinessCodeService;
import cn.com.base.sys.web.vo.BusinessCodeRS;
import cn.com.busi.model.InfoProductType;
@Service("businessService")
public class BusinessServiceImpl implements BusinessCodeService {

	@Override
	public Page<BusinessCodeRS> findBusinessCodeByPage(int pageNo,
			int pageSize, BusinessCodeRS businessCodeRS) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<BusinessCode> findBusinessCodeList(BusinessCodeRS businessCodeRS) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BusinessCode businessCodeUpdate(BusinessCode businessCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BusinessCode businessCodeSave(BusinessCode businessCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void businessCodeDelete(BusinessCode businessCode) {
		// TODO Auto-generated method stub

	}

	@Override
	public void businessCodeDelete(Long businessCodeId) {
		// TODO Auto-generated method stub

	}

	@Override
	public BusinessCode businessCodeGet(BusinessCode businessCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BusinessCode businessCodeGet(Long businessCodeId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Autowired
	InfoProductTypeService infoProductTypeService;
	@Override
	public String translate(String codeType, String codeCode) {
		// TODO Auto-generated method stub
		if("ProjectType".equals(codeType)){
			InfoProductType info=infoProductTypeService.infoProductTypeGet(Long.parseLong(codeCode));
			if(info!=null){
				return info.getName();
				
			}else
			{
				return "";
			}
		}
		return null;
	}

}
