package cn.com.busi.web.ctl;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.com.base.sys.common.Page;
import cn.com.base.sys.common.Result;
import cn.com.base.sys.model.BusinessCode;
import cn.com.base.sys.model.SysUsers;
import cn.com.base.sys.security.SessionManager;
import cn.com.base.sys.service.BusinessCodeService;
import cn.com.base.sys.web.vo.BusinessCodeRS;
import cn.com.busi.model.InfoPages;
import cn.com.busi.service.InfoPagesService;
import cn.com.busi.web.vo.InfoPagesRS;
import cn.com.busi.web.vo.InfoPagesReq;

/**
 * InfoPages 的控制类
 */
@Controller
@RequestMapping("/admin")
public class InfoPagesCtl  extends SessionManager{
	@Autowired
	private InfoPagesService infoPagesService;
	/**
	 * InfoPages分页查询
	 */
	@RequestMapping("/infoPagesQuery")
	public ModelAndView infoPagesQuery(
			@ModelAttribute InfoPagesReq infoPagesReq,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap) {
		super.adminLoginValidate(request);
		InfoPagesRS infoPagesRS = infoPagesReq.getInfoPagesRS();
		modelMap.put("pageSize", infoPagesReq.getPageSize());
		modelMap.put("pageNo", infoPagesReq.getPageNo());
		modelMap.put("ctx", request.getContextPath());
		modelMap.put("infoPagesRS", infoPagesRS);
		Page<InfoPages> page = infoPagesService.findInfoPagesByPage(
				infoPagesReq.getPageNo(), infoPagesReq.getPageSize(),
				infoPagesRS);// .getMenuByAuthoritieId(principal.getName());
		modelMap.put("page", page);
		return new ModelAndView("admin/page/page_list", modelMap);
	}

	/**
	 * InfoPages打开新增界面
	 */
	@RequestMapping("/prepareInfoPagesAdd")
	public ModelAndView prepareInfoPagesAdd(
			@ModelAttribute InfoPagesReq infoPagesReq,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap) {
		super.adminLoginValidate(request);
		
		BusinessCodeRS businessCodeRS=new BusinessCodeRS();
		businessCodeRS.setCodeType("PageType");
		List<BusinessCode> list=businessCodeService.findBusinessCodeList(businessCodeRS);
		modelMap.put("businessCodes", list);
		return new ModelAndView("admin/page/page_create", modelMap);
	}

	/**
	 * InfoPages新增保存
	 */
	@RequestMapping("/infoPagesAdd")
	public  @ResponseBody Result    infoPagesAdd(@ModelAttribute InfoPagesReq infoPagesReq,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap) {
		super.adminLoginValidate(request);
		
		SysUsers u=(SysUsers)super.findLogonUser(request);
		InfoPages infoPages = infoPagesReq.getInfoPages();
		infoPages.setCreateDate(new Date());
		infoPages.setCreateUser(u.getUserId());
		infoPagesService.infoPagesSave(infoPages);
		Result r=new Result();
		r.setCode("0");
		r.setMessage("操作成功");
		return r;
	}

	/**
	 * InfoPages打开修改界面
	 */
	@RequestMapping("/prepareInfoPagesEdit")
	public ModelAndView prepareInfoPagesEdit(
			@ModelAttribute InfoPagesReq infoPagesReq,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap) {
		super.adminLoginValidate(request);
		// InfoPages infoPages=infoPagesReq.getInfoPages();
		// infoPages=infoPagesService.infoPagesGet(infoPages);
		Long id = infoPagesReq.getId();
		InfoPages infoPages = infoPagesService.infoPagesGet(id);

		BusinessCodeRS businessCodeRS=new BusinessCodeRS();
		businessCodeRS.setCodeType("PageType");
		List<BusinessCode> list=businessCodeService.findBusinessCodeList(businessCodeRS);
		modelMap.put("businessCodes", list);
		modelMap.put("infoPages", infoPages);
		return new ModelAndView("admin/page/page_edit", modelMap);
	}
	@Autowired
	private BusinessCodeService businessCodeService;

	/**
	 * InfoPages修改保存
	 */
	@RequestMapping("/infoPagesEdit")
	public  @ResponseBody Result    infoPagesEdit(
			@ModelAttribute InfoPagesReq infoPagesReq,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap) {
		super.adminLoginValidate(request);
		InfoPages infoPages = infoPagesReq.getInfoPages();
		infoPagesService.infoPagesUpdate(infoPages);
		Result r=new Result();
		r.setCode("0");
		r.setMessage("操作成功");
		return r;
	}

	/**
	 * InfoPages删除操作
	 */
	@RequestMapping("/infoPagesDelete")
	public  @ResponseBody Result    infoPagesDelete(
			@ModelAttribute InfoPagesReq infoPagesReq,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap) {
		super.adminLoginValidate(request);
		
		Long id = infoPagesReq.getId();
		infoPagesService.infoPagesDelete(id);

		Result r=new Result();
		r.setCode("0");
		r.setMessage("操作成功");
		return r;
	}
}
