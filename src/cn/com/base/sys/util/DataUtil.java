package cn.com.base.sys.util;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
public class DataUtil {
	static Logger logger=Logger.getLogger(DataUtil.class);
	public static <T> T formatClass(Class<T> clasz,Class<?> obj,Boolean isCoyNull) throws Exception{
		logger.info("正在拷贝值");
		T t=clasz.newInstance();
		Map<String, PropertyDescriptor> fomProperty=getPropertyDescriptorMap(obj);
		Map<String, PropertyDescriptor> toProperty=getPropertyDescriptorMap(clasz);
		for (String key : toProperty.keySet()){
			PropertyDescriptor toP=toProperty.get(key);
			PropertyDescriptor fromP=toProperty.get(key);
			if(fromP!=null){
				logger.info("正在拷贝值给"+key);
				Object valueFrom=getValue(fromP,key);
				if(valueFrom!=null){
					putValue(valueFrom,toP,t);
				}else{
					if(isCoyNull){
						putValue(valueFrom,toP,t);
					}
				}
			}
		}
		logger.info("结束拷贝值");
		return t;
	}
	public static void copy(Object toObject,Object fromObject,Boolean isCoyNull){
		try{
			logger.info("正在拷贝值");
			Map<String, PropertyDescriptor> fomProperty=getPropertyDescriptorMap(fromObject.getClass());
			Map<String, PropertyDescriptor> toProperty=getPropertyDescriptorMap(toObject.getClass());
			for (String key : toProperty.keySet()){
				PropertyDescriptor toP=toProperty.get(key);
				PropertyDescriptor fromP=fomProperty.get(key);
				if(fromP!=null){
					Object valueFrom=getValue(fromP,fromObject);
					logger.info("正在拷贝值给"+key+";值为："+valueFrom);
					if(valueFrom!=null){
						putValue(valueFrom,toP,toObject);
					}else{
						if(isCoyNull){
							putValue(valueFrom,toP,toObject);
						}
					}
				}
			}
			logger.info("结束拷贝值");
			
		}catch(Exception e){
			logger.error("拷贝值出现错误："+e.getMessage());
		}
		
	}
	public static void copyFromDiffField(Object toObject,Object fromObject,String fieldKey,Boolean isCoyNull){
		try{
			if(Strings.isNullOrEmpty(fieldKey)){
				 copy( toObject, fromObject, isCoyNull);
			}else{
				String keys[]=fieldKey.split(",");
				logger.info("正在拷贝值");
				Map<String, PropertyDescriptor> fomProperty=getPropertyDescriptorMap(fromObject.getClass());
				Map<String, PropertyDescriptor> toProperty=getPropertyDescriptorMap(toObject.getClass());
				for(String item:keys){
					String keyValue[]=item.split(":");
					String key=keyValue[0];
					String value=keyValue[1];
					PropertyDescriptor toP=toProperty.get(value);
					PropertyDescriptor fromP=fomProperty.get(key);
					if(fromP!=null){
						Object valueFrom=getValue(fromP,fromObject);
						logger.info("正在拷贝值给"+key+";值为："+valueFrom);
						if(valueFrom!=null){
							putValue(valueFrom,toP,toObject);
						}else{
							if(isCoyNull){
								putValue(valueFrom,toP,toObject);
							}
						}
					}
				}
				logger.info("结束拷贝值");
			}
			
		}catch(Exception e){
			logger.error("拷贝值出现错误："+e.getMessage());
		}
		
	}
	public static Map<String, PropertyDescriptor> getPropertyDescriptorMap(
			Class<?> type) {
		try {
			Map<String, PropertyDescriptor> propertyDescriptorMap = new LinkedHashMap<String, PropertyDescriptor>();
			PropertyDescriptor[] propertyDescriptors = Introspector
					.getBeanInfo(type).getPropertyDescriptors();
			for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
				String name = propertyDescriptor.getName();
				if ("class".equals(name.toLowerCase())) {
					continue;
				}
				propertyDescriptorMap.put(name, propertyDescriptor);
			}
			return propertyDescriptorMap;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	public static void putValue(Object value, PropertyDescriptor propertyDescriptor,
			Object classObject) {
		try{
			Method method = propertyDescriptor.getWriteMethod();
			if (method != null) {
				if (!method.isAccessible()) {
					method.setAccessible(true);
				}
				method.invoke(classObject, value);
			}	
		}catch(Exception e){
			System.out.println("给对象设置值");
		}
		

	}
	
	public static Object getValue(PropertyDescriptor propertyDescriptor,
			Object classObject) {
		Object value=null;
		try{
			Method method = propertyDescriptor.getReadMethod();
			if (method != null) {
				if (!method.isAccessible()) {
					method.setAccessible(true);
				}
				value=method.invoke(classObject);
			}	
		}catch(Exception e){
			System.out.println("给对象设置值么");
		}
		return value;
	}
}
