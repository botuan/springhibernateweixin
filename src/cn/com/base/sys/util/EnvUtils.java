package cn.com.base.sys.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.time.DateUtils;

public class EnvUtils {
	private static String SERVER_IP = null;
	
	
	public static Date getWeekStart(Date dt){
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		int week = cal.get(Calendar.DAY_OF_WEEK) - 2;
		return DateUtils.addDays(dt, -(week));
	}
	
	public static Date getWeekEnd(Date dt){
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		int week = cal.get(Calendar.DAY_OF_WEEK) - 1;
		return DateUtils.addDays(dt, 5-(week));
	}
	
	
	public static String getWeek(Date dt){
		SimpleDateFormat sdf = new SimpleDateFormat("E");
		String week = sdf.format(dt);
		/*String[] weeks = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		int week = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if(week < 0){
			week = 0;
		}*/
		return week;
	}
	
	public static boolean isWeekEnd(Date chackInTime){
		
		String week = getWeek(chackInTime);
		
		if("星期六".equals(week) || "星期日".equals(week)){
			return true;
		}else{
			return false;
		}
		
	}
	
	public static String getNightShift(Date chackOutTime) throws Exception{

		Date nowDate = Calendar.getInstance().getTime();
		SimpleDateFormat ymd = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		if(isWeekEnd(chackOutTime)){
			return "1";//加班
		}else{
			String now = ymd.format(nowDate);
			now  = now +" 19:00:00";
			nowDate = sdf.parse(now);
			if(chackOutTime.getTime() > nowDate.getTime()){
				return "1";
			}else{
				return "0";
			}
		}
	}

	public static String getClientIp(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if ((ip == null) || (ip.length() == 0)
				|| ("unknown".equalsIgnoreCase(ip))) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if ((ip == null) || (ip.length() == 0)
				|| ("unknown".equalsIgnoreCase(ip))) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if ((ip == null) || (ip.length() == 0)
				|| ("unknown".equalsIgnoreCase(ip))) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	public static String getWebClassesPath() {
		String path = new File(Thread.currentThread().getContextClassLoader()
				.getResource("").getPath()).getPath();

		path = path.replace("%20", " ");
		return path;
	}

	public static String getServerIp() {
		return SERVER_IP;
	}

	public static void main(String[] args) {
		System.out.println("IP=[" + getServerIp() + "]");
	}

	private static String getInnerServerIp() {
		String osName = System.getProperty("os.name").toLowerCase();
		String ip = null;
		try {
			if (osName.startsWith("linux")) {
				ip = getLocalIp("/sbin/ip addr", "inet ", "", "/");
			} else if (osName.startsWith("window")) {
				ip = getLocalIp("ipconfig /all", "IP Address", ": ", "(");
				if (ip == null)
					ip = getLocalIp("ipconfig /all", "IPv4 地址", ":", "(");
			} else if (osName.startsWith("aix")) {
				ip = getLocalIp("ifconfig -a", "inet ", "", "netmask");
			} else {
				throw new IllegalStateException("Not support OS:" + osName);
			}
		} catch (Exception e) {
			throw new IllegalStateException(e.toString(), e);
		}
		if (ip == null) {
			ip = "127.0.0.1";
		}
		return ip;
	}

	private static String getLocalIp(String cmd, String indLine,
			String indStart, String indEnd) throws Exception {
		Runtime rt = Runtime.getRuntime();
		Process process = rt.exec(cmd);

		BufferedReader in = new BufferedReader(new InputStreamReader(
				process.getInputStream()));
		String s;
		while ((s = in.readLine()) != null) {
			s = s.trim();
			if (s.startsWith(indLine)) {
				s = s.substring(indLine.length()).trim();
				int pos = s.indexOf(indStart);
				String ip = s.substring(indStart.length() + pos).trim();
				pos = ip.indexOf(indEnd);
				if (pos > -1) {
					ip = ip.substring(0, pos);
				}
				if (!ip.startsWith("127.")) {
					return ip;
				}
			}
		}
		return null;
	}

	static {
		try {
			SERVER_IP = getInnerServerIp();
		} catch (Throwable e) {
			SERVER_IP = "127.0.0.1";
		}
	}
}