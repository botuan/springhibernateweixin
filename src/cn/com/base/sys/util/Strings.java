package cn.com.base.sys.util;

public class Strings {
	
	
	private Strings(){
		
	}
	
	public static boolean isNullOrEmpty(Object value){
		if(value==null)
			return true;
		return value.toString().trim().isEmpty();
	}
	
	
	public static String trim(Object value){
		if(isNullOrEmpty(value)){
			return "";
		}
		return value.toString().trim();
		
	}

}
