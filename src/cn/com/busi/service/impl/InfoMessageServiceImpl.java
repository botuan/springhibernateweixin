package cn.com.busi.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import cn.com.base.sys.common.BaseJpaDao;
import cn.com.base.sys.common.Page;
import cn.com.base.sys.common.Query;
import cn.com.busi.model.InfoMessage;
import cn.com.busi.service.InfoMessageService;
import cn.com.base.sys.util.Strings;
import cn.com.busi.web.vo.InfoMessageRS;
/**
 * ${tableBean.desc}的业务逻辑类
 * @author 谢林
 */
@Service(value="infoMessageService")
public class InfoMessageServiceImpl implements InfoMessageService {
	@Autowired
	/**
	 * jpa的操作类
	 */
	private BaseJpaDao baseJpaDao;
    static Logger log=Logger.getLogger(InfoMessageServiceImpl.class);
	@Override
	/**
	 * jpa分页
	 */
	public Page<InfoMessage> findInfoMessageByPage(int pageNo,
			int pageSize, InfoMessageRS infoMessageRS) {
		log.info("正在执行  getInfoMessageByPage()");
		Query query=baseJpaDao.createQuery(InfoMessage.class);
		this.initQuery(query, infoMessageRS);
		Page page=baseJpaDao.queryPage(query, pageNo, pageSize);
		log.info("执行 getInfoMessageByPage()完毕，正在返回");
		return page;
	}
   /**
	 * 初始化查询条件
	 */
	private void initQuery(Query query,InfoMessageRS infoMessageRS){
		log.info("正在执行  initQuery()");
		if(infoMessageRS!=null){
								}
		
		log.info("执行 initQuery完毕，正在返回");
	}

	@Override
	/**
	 * 修改操作
	 */
	public InfoMessage infoMessageUpdate(InfoMessage infoMessage) {
		log.info("正在执行  infoMessageUpdate(InfoMessage)");
		baseJpaDao.update(infoMessage);
		log.info("执行 infoMessageUpdate完毕，正在返回");
		return infoMessage;
	}

	@Override
	
	/**
	 * 新增操作
	 */
	public InfoMessage infoMessageSave(InfoMessage infoMessage) {
		log.info("正在执行  infoMessageSave(infoMessage)");
		baseJpaDao.save(infoMessage);
		log.info("执行 infoMessageSave完毕，正在返回");
		return infoMessage;
	}

	@Override
	
	/**
	 * 删除操作
	 */
	public void infoMessageDelete(InfoMessage infoMessage) {
		log.info("正在执行  infoMessageDelete(InfoMessage)");
		//baseJpaDao.delete(infoMessage);
								log.info("执行 Delete()完毕，正在返回");
	}
	
	@Override
	/**
	 * 通过主键删除操作
	 */
		public void infoMessageDelete(Long  id){
	    List<Long> list=new ArrayList<Long>();
		list.add(id);
		baseJpaDao.delete(InfoMessage.class, list);
	}
		
	@Override
			/**
	 * 通过主键查找一个记录
	 */
	public InfoMessage infoMessageGet(Long id) {
		log.info("正在执行  infoMessageGet(Long)");
		InfoMessage infoMessage=baseJpaDao.get(InfoMessage.class, id);
		log.info("执行 infoMessageGet完毕，正在返回");
		return infoMessage;
	}
	
}
