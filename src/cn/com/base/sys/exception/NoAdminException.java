package cn.com.base.sys.exception;

public class NoAdminException extends BaseException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoAdminException(String message) {
		super(message);
	}

}
