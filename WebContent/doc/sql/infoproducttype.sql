/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50620
Source Host           : localhost:3306
Source Database       : mvc

Target Server Type    : MYSQL
Target Server Version : 50620
File Encoding         : 65001

Date: 2016-07-21 19:44:00
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `infoproducttype`
-- ----------------------------
DROP TABLE IF EXISTS `infoproducttype`;
CREATE TABLE `infoproducttype` (
  `id` bigint(20) NOT NULL,
  `createDate` datetime DEFAULT NULL,
  `createUser` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `parentId` bigint(20) DEFAULT NULL,
  `remark` varchar(300) DEFAULT NULL,
  `typeDesc` varchar(500) DEFAULT NULL,
  `typeImageUrl` varchar(300) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `updateUser` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of infoproducttype
-- ----------------------------
