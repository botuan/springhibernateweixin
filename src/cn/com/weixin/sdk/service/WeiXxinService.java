package cn.com.weixin.sdk.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface WeiXxinService {
	void handlerMsg(HttpServletRequest request, HttpServletResponse response);
	void handlerApi(HttpServletRequest request, HttpServletResponse response);
	void handlerMenu(HttpServletRequest request, HttpServletResponse response);
}
