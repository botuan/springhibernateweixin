package cn.com.busi.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;








import cn.com.base.sys.common.BaseJpaDao;
import cn.com.base.sys.common.Page;
import cn.com.base.sys.common.Query;
import cn.com.base.sys.util.Strings;
import cn.com.busi.model.InfoProduct;
import cn.com.busi.service.InfoProductService;
import cn.com.busi.web.vo.InfoProductRS;
/**
 * 产品的业务逻辑类
 * @author 谢林
 */
@Service(value="infoProductService")
public class InfoProductServiceImpl implements InfoProductService {
	@Autowired
	/**
	 * jpa的操作类
	 */
	private BaseJpaDao baseJpaDao;
    static Logger log=Logger.getLogger(InfoProductServiceImpl.class);
	@Override
	/**
	 * jpa分页
	 */
	public Page<InfoProduct> findInfoProductByPage(int pageNo,
			int pageSize, InfoProductRS infoProductRS) {
		log.info("正在执行  getInfoProductByPage()");
		Query query=baseJpaDao.createQuery(InfoProduct.class);
		this.initQuery(query, infoProductRS);
		Page page=baseJpaDao.queryPage(query, pageNo, pageSize);
		log.info("执行 getInfoProductByPage()完毕，正在返回");
		return page;
	}
   /**
	 * 初始化查询条件
	 */
	private void initQuery(Query query,InfoProductRS infoProductRS){
		log.info("正在执行  initQuery()");
		if(infoProductRS!=null){
							if(!Strings.isNullOrEmpty(infoProductRS.getCreateDate())){
				query.eq("createDate",infoProductRS.getCreateDate());
			}
					if(!Strings.isNullOrEmpty(infoProductRS.getUpdateDate())){
				query.eq("updateDate",infoProductRS.getUpdateDate());
			}
					if(!Strings.isNullOrEmpty(infoProductRS.getCreateUser())){
				query.eq("createUser",infoProductRS.getCreateUser());
			}
					if(!Strings.isNullOrEmpty(infoProductRS.getUpdateUser())){
				query.eq("updateUser",infoProductRS.getUpdateUser());
			}
					if(!Strings.isNullOrEmpty(infoProductRS.getId())){
				query.eq("id",infoProductRS.getId());
			}
					if(!Strings.isNullOrEmpty(infoProductRS.getProductDesc())){
				query.eq("productDesc",infoProductRS.getProductDesc());
			}
					if(!Strings.isNullOrEmpty(infoProductRS.getImageUrl())){
				query.eq("imageUrl",infoProductRS.getImageUrl());
			}
					if(!Strings.isNullOrEmpty(infoProductRS.getContent())){
				query.eq("content",infoProductRS.getContent());
			}
					
					if(!Strings.isNullOrEmpty(infoProductRS.getName())){
						query.like("name",infoProductRS.getName());
					}
					if(!Strings.isNullOrEmpty(infoProductRS.getType())){
						query.eq("type",infoProductRS.getType());
					}
						}
		
		log.info("执行 initQuery完毕，正在返回");
	}

	@Override
	/**
	 * 修改操作
	 */
	public InfoProduct infoProductUpdate(InfoProduct infoProduct) {
		log.info("正在执行  infoProductUpdate(InfoProduct)");
		Date date=new Date();
		infoProduct.setUpdateDate(date);
		baseJpaDao.update(infoProduct);
		log.info("执行 infoProductUpdate完毕，正在返回");
		return infoProduct;
	}
	@Override
	/**
	 * 新增操作
	 */
	public InfoProduct infoProductSave(InfoProduct infoProduct) {
		log.info("正在执行  infoProductSave(infoProduct)");
		Date date=new Date();
		infoProduct.setCreateDate(date);
		infoProduct.setUpdateDate(date);
		baseJpaDao.save(infoProduct);
		log.info("执行 infoProductSave完毕，正在返回");
		return infoProduct;
	}

	@Override
	
	/**
	 * 删除操作
	 */
	public void infoProductDelete(InfoProduct infoProduct) {
		log.info("正在执行  infoProductDelete(InfoProduct)");
		List<Long> ids=new ArrayList<Long>();
		ids.add(infoProduct.getId());
		baseJpaDao.delete(InfoProduct.class, ids);
		log.info("执行 Delete()完毕，正在返回");
	}
	@Override
	public InfoProduct infoProductGet(InfoProduct infoProduct) {

		InfoProduct info=baseJpaDao.get(InfoProduct.class, infoProduct.getId());
		return info;
	}
	@Override
	public InfoProduct infoProductGet(Long id) {
		InfoProduct info=baseJpaDao.get(InfoProduct.class, id);
		return info;
	}
	
	
}
