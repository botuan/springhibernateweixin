package cn.com.busi.web.ctl;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.com.base.sys.common.Page;
import cn.com.base.sys.common.Result;
import cn.com.base.sys.security.SessionManager;
import cn.com.busi.model.InfoCustomer;
import cn.com.busi.service.InfoCustomerService;
import cn.com.busi.web.vo.InfoCustomerRS;
import cn.com.busi.web.vo.InfoCustomerReq;
/**
 *用户表 的控制类
 */
@Controller
@RequestMapping("/admin")
public class InfoCustomerCtl extends SessionManager{
	@Autowired
	/**
	 *InfoCustomer逻辑操作接口
	 */
	private InfoCustomerService infoCustomerService;
	/**
	 *InfoCustomer分页查询
	 */
	@RequestMapping("/infoCustomerQuery")
	public ModelAndView  infoCustomerQuery(@ModelAttribute InfoCustomerReq infoCustomerReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		InfoCustomerRS infoCustomerRS=infoCustomerReq.getInfoCustomerRS();
		modelMap.put("pageSize",infoCustomerReq.getPageSize());
		modelMap.put("pageNo", infoCustomerReq.getPageNo());
		modelMap.put("ctx", request.getContextPath());
		modelMap.put("infoCustomerRS", infoCustomerRS);
		Page<InfoCustomer> page=infoCustomerService.findInfoCustomerByPage(infoCustomerReq.getPageNo(), infoCustomerReq.getPageSize(),infoCustomerRS);//.getMenuByAuthoritieId(principal.getName());
		modelMap.put("page",page);
		return new ModelAndView("admin/customer/customer_list",modelMap);
	}
	/**
	 *InfoCustomer打开新增界面
	 */
	@RequestMapping("/prepareInfoCustomerAdd")
	public ModelAndView  prepareInfoCustomerAdd(@ModelAttribute InfoCustomerReq infoCustomerReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		return new ModelAndView("admin/customer/customer_create",modelMap);
	}
	/**
	 *InfoCustomer新增保存
	 */
	@RequestMapping("/infoCustomerAdd")
	public  @ResponseBody Result  infoCustomerAdd(@ModelAttribute InfoCustomerReq infoCustomerReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		InfoCustomer infoCustomer=infoCustomerReq.getInfoCustomer();
		infoCustomerService.infoCustomerSave(infoCustomer);
		Result r=new Result();
		r.setCode("0");
		r.setMessage("上传成功");
		return r;
	}
	/**
	 *InfoCustomer打开修改界面
	 */
	@RequestMapping("/prepareInfoCustomerEdit")
	public ModelAndView  prepareInfoCustomerEdit(@ModelAttribute InfoCustomerReq infoCustomerReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		InfoCustomer infoCustomer=infoCustomerReq.getInfoCustomer();
		infoCustomer=infoCustomerService.infoCustomerGet(infoCustomer.getEmail());
					
		
		modelMap.put("infoCustomer", infoCustomer);
		return new ModelAndView("admin/customer/customer_edit",modelMap);
	}
	/**
	 *InfoCustomer修改保存
	 */
	@RequestMapping("/infoCustomerEdit")
	public  @ResponseBody Result  infoCustomerEdit(@ModelAttribute InfoCustomerReq infoCustomerReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		InfoCustomer infoCustomer=infoCustomerReq.getInfoCustomer();
		infoCustomerService.infoCustomerUpdate(infoCustomer);
		Result r=new Result();
		r.setCode("0");
		r.setMessage("上传成功");
		return r;
	}
	/**
	 *InfoCustomer删除操作
	 */
	@RequestMapping("/infoCustomerDelete")
	public  @ResponseBody Result  infoCustomerDelete(@ModelAttribute InfoCustomerReq infoCustomerReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		String email=infoCustomerReq.getEmail();
		infoCustomerService.infoCustomerDelete(email);				
		Result r=new Result();
		r.setCode("0");
		r.setMessage("上传成功");
		return r;
	}
}
