package cn.com.busi.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.base.sys.common.BaseJpaDao;
import cn.com.base.sys.common.Constants;
import cn.com.base.sys.common.Page;
import cn.com.base.sys.common.Query;
import cn.com.base.sys.util.Strings;
import cn.com.busi.model.InfoPages;
import cn.com.busi.service.InfoPagesService;
import cn.com.busi.web.vo.InfoPagesRS;

/**
 * 页面的业务逻辑类
 * 
 * @author 谢林
 */
@Service(value = "infoPagesService")
public class InfoPagesServiceImpl implements InfoPagesService {
	@Autowired
	/**
	 * jpa的操作类
	 */
	private BaseJpaDao baseJpaDao;
	static Logger log = Logger.getLogger(InfoPagesServiceImpl.class);

	@Override
	/**
	 * jpa分页
	 */
	public Page<InfoPages> findInfoPagesByPage(int pageNo, int pageSize,
			InfoPagesRS infoPagesRS) {
		log.info("正在执行  getInfoPagesByPage()");
		Query query = baseJpaDao.createQuery(InfoPages.class);
		this.initQuery(query, infoPagesRS);
		Page page = baseJpaDao.queryPage(query, pageNo, pageSize);
		log.info("执行 getInfoPagesByPage()完毕，正在返回");
		return page;
	}

	/**
	 * 初始化查询条件
	 */
	private void initQuery(Query query, InfoPagesRS infoPagesRS) {
		log.info("正在执行  initQuery()");
		if (infoPagesRS != null) {
			if (!Strings.isNullOrEmpty(infoPagesRS.getId())) {
				query.eq("id", infoPagesRS.getId());
			}
			if (!Strings.isNullOrEmpty(infoPagesRS.getType())) {
				query.eq("type", infoPagesRS.getType());
			}
			if (!Strings.isNullOrEmpty(infoPagesRS.getContent())) {
				query.eq("content", infoPagesRS.getContent());
			}
			if (!Strings.isNullOrEmpty(infoPagesRS.getCreateDate())) {
				query.eq("createDate", infoPagesRS.getCreateDate());
			}
			if (!Strings.isNullOrEmpty(infoPagesRS.getCreateUser())) {
				query.eq("createUser", infoPagesRS.getCreateUser());
			}
		}

		log.info("执行 initQuery完毕，正在返回");
	}

	@Override
	/**
	 * 修改操作
	 */
	public InfoPages infoPagesUpdate(InfoPages infoPages) {
		log.info("正在执行  infoPagesUpdate(InfoPages)");
		baseJpaDao.update(infoPages);
		log.info("执行 infoPagesUpdate完毕，正在返回");
		return infoPages;
	}

	@Override
	/**
	 * 新增操作
	 */
	public InfoPages infoPagesSave(InfoPages infoPages) {
		log.info("正在执行  infoPagesSave(infoPages)");
		baseJpaDao.save(infoPages);
		log.info("执行 infoPagesSave完毕，正在返回");
		return infoPages;
	}

	@Override
	/**
	 * 删除操作
	 */
	public void infoPagesDelete(InfoPages infoPages) {
		log.info("正在执行  infoPagesDelete(InfoPages)");
		// baseJpaDao.delete(infoPages);
		infoPagesDelete(infoPages.getId());
		log.info("执行 Delete()完毕，正在返回");
	}

	@Override
	/**
	 * 通过主键删除操作
	 */
	public void infoPagesDelete(Long id) {
		List<Long> list = new ArrayList<Long>();
		list.add(id);
		baseJpaDao.delete(InfoPages.class, list);
	}

	@Override
	/**
	 * 通过对象查找一个记录，对象中主键必须有
	 */
	public InfoPages infoPagesGet(InfoPages infoPages) {
		log.info("正在执行  infoPagesGet(InfoPages)");
		infoPages = infoPagesGet(infoPages.getId());
		log.info("执行 infoPagesGet 完毕，正在返回");
		return infoPages;
	}

	@Override
	/**
	 * 通过主键查找一个记录
	 */
	public InfoPages infoPagesGet(Long id) {
		log.info("正在执行  infoPagesGet(Long)");
		InfoPages infoPages = baseJpaDao.get(InfoPages.class, id);
		log.info("执行 infoPagesGet完毕，正在返回");
		return infoPages;
	}

	@Override
	public List<InfoPages> queryAll() {
		// TODO Auto-generated method stub
		Query query=baseJpaDao.createQuery(InfoPages.class);
		List<InfoPages> page=baseJpaDao.query(query);
		return page;
	}

	@Override
	public List<InfoPages> findCommPage() {
		InfoPagesRS rs=new InfoPagesRS();
		rs.setType(Constants.PageType.common);
		Page page=this.findInfoPagesByPage(0, Integer.MAX_VALUE, rs);
		// TODO Auto-generated method stub
		List<InfoPages> list=null;
		if(page!=null){
			list=page.getResult();
		}
		return list;
	}

}
