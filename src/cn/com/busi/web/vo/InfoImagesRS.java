package cn.com.busi.web.vo;

import java.util.Date;


/**
 * ${modelFieldBean.desc}
 * @author 谢林
 */
public class InfoImagesRS implements java.io.Serializable {
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		/**
	 * 返回 主键
	 */
	private Long id;
	/**
	 * 返回 主键
	 * @return
	 */
	public Long getId (){
	 return id;
	}
	/**
	 * 设置 主键
	 * @param id
	 */
	public void setId (Long id){
	 this.id=id;
	}

	private String type;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 返回 图片描述
	 */
	private String imageDesc;
	/**
	 * 返回 图片描述
	 * @return
	 */
	public String getImageDesc (){
	 return imageDesc;
	}
	/**
	 * 设置 图片描述
	 * @param imageDesc
	 */
	public void setImageDesc (String imageDesc){
	 this.imageDesc=imageDesc;
	}
	/**
	 * 返回 图片上传人
	 */
	private String createUser;
	/**
	 * 返回 图片上传人
	 * @return
	 */
	public String getCreateUser (){
	 return createUser;
	}
	/**
	 * 设置 图片上传人
	 * @param createUser
	 */
	public void setCreateUser (String createUser){
	 this.createUser=createUser;
	}
	/**
	 * 返回 图片地址
	 */
	private String imageUrl;
	/**
	 * 返回 图片地址
	 * @return
	 */
	public String getImageUrl (){
	 return imageUrl;
	}
	/**
	 * 设置 图片地址
	 * @param imageUrl
	 */
	public void setImageUrl (String imageUrl){
	 this.imageUrl=imageUrl;
	}
	/**
	 * 返回 图片上传时间
	 */
	private Date createDate;
	/**
	 * 返回 图片上传时间
	 * @return
	 */
	public Date getCreateDate (){
	 return createDate;
	}
	/**
	 * 设置 图片上传时间
	 * @param createDate
	 */
	public void setCreateDate (Date createDate){
	 this.createDate=createDate;
	}
	
	private String name;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}