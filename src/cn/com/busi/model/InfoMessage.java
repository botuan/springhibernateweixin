package cn.com.busi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Id;

/**
 *  锟斤拷锟皆憋拷锟绞碉拷锟斤拷锟�
 * @return
 */ 
@Entity
@TableGenerator(name = "SYSSEQUENCE", table = "SYSSEQUENCE", pkColumnName = "GenName", valueColumnName = "GenValue", pkColumnValue = "InfoMessage_PK", allocationSize = 1)
@Table(name = "InfoMessage")
public class InfoMessage implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	private String title;
	/**
	 * 锟斤拷锟斤拷 锟斤拷锟斤拷
	 * @return
	 */
	@Column(name = "Title", nullable = false, length=300)
	public String getTitle (){
	 return title;
	}
	/**
	 * 锟斤拷锟斤拷 锟斤拷锟斤拷
	 * @param title
	 */
	public void setTitle (String title){
	 this.title=title;
	}
	private String linkerName;
	/**
	 * 锟斤拷锟斤拷 锟斤拷锟斤拷
	 * @return
	 */
	@Column(name = "LinkerName", nullable = false, length=300)
	public String getLinkerName (){
	 return linkerName;
	}
	/**
	 * 锟斤拷锟斤拷 锟斤拷锟斤拷
	 * @param linkerName
	 */
	public void setLinkerName (String linkerName){
	 this.linkerName=linkerName;
	}
	private String linkerPhone;
	/**
	 * 锟斤拷锟斤拷 锟斤拷锟斤拷
	 * @return
	 */
	@Column(name = "LinkerPhone", nullable = false, length=300)
	public String getLinkerPhone (){
	 return linkerPhone;
	}
	/**
	 * 锟斤拷锟斤拷 锟斤拷锟斤拷
	 * @param linkerPhone
	 */
	public void setLinkerPhone (String linkerPhone){
	 this.linkerPhone=linkerPhone;
	}
	private String message;
	/**
	 * 锟斤拷锟斤拷 锟斤拷锟斤拷
	 * @return
	 */
	@Column(name = "Message", nullable = false, length=300)
	public String getMessage (){
	 return message;
	}
	/**
	 * 锟斤拷锟斤拷 锟斤拷锟斤拷
	 * @param message
	 */
	public void setMessage (String message){
	 this.message=message;
	}
	private String linkerEmail;
	/**
	 * 锟斤拷锟斤拷 锟斤拷锟斤拷
	 * @return
	 */
	@Column(name = "LinkerEmail", nullable = false, length=70)
	public String getLinkerEmail (){
	 return linkerEmail;
	}
	/**
	 * 锟斤拷锟斤拷 锟斤拷锟斤拷
	 * @param linkerEmail
	 */
	public void setLinkerEmail (String linkerEmail){
	 this.linkerEmail=linkerEmail;
	}

	private Long id;
	/**
	 * 锟斤拷锟斤拷 锟斤拷锟斤拷 锟斤拷锟斤拷
	 * @return
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "SYSSEQUENCE")
	@Column(name = "Id", nullable = false, length=50)
	public Long getId (){
	 return id;
	}
	/**
	 * 锟斤拷锟斤拷 锟斤拷锟斤拷 锟斤拷锟斤拷
	 * @param id
	 */
	public void setId (Long id){
	 this.id=id;
	}
}