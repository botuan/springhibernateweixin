package cn.com.base.sys.model;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.springframework.format.annotation.DateTimeFormat;

/**
 *  代码字典的实体类
 * @return
 */ 
@Entity
@TableGenerator(name = "SYSSEQUENCE", table = "SYSSEQUENCE", pkColumnName = "GenName", valueColumnName = "GenValue", pkColumnValue = "BusinessCode_PK", allocationSize = 1)
@Table(name = "BusinessCode")
public class BusinessCode implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private String codeCName;
	/**
	 * 返回 中文名称
	 * @return
	 */
	@Column(name = "CodeCName", nullable = false, length=15)
	public String getCodeCName (){
	 return codeCName;
	}
	/**
	 * 设置 中文名称
	 * @param codeCName
	 */
	public void setCodeCName (String codeCName){
	 this.codeCName=codeCName;
	}
	private String codeEEname;
	/**
	 * 返回 英文名称
	 * @return
	 */
	@Column(name = "CodeEEname", nullable = false, length=15)
	public String getCodeEEname (){
	 return codeEEname;
	}
	/**
	 * 设置 英文名称
	 * @param codeEEname
	 */
	public void setCodeEEname (String codeEEname){
	 this.codeEEname=codeEEname;
	}
	private String flag;
	/**
	 * 返回 标识自动
	 * @return
	 */
	@Column(name = "Flag", nullable = false, length=15)
	public String getFlag (){
	 return flag;
	}
	/**
	 * 设置 标识自动
	 * @param flag
	 */
	public void setFlag (String flag){
	 this.flag=flag;
	}
	private Date insertTimeForHis;
	/**
	 * 返回 数据插入时间
	 * @return
	 */

	@DateTimeFormat(pattern="yyyy-MM-dd")
	@Column(name = "InsertTimeForHis", nullable = false, length=15)
	public Date getInsertTimeForHis (){
	 return insertTimeForHis;
	}
	/**
	 * 设置 数据插入时间
	 * @param insertTimeForHis
	 */
	public void setInsertTimeForHis (Date insertTimeForHis){
	 this.insertTimeForHis=insertTimeForHis;
	}
	private String newCodeCode;
	/**
	 * 返回 新代码值
	 * @return
	 */
	@Column(name = "NewCodeCode", nullable = false, length=15)
	public String getNewCodeCode (){
	 return newCodeCode;
	}
	/**
	 * 设置 新代码值
	 * @param newCodeCode
	 */
	public void setNewCodeCode (String newCodeCode){
	 this.newCodeCode=newCodeCode;
	}
	private Date operateTimeForHis;
	/**
	 * 返回 修改时间
	 * @return
	 */

	@DateTimeFormat(pattern="yyyy-MM-dd")
	@Column(name = "OperateTimeForHis", nullable = false, length=15)
	public Date getOperateTimeForHis (){
	 return operateTimeForHis;
	}
	/**
	 * 设置 修改时间
	 * @param operateTimeForHis
	 */
	public void setOperateTimeForHis (Date operateTimeForHis){
	 this.operateTimeForHis=operateTimeForHis;
	}
	private String validStatus;
	/**
	 * 返回 有效状态
	 * @return
	 */
	@Column(name = "ValidStatus", nullable = false, length=15)
	public String getValidStatus (){
	 return validStatus;
	}
	/**
	 * 设置 有效状态
	 * @param validStatus
	 */
	public void setValidStatus (String validStatus){
	 this.validStatus=validStatus;
	}
	private Integer version;
	/**
	 * 返回 系统版本
	 * @return
	 */
	@Column(name = "Version", nullable = false, length=15)
	public Integer getVersion (){
	 return version;
	}
	/**
	 * 设置 系统版本
	 * @param version
	 */
	public void setVersion (Integer version){
	 this.version=version;
	}

	private Long id;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "SYSSEQUENCE")
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	private String codeCode;
	/**
	 * 返回 代码值
	 * @return
	 */
	@Column(name = "CodeCode", nullable = false, length=15)
	public String getCodeCode (){
	 return codeCode;
	}
	/**
	 * 设置 代码值
	 * @param codeCode
	 */
	public void setCodeCode (String codeCode){
	 this.codeCode=codeCode;
	}
	private String codeType;
	/**
	 * 返回 代码类型
	 * @return
	 */
	@Column(name = "CodeType", nullable = false, length=15)
	public String getCodeType (){
	 return codeType;
	}
	/**
	 * 设置 代码类型
	 * @param codeType
	 */
	public void setCodeType (String codeType){
	 this.codeType=codeType;
	}
}