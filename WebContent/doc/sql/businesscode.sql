/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50620
Source Host           : localhost:3306
Source Database       : mvc

Target Server Type    : MYSQL
Target Server Version : 50620
File Encoding         : 65001

Date: 2016-07-21 19:42:42
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `businesscode`
-- ----------------------------
DROP TABLE IF EXISTS `businesscode`;
CREATE TABLE `businesscode` (
  `ID` bigint(20) NOT NULL,
  `CodeCName` varchar(15) NOT NULL,
  `CodeCode` varchar(15) NOT NULL,
  `CodeEEname` varchar(15) NOT NULL,
  `CodeType` varchar(15) NOT NULL,
  `Flag` varchar(15) NOT NULL,
  `InsertTimeForHis` datetime NOT NULL,
  `NewCodeCode` varchar(15) NOT NULL,
  `OperateTimeForHis` datetime NOT NULL,
  `ValidStatus` varchar(15) NOT NULL,
  `Version` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of businesscode
-- ----------------------------
