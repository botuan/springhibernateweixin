package cn.com.busi.web.ctl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cn.com.base.sys.common.Page;
import cn.com.base.sys.security.SessionManager;
import cn.com.busi.model.InfoImages;
import cn.com.busi.model.InfoNews;
import cn.com.busi.model.WeiXinNews;
import cn.com.busi.service.InfoImagesService;
import cn.com.busi.service.InfoNewsService;
import cn.com.busi.service.WeiXinNewsService;
import cn.com.busi.web.vo.InfoNewsRS;
import cn.com.busi.web.vo.InfoNewsReq;
import cn.com.busi.web.vo.WeiXinNewsRS;
import cn.com.busi.web.vo.WeiXinNewsReq;
import cn.com.weixin.sdk.api.ApiResult;
import cn.com.weixin.sdk.api.MediaApi;

@Controller
@RequestMapping("/admin")
public class WeiXinNewsCtl extends SessionManager{
	@Autowired
	private WeiXinNewsService weiXinNewsService;

	@Autowired
	private InfoNewsService infoNewsService;
	@Autowired
	private InfoImagesService infoImagesService;
	@RequestMapping("/weiXinNewsQuery")
	public ModelAndView  weiXinNewsQuery(@ModelAttribute WeiXinNewsReq weiXinNewsReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		WeiXinNewsRS weiXinNewsRS=weiXinNewsReq.getWeiXinNewsRS();
		modelMap.put("pageSize",weiXinNewsReq.getPageSize());
		modelMap.put("pageNo", weiXinNewsReq.getPageNo());
		modelMap.put("ctx", request.getContextPath());
		modelMap.put("weiXinNewsRS", weiXinNewsRS);
		Page<WeiXinNews> page=weiXinNewsService.findWeiXinNewsByPage(weiXinNewsReq.getPageNo(), weiXinNewsReq.getPageSize(),weiXinNewsRS);//.getMenuByAuthoritieId(principal.getName());
		modelMap.put("page",page);
		return new ModelAndView("Articles/list",modelMap);
	}
	
	
	@RequestMapping("/infoNewsInQuery")
	public ModelAndView  infoNewsInQuery(@ModelAttribute InfoNewsReq infoNewsReq,@ModelAttribute WeiXinNewsReq weiXinNewsReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		InfoNewsRS infoNewsRS=infoNewsReq.getInfoNewsRS();
		WeiXinNewsRS weiXinNewsRS=weiXinNewsReq.getWeiXinNewsRS();
		modelMap.put("pageSize",infoNewsReq.getPageSize());
		modelMap.put("pageNo", infoNewsReq.getPageNo());
		modelMap.put("ctx", request.getContextPath());
		modelMap.put("infoNewsRS", infoNewsRS);
		modelMap.put("weiXinNewsRS", weiXinNewsRS);
		Page<InfoNews> page=weiXinNewsService.findInfoNewsInByPage(infoNewsReq.getPageNo(), infoNewsReq.getPageSize(),infoNewsRS,weiXinNewsRS);//.getMenuByAuthoritieId(principal.getName());
		modelMap.put("page",page);
		return new ModelAndView("Articles/inlist",modelMap);
	}

	@RequestMapping("/prepareWeiXinNewsEdit")
	public ModelAndView  prepareWeiXinNewsEdit(@ModelAttribute InfoNewsReq infoNewsReq,@ModelAttribute WeiXinNewsReq weiXinNewsReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		WeiXinNewsRS weiXinNewsRS=weiXinNewsReq.getWeiXinNewsRS();
		Page<WeiXinNews> page=weiXinNewsService.findWeiXinNewsByPage(weiXinNewsReq.getPageNo(), weiXinNewsReq.getPageSize(),weiXinNewsRS);//.getMenuByAuthoritieId(principal.getName());
		WeiXinNews news=page.getResult().get(0);
		modelMap.put("weiXinNews",news);
		return new ModelAndView("Articles/contentsEdit",modelMap);
	}
	
	@RequestMapping("/articlePreView")
	public ModelAndView  articlePreView(@ModelAttribute InfoNewsReq infoNewsReq,@ModelAttribute WeiXinNewsReq weiXinNewsReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		WeiXinNewsRS weiXinNewsRS=weiXinNewsReq.getWeiXinNewsRS();
		modelMap.put("contents",weiXinNewsRS.getContent());
		return new ModelAndView("web/contents",modelMap);
	}
	
	@RequestMapping("/weiXinNewsDelete")
	public ModelAndView  weiXinNewsDelete(@ModelAttribute InfoNewsReq infoNewsReq,@ModelAttribute WeiXinNewsReq weiXinNewsReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		WeiXinNewsRS weiXinNewsRS=weiXinNewsReq.getWeiXinNewsRS();
		Page<WeiXinNews> page=weiXinNewsService.findWeiXinNewsByPage(weiXinNewsReq.getPageNo(), weiXinNewsReq.getPageSize(),weiXinNewsRS);//.getMenuByAuthoritieId(principal.getName());
		
		for(WeiXinNews weiXinNews :page.getResult()){
			weiXinNewsService.WeiXinNewsDelete(weiXinNews.getId());
		}
		modelMap.put("callbackUrl", "admin/infoNewsInQuery");
		modelMap.put("para", "?weiXinNewsRS.weiXinNewListId="+weiXinNewsRS.getWeiXinNewListId());
		modelMap.put("message", "修改成功");
		return new ModelAndView("success",modelMap);
	}
	
	
	@RequestMapping("/weiXinNewsAdd")
	public ModelAndView  weiXinNewsAdd(@ModelAttribute InfoNewsReq infoNewsReq,@ModelAttribute WeiXinNewsReq weiXinNewsReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		WeiXinNewsRS weiXinNewsRS=weiXinNewsReq.getWeiXinNewsRS();
		
		String newsIds[]=weiXinNewsReq.getNewsId().split(",");
		List<WeiXinNews>  newList=new ArrayList<WeiXinNews>();
		for(String id:newsIds){
			if(id!=null&&!"".equals(id)){
				InfoNews	infoNews=infoNewsService.infoNewsGet(Long.parseLong(id));
				WeiXinNews news=new WeiXinNews();
				news.setAuthor(infoNews.getCreateUser());
				news.setContent(infoNews.getContents());
				news.setContent_source_url("");
				news.setDigest(infoNews.getTitleDesc());
				news.setInfoNewsId(infoNews.getId());
				news.setShow_cover_pic(false);
				InfoImages infoImages=infoImagesService.infoImagesGet(infoNews.getImageId());
				if("0".equals(infoImages.getIsUploadwx())||infoImages.getIsUploadwx()==null){
					File file=new File(infoImages.getRealUrl());
					if(file!=null&&file.exists()){
						try{

							ApiResult r=MediaApi.addMaterial(file, "图片", "图片");
							Integer errcode=r.getErrorCode();
							if(errcode==0||errcode==null){

								String media_id=r.getStr("media_id");
								String url=r.getStr("url");
								infoImages.setIsUploadwx("1");
								infoImages.setWxMediaId(media_id);
								infoImages.setWxUrl(url);
								infoImagesService.infoImagesUpdate(infoImages);
							}else{
								String errmsg=r.getStr("errmsg");
								modelMap.put("callbackUrl", "admin/infoNewsNotInQuery");
								modelMap.put("para", "?weiXinNewsRS.weiXinNewListId="+weiXinNewsRS.getWeiXinNewListId());
								modelMap.put("message", "图片上传微信中出错。"+"错误代码："+errcode+"，信息："+errmsg);
								return new ModelAndView("error",modelMap);
							}
						}catch(Exception e){

							modelMap.put("callbackUrl", "admin/infoNewsNotInQuery");
							modelMap.put("para", "?weiXinNewsRS.weiXinNewListId="+weiXinNewsRS.getWeiXinNewListId());
							modelMap.put("message", e.getMessage());
							return new ModelAndView("error",modelMap);
						}
						
					}else{


						modelMap.put("callbackUrl", "admin/infoNewsNotInQuery");
						modelMap.put("para", "?weiXinNewsRS.weiXinNewListId="+weiXinNewsRS.getWeiXinNewListId());
						modelMap.put("message", "图片上传微信中出错，系统找不到文件的路径");
						return new ModelAndView("error",modelMap);
						
						
					}
				}
				StringBuilder sb=new StringBuilder();
				sb.append("http://");
				sb.append(request.getServerName());
				Integer port=request.getServerPort();
				if(port!=80){
					sb.append(":"+request.getServerPort());
				}
				if(!"".equals(request.getContextPath())){
					sb.append(request.getContextPath());
				};
				
				sb.append("/upload.do");
				news.setContent_source_url(sb.toString());
				news.setThumb_media_id(infoImages.getWxMediaId());
				news.setTitle(infoNews.getTitle());
				news.setWeiXinNewListId(weiXinNewsRS.getWeiXinNewListId());
				newList.add(news);
			}
		}
		for(WeiXinNews weiXinNews:newList){
			weiXinNewsService.WeiXinNewsSave(weiXinNews);
		}
		modelMap.put("callbackUrl", "admin/infoNewsNotInQuery");
		modelMap.put("para", "?weiXinNewsRS.weiXinNewListId="+weiXinNewsRS.getWeiXinNewListId());
		modelMap.put("message", "修改成功");
		return new ModelAndView("success",modelMap);
	}
	
	@RequestMapping("/weiXinNewsEdit")
	public ModelAndView  weiXinNewsEdit(@ModelAttribute InfoNewsReq infoNewsReq,@ModelAttribute WeiXinNewsReq weiXinNewsReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		WeiXinNews w=weiXinNewsReq.getWeiXinNews();
		WeiXinNews weiXinNews=weiXinNewsService.WeiXinNewsGet(w.getId());
		weiXinNews.setContent(w.getContent());
		weiXinNewsService.WeiXinNewsUpdate(weiXinNews);
		modelMap.put("callbackUrl", "admin/infoNewsInQuery");
		modelMap.put("para", "?weiXinNewsRS.weiXinNewListId="+weiXinNews.getWeiXinNewListId());
		modelMap.put("message", "修改成功");
		return new ModelAndView("success",modelMap);
	}
	
	@RequestMapping("/infoNewsNotInQuery")
	public ModelAndView  infoNewsNotInQuery(@ModelAttribute InfoNewsReq infoNewsReq,@ModelAttribute WeiXinNewsReq weiXinNewsReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		InfoNewsRS infoNewsRS=infoNewsReq.getInfoNewsRS();
		WeiXinNewsRS weiXinNewsRS=weiXinNewsReq.getWeiXinNewsRS();
		modelMap.put("pageSize",infoNewsReq.getPageSize());
		modelMap.put("pageNo", infoNewsReq.getPageNo());
		modelMap.put("ctx", request.getContextPath());
		modelMap.put("infoNewsRS", infoNewsRS);
		modelMap.put("weiXinNewsRS", weiXinNewsRS);
		Page<InfoNews> page=weiXinNewsService.findInfoNewsNotInByPage(infoNewsReq.getPageNo(), infoNewsReq.getPageSize(),infoNewsRS,weiXinNewsRS);//.getMenuByAuthoritieId(principal.getName());
		modelMap.put("page",page);
		return new ModelAndView("Articles/notlist",modelMap);
	}
}
