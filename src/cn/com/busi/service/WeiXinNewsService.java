package cn.com.busi.service;

import cn.com.base.sys.common.Page;
import cn.com.busi.model.InfoNews;
import cn.com.busi.model.WeiXinNews;
import cn.com.busi.web.vo.InfoNewsRS;
import cn.com.busi.web.vo.WeiXinNewsRS;

public interface WeiXinNewsService {

	public Page<InfoNews> findInfoNewsInByPage(int pageNo, int pageSize,
			InfoNewsRS infoNewsRS,WeiXinNewsRS weiXinNewsRS);
	public Page<InfoNews> findInfoNewsNotInByPage(int pageNo, int pageSize,
			InfoNewsRS infoNewsRS,WeiXinNewsRS weiXinNewsRS);
	/**
	 * 分页查询
	 */
	public Page<WeiXinNews> findWeiXinNewsByPage(int pageNo,
			int pageSize, WeiXinNewsRS WeiXinNewsRS);

	/**
	 * 修改数据
	 */
	public WeiXinNews WeiXinNewsUpdate(WeiXinNews WeiXinNews);

	/**
	 * 新增数据
	 */
	public WeiXinNews WeiXinNewsSave(WeiXinNews WeiXinNews);

	/**
	 * 删除数据
	 */
	public void WeiXinNewsDelete(WeiXinNews WeiXinNews);

	/**
	 * 通过主键查询数据
	 */
	public WeiXinNews WeiXinNewsGet(Long id);

	/**
	 * 通过主键删除数据
	 */
	public void WeiXinNewsDelete(Long id);

	/**
	 * 通过对象查询数据
	 */
	public WeiXinNews WeiXinNewsGet(WeiXinNews WeiXinNews);
}
