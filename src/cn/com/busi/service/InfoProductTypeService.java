package cn.com.busi.service;

import cn.com.base.sys.common.Page;
import cn.com.busi.model.InfoProductType;
import cn.com.busi.web.vo.InfoProductTypeRS;

public interface InfoProductTypeService {
	/**
	 * 分页查询
	 */
	public Page<InfoProductType> findInfoProductTypeByPage(int pageNo, int pageSize,InfoProductTypeRS infoProductTypeRS);

	/**
	 * 修改数据
	 */
	public InfoProductType infoProductTypeUpdate(InfoProductType infoProductType);

	/**
	 * 新增数据
	 */
	public InfoProductType infoProductTypeSave(InfoProductType infoProductType);

	/**
	 * 删除数据
	 */
	public void infoProductTypeDelete(Long id);

	/**
	 * 通过主键删除数据
	 */

	public InfoProductType infoProductTypeGet(Long id);

}
