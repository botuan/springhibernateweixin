package cn.com.busi.model;

import java.util.Date;

public class WeiXinUser {
	private String openId;
	private String nickName;
	private String sex;
	private String city;
	private String country;
	private String headImgUrl;
	private String language;
	private String province;
	private Date subscribeTime;
	private String unionId;
	private String remark;
	private Integer groupId;
	
}
