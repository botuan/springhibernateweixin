package cn.com.base.sys.web.ctl;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;




import cn.com.base.sys.common.Page;
import cn.com.base.sys.common.Result;
import cn.com.base.sys.model.SysUsers;
import cn.com.base.sys.security.SessionManager;
import cn.com.base.sys.service.UserService;
import cn.com.base.sys.web.vo.UsersRS;
import cn.com.base.sys.web.vo.UsersReq;

@Controller
@RequestMapping("/admin")
public class UserCtl extends SessionManager{
	static Logger logger =Logger.getLogger(UserCtl.class);
	@Autowired
	private UserService userService;
	@RequestMapping("/userQuery")
	public ModelAndView  userQuery(@ModelAttribute UsersReq usersReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		//SysUsers user=(SysUsers)super.findLogonUser(request);
		UsersRS usersRS=usersReq.getUsersRS();
		modelMap.put("pageSize",usersReq.getPageSize());
		modelMap.put("pageNo", usersReq.getPageNo());
		modelMap.put("ctx", request.getContextPath());
		modelMap.put("usersRS", usersRS);
		Page<SysUsers> page=userService.findUserByPage("test", usersReq.getPageNo(), usersReq.getPageSize(),usersRS);//.getMenuByUserId(principal.getName());
		modelMap.put("page",page);
		return new ModelAndView("admin/manager/manager_list",modelMap);
	}
	@RequestMapping("/prepareUserAdd")
	public ModelAndView  prepareUserAdd(@ModelAttribute UsersReq usersReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		return new ModelAndView("admin/manager/manager_create",modelMap);
	}
	@RequestMapping("/userAdd")
	public  @ResponseBody Result    userAdd(@ModelAttribute UsersReq usersReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		SysUsers sysUsers=usersReq.getSysUsers();
		sysUsers.setUserAccount(sysUsers.getUserId());
		userService.userSave(sysUsers);
		Result r=new Result();
		r.setCode("0");
		r.setMessage("上传成功");
		return r;
	}
	@RequestMapping("/prepareUserEdit")
	public ModelAndView  prepareUserEdit(@ModelAttribute UsersReq usersReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		SysUsers sysUsers=usersReq.getSysUsers();
		sysUsers=userService.findByUserAccount(sysUsers.getUserId());
		modelMap.put("sysUsers", sysUsers);
		return new ModelAndView("admin/manager/manager_edit",modelMap);
	}
	@RequestMapping("/preparePassWordEdit")
	public ModelAndView  preparePassWordEdit(@ModelAttribute UsersReq usersReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		SysUsers user=(SysUsers)super.findLogonUser(request);
		SysUsers sysUsers=userService.findByUserAccount(user.getUserId());
		modelMap.put("sysUsers", sysUsers);
		return new ModelAndView("admin/user/passWordEdit",modelMap);
	}
	@RequestMapping("/passWordEdit")
	public ModelAndView  passWordEdit(@ModelAttribute UsersReq usersReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		logger.info("修改密码开始");
		super.adminLoginValidate(request);
		SysUsers user=(SysUsers)super.findLogonUser(request);
		logger.info(user.getUserId()+"正在修改密码开始");
		UsersRS usersRS=usersReq.getUsersRS();
		String userName=user.getUserId();
		SysUsers sysUsers=userService.findByUserAccount(userName);
		logger.info(user.getUserId()+"修改密码，原始密码是："+usersRS.getOdlUserPassword());
		logger.info(user.getUserId()+"修改密码，新密码是："+usersRS.getUserPassword());
		modelMap.put("sysUsers", sysUsers);
		modelMap.put("message", "修改密码成功");
		return new ModelAndView("admin/common/success",modelMap);
	}
	@RequestMapping("/userEdit")
	public  @ResponseBody Result    userEdit(@ModelAttribute UsersReq usersReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		SysUsers sysUsers=usersReq.getSysUsers();
		sysUsers.setUserAccount(sysUsers.getUserId());
		userService.userUpdate(sysUsers);
		Result r=new Result();
		r.setCode("0");
		r.setMessage("上传成功");
		return r;
	}
	@RequestMapping("/userDelete")
	public  @ResponseBody Result    userDelete(@ModelAttribute UsersReq usersReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		SysUsers sysUsers=usersReq.getSysUsers();
		userService.userDelete(sysUsers);
		Result r=new Result();
		r.setCode("0");
		r.setMessage("上传成功");
		return r;
	}
}
