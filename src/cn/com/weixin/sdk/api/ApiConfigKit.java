package cn.com.weixin.sdk.api;

import cn.com.weixin.sdk.cache.DefaultAccessTokenCache;
import cn.com.weixin.sdk.cache.IAccessTokenCache;
import cn.com.weixin.sdk.kit.PropKit;

/**
 * 将 ApiConfig 绑定到 ThreadLocal 的工具类，以方便在当前线程的各个地方获取 ApiConfig 对象： 1：如果控制器继承自
 * MsgController 该过程是自动的，详细可查看 MsgInterceptor 与之的配合 2：如果控制器继承自 ApiController
 * 该过程是自动的，详细可查看 ApiInterceptor 与之的配合 3：如果控制器没有继承自
 * MsgController、ApiController，则需要先手动调用
 * ApiConfigKit.setThreadLocalApiConfig(ApiConfig) 来绑定 apiConfig 到线程之上
 */
public class ApiConfigKit {

	private static ApiConfig ac;
	// 开发模式将输出消息交互 xml 到控制台
	private static boolean devMode = false;

	public static void setDevMode(boolean devMode) {
		ApiConfigKit.devMode = devMode;
	}

	public static boolean isDevMode() {
		return devMode;
	}

	public static void setApiConfig(ApiConfig apiConfig) {
		ac = apiConfig;
	}

	public static void removeApiConfig() {
		ac = null;
	}

	static {
		ac = new ApiConfig();
		// 配置微信 API 相关常量
		if (PropKit.getProp("a_weixin_config.txt") == null) {
			PropKit.use("a_weixin_config.txt");
		}
		ac.setToken(PropKit.get("token"));
		ac.setAppId(PropKit.get("appId"));
		ac.setAppSecret(PropKit.get("appSecret"));
		ApiConfigKit.setDevMode(PropKit.getBoolean("devMode"));
		/**
		 * 是否对消息进行加密，对应于微信平台的消息加解密方式： 1：true进行加密且必须配置 encodingAesKey
		 * 2：false采用明文模式，同时也支持混合模式
		 */
		ac.setEncryptMessage(PropKit.getBoolean("encryptMessage", false));
		ac.setEncodingAesKey(PropKit.get("encodingAesKey",
				"setting it in config file"));

	}

	public static ApiConfig getApiConfig() {
		// ApiConfig result = tl.get();
		ApiConfig result = ac;
		if (result == null)
			throw new IllegalStateException(
					"需要事先使用 ApiConfigKit.setThreadLocalApiConfig(apiConfig) 将 ApiConfig对象存入，才可以调用 ApiConfigKit.getApiConfig() 方法");
		return result;
	}

	static IAccessTokenCache accessTokenCache = new DefaultAccessTokenCache();

	public static void setAccessTokenCache(IAccessTokenCache accessTokenCache) {
		ApiConfigKit.accessTokenCache = accessTokenCache;
	}

	public static IAccessTokenCache getAccessTokenCache() {
		return ApiConfigKit.accessTokenCache;
	}

}