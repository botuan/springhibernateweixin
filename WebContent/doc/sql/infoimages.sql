/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50620
Source Host           : localhost:3306
Source Database       : mvc

Target Server Type    : MYSQL
Target Server Version : 50620
File Encoding         : 65001

Date: 2016-07-21 19:43:19
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `infoimages`
-- ----------------------------
DROP TABLE IF EXISTS `infoimages`;
CREATE TABLE `infoimages` (
  `Id` bigint(20) NOT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `CreateUser` varchar(255) DEFAULT NULL,
  `ImageDesc` varchar(255) DEFAULT NULL,
  `ImageUrl` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `isUploadwx` varchar(255) DEFAULT NULL,
  `realUrl` varchar(255) DEFAULT NULL,
  `wxMediaId` varchar(255) DEFAULT NULL,
  `wxUrl` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of infoimages
-- ----------------------------
INSERT INTO `infoimages` VALUES ('3', '2016-07-12 13:02:17', 'test', null, '/upload/image/QQ图片20160318144805.jpg', 'QQ图片20160318144805.jpg', '1', 'E:\\git\\weixin_web\\WebContent\\upload\\/image/QQ图片20160318144805.jpg', 'UQdSKDzksgIa5Z_K5hqDdYIYPEuF-kEzzHvv2hQH_mM', 'http://mmbiz.qpic.cn/mmbiz/IEL8T6C796zLlXqAVls6XfVGvEOicykEI8jTplZpDILNlMUTjmMpehPCRoFyzo3KalVytPmHemxzjlfl9jakSaQ/0?wx_fmt=jpeg', null);
INSERT INTO `infoimages` VALUES ('4', '2016-07-12 14:42:51', 'test', null, '/upload/image/222.jpg', '222.jpg', '1', 'E:\\git\\weixin_web\\WebContent\\upload\\/image/222.jpg', 'UQdSKDzksgIa5Z_K5hqDdVxrGsPni1ClStFNQ6s3FqQ', 'http://mmbiz.qpic.cn/mmbiz/IEL8T6C796zLlXqAVls6XfVGvEOicykEIs8EbXGKgFV75icEM4xVfXxBe3BkdfofGtAt42ib60ic1DWNDhcIMZtL8w/0?wx_fmt=jpeg', null);
INSERT INTO `infoimages` VALUES ('9', '2016-07-12 14:46:41', 'test', null, '/upload/image/2016-03-28_181639.png', '2016-03-28_181639.png', '0', 'E:\\git\\weixin_web\\WebContent\\upload\\/image/2016-03-28_181639.png', null, null, null);
INSERT INTO `infoimages` VALUES ('10', '2016-07-12 14:46:41', 'test', null, '/upload/image/2016-03-28_181738.png', '2016-03-28_181738.png', '0', 'E:\\git\\weixin_web\\WebContent\\upload\\/image/2016-03-28_181738.png', null, null, null);
INSERT INTO `infoimages` VALUES ('11', '2016-07-12 14:46:41', 'test', null, '/upload/image/2016-03-28_181700.png', '2016-03-28_181700.png', '0', 'E:\\git\\weixin_web\\WebContent\\upload\\/image/2016-03-28_181700.png', null, null, null);
INSERT INTO `infoimages` VALUES ('12', '2016-07-12 14:46:41', 'test', null, '/upload/image/2016-03-28_181811.png', '2016-03-28_181811.png', '0', 'E:\\git\\weixin_web\\WebContent\\upload\\/image/2016-03-28_181811.png', null, null, null);
INSERT INTO `infoimages` VALUES ('13', '2016-07-12 14:46:42', 'test', null, '/upload/image/2016-03-28_181831.png', '2016-03-28_181831.png', '0', 'E:\\git\\weixin_web\\WebContent\\upload\\/image/2016-03-28_181831.png', null, null, null);
INSERT INTO `infoimages` VALUES ('14', '2016-07-12 14:46:42', 'test', null, '/upload/image/2016-03-28_181852.png', '2016-03-28_181852.png', '0', 'E:\\git\\weixin_web\\WebContent\\upload\\/image/2016-03-28_181852.png', null, null, null);
INSERT INTO `infoimages` VALUES ('15', '2016-07-12 14:46:42', 'test', null, '/upload/image/2016-03-28_181909.png', '2016-03-28_181909.png', '0', 'E:\\git\\weixin_web\\WebContent\\upload\\/image/2016-03-28_181909.png', null, null, null);
INSERT INTO `infoimages` VALUES ('16', '2016-07-12 14:46:42', 'test', null, '/upload/image/2016-04-06_091623.png', '2016-04-06_091623.png', '0', 'E:\\git\\weixin_web\\WebContent\\upload\\/image/2016-04-06_091623.png', null, null, null);
INSERT INTO `infoimages` VALUES ('17', '2016-07-12 14:46:42', 'test', null, '/upload/image/2775423736_114362229.jpg', '2775423736_114362229.jpg', '0', 'E:\\git\\weixin_web\\WebContent\\upload\\/image/2775423736_114362229.jpg', null, null, null);
INSERT INTO `infoimages` VALUES ('18', '2016-07-12 14:46:42', 'test', null, '/upload/image/2866272995_86954140.jpg', '2866272995_86954140.jpg', '0', 'E:\\git\\weixin_web\\WebContent\\upload\\/image/2866272995_86954140.jpg', null, null, null);
INSERT INTO `infoimages` VALUES ('19', '2016-07-12 14:46:42', 'test', null, '/upload/image/20147319720.jpg', '20147319720.jpg', '0', 'E:\\git\\weixin_web\\WebContent\\upload\\/image/20147319720.jpg', null, null, null);
INSERT INTO `infoimages` VALUES ('20', '2016-07-12 14:46:42', 'test', null, '/upload/image/2009310134823229_2.jpg', '2009310134823229_2.jpg', '0', 'E:\\git\\weixin_web\\WebContent\\upload\\/image/2009310134823229_2.jpg', null, null, null);
