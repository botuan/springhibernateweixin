package cn.com.base.sys.common;

import java.io.Serializable;

public class ReqModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2181719080730451929L;
	
    private Integer pageNo=0;
    private Integer pageNum=0;
    private Integer pageSize=10;
    
    private String orderField;
    
    public String getOrderField() {
		return orderField;
	}

	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}

	private String orderDirection="DESC";
    
	public String getOrderDirection() {
		return orderDirection;
	}

	public void setOrderDirection(String orderDirection) {
		this.orderDirection = orderDirection;
	}

	public Integer getPageNo() {
		if(pageNo==null){
			pageNo=0;
		}
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageNum() {
		return pageNum;
	}

	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}

	

}
