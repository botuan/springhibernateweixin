package cn.com.base.sys.tlds;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.util.Assert;

import cn.com.base.sys.common.BusinessFactory;
import cn.com.base.sys.service.BusinessCodeService;

/**
 * 翻译区
 * 
 * @author Administrator
 *
 */
public class CodeInputEL {
	public static String translate(String codeType, String codeCode) {
		String serviceName="businessCodeService";
		if(codeType.indexOf(".")!=-1){
			serviceName=codeType.substring(0, codeType.indexOf("."));
		}
		BusinessCodeService service = BusinessFactory.getInstance().getService(serviceName);
		Assert.notNull(service,"服务类不能为空");
		String name = service.translate(codeType, codeCode);
		return name;
	}
	
	public static String subStr(String str, String len) {
		if(str==null){
			return "";
		}
		if(len==null||"".equals(len)){
			return str;
		}
		else {
			int lens=Integer.valueOf(len);
			if(str.length()<lens){
				return str;
			}else{
				return str.trim().substring(0, lens)+"...";
			}
		}
	}
	/**
	 * 格式化时间
	 * @param date
	 * @param format
	 * @return
	 */
	public static String formatDate(Date date, String format) {
		if(date==null){
			return "";
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date);
	}
}
