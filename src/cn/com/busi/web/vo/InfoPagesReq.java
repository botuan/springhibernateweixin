package cn.com.busi.web.vo;

import cn.com.base.sys.common.ReqModel;
import cn.com.busi.model.InfoPages;
/**
 * ${modelFieldBean.desc}
 * @author 谢林
 */
public class InfoPagesReq extends ReqModel {
	private static final long serialVersionUID = 1L;
	private InfoPages infoPages=new InfoPages();
	private InfoPagesRS infoPagesRS=new InfoPagesRS();
	public InfoPages getInfoPages() {
		return infoPages;
	}
	public void setInfoPages(InfoPages infoPages) {
		this.infoPages = infoPages;
	}
	public InfoPagesRS getInfoPagesRS() {
		return infoPagesRS;
	}
	public void setInfoPagesRS(InfoPagesRS infoPagesRS) {
		this.infoPagesRS = infoPagesRS;
	}
		
		
	/**
	 *主键
	 */
	private Long id=new Long(0);
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	    
}
