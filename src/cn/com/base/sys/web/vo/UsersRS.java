package cn.com.base.sys.web.vo;

public class UsersRS {

	//用户id
	private String userId;
	
	//用户账号 与 用户id相同，具有唯一性。
	private String userAccount;
	
	//中文用户名。
	private String userName;
	
	//密码原文 ＋ 用户名作为盐值 的字串经过Md5加密后形成的密文。
	private String userPassword;
	private String odlUserPassword;
	
	//用户备注
	private String userDesc;
	
	//是否能用。
	private Integer enabled;
	
	//是否是超级用户。
	private Integer issys;
	
	//用户所在的单位。
	private String userDept;
	
	//用户的职位：比如主任、经理等。
	private String userDuty;
	
	//该用户所负责的子系统
	private String subSystem;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserDesc() {
		return userDesc;
	}

	public void setUserDesc(String userDesc) {
		this.userDesc = userDesc;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public Integer getIssys() {
		return issys;
	}

	public void setIssys(Integer issys) {
		this.issys = issys;
	}

	public String getUserDept() {
		return userDept;
	}

	public void setUserDept(String userDept) {
		this.userDept = userDept;
	}

	public String getUserDuty() {
		return userDuty;
	}

	public void setUserDuty(String userDuty) {
		this.userDuty = userDuty;
	}

	public String getSubSystem() {
		return subSystem;
	}

	public void setSubSystem(String subSystem) {
		this.subSystem = subSystem;
	}

	public String getOdlUserPassword() {
		return odlUserPassword;
	}

	public void setOdlUserPassword(String odlUserPassword) {
		this.odlUserPassword = odlUserPassword;
	}
}
