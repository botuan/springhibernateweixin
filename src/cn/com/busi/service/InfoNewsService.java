package cn.com.busi.service;

import cn.com.base.sys.common.Page;
import cn.com.busi.model.InfoNews;
import cn.com.busi.web.vo.InfoNewsRS;

/**
 * 新闻通知的业务逻辑接口
 * 
 * @author 谢林
 */
public interface InfoNewsService {
	/**
	 * 分页查询
	 */
	public Page<InfoNews> findInfoNewsByPage(int pageNo, int pageSize,
			InfoNewsRS infoNewsRS);

	/**
	 * 修改数据
	 */
	public InfoNews infoNewsUpdate(InfoNews infoNews);

	/**
	 * 新增数据
	 */
	public InfoNews infoNewsSave(InfoNews infoNews);

	/**
	 * 删除数据
	 */
	public void infoNewsDelete(InfoNews infoNews);

	/**
	 * 通过主键查询数据
	 */
	public InfoNews infoNewsGet(Long id);
	/**
	 * 通过主键删除数据
	 */
	public void infoNewsDelete(Long id);

	/**
	 * 通过对象查询数据
	 */
	public InfoNews infoNewsGet(InfoNews infoNews);

}
