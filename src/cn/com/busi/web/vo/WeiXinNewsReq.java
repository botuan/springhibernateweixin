package cn.com.busi.web.vo;

import cn.com.base.sys.common.ReqModel;
import cn.com.busi.model.WeiXinNews;

public class WeiXinNewsReq  extends ReqModel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private WeiXinNewsRS weiXinNewsRS;
	private WeiXinNews weiXinNews;
	private Long id;
	public WeiXinNewsRS getWeiXinNewsRS() {
		return weiXinNewsRS;
	}
	public void setWeiXinNewsRS(WeiXinNewsRS weiXinNewsRS) {
		this.weiXinNewsRS = weiXinNewsRS;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public WeiXinNews getWeiXinNews() {
		return weiXinNews;
	}
	public void setWeiXinNews(WeiXinNews weiXinNews) {
		this.weiXinNews = weiXinNews;
	}
	
	private String newsId;
	public String getNewsId() {
		return newsId;
	}
	public void setNewsId(String newsId) {
		this.newsId = newsId;
	}
	

}
