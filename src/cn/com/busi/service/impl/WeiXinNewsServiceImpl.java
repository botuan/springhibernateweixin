package cn.com.busi.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.base.sys.common.BaseJpaDao;
import cn.com.base.sys.common.Page;
import cn.com.base.sys.common.Query;
import cn.com.base.sys.util.Strings;
import cn.com.busi.model.InfoNews;
import cn.com.busi.model.WeiXinNews;
import cn.com.busi.service.WeiXinNewsService;
import cn.com.busi.web.vo.InfoNewsRS;
import cn.com.busi.web.vo.WeiXinNewsRS;

@Service(value = "weiXinNewsService")
public class WeiXinNewsServiceImpl implements WeiXinNewsService {
	@Autowired
	private BaseJpaDao baseJpaDao;
	static Logger log = Logger.getLogger(WeiXinNewsServiceImpl.class);

	@Override
	public Page<WeiXinNews> findWeiXinNewsByPage(int pageNo, int pageSize,
			WeiXinNewsRS WeiXinNewsRS) {
		log.info("正在执行  getInfoNewsByPage()");
		Query query = baseJpaDao.createQuery(WeiXinNews.class);
		this.initQuery(query, WeiXinNewsRS);
		Page page = baseJpaDao.queryPage(query, pageNo, pageSize);
		log.info("执行 getInfoNewsByPage()完毕，正在返回");
		return page;
	}
	private void initQuery(Query query, WeiXinNewsRS weiXinNewsRS) {
		log.info("正在执行  initQuery()");
		if (weiXinNewsRS != null) {
			if (!Strings.isNullOrEmpty(weiXinNewsRS.getWeiXinNewListId())) {
				query.eq("weiXinNewListId", weiXinNewsRS.getWeiXinNewListId());
			}
		}

		log.info("执行 initQuery完毕，正在返回");
	}

	@Override
	public WeiXinNews WeiXinNewsUpdate(WeiXinNews weiXinNews) {
		baseJpaDao.update(weiXinNews);
		return weiXinNews;
	}

	@Override
	public WeiXinNews WeiXinNewsSave(WeiXinNews weiXinNews) {
		baseJpaDao.save(weiXinNews);
		return weiXinNews;
	}

	@Override
	public void WeiXinNewsDelete(WeiXinNews weiXinNews) {
		WeiXinNewsDelete(weiXinNews.getId());

	}

	@Override
	public WeiXinNews WeiXinNewsGet(Long id) {
		WeiXinNews weiXinNews=baseJpaDao.get(WeiXinNews.class, id);
		return weiXinNews;
	}

	@Override
	public void WeiXinNewsDelete(Long id) {
		List<Long> ids=new ArrayList<Long>();
		ids.add(id);
		baseJpaDao.delete(WeiXinNews.class, ids);

	}

	@Override
	public WeiXinNews WeiXinNewsGet(WeiXinNews weiXinNews) {
		return WeiXinNewsGet(weiXinNews.getId());
	}
	public Page<InfoNews> findInfoNewsInByPage(int pageNo, int pageSize,
			InfoNewsRS infoNewsRS,WeiXinNewsRS weiXinNewsRS) {
		log.info("正在执行  getInfoNewsByPage()");
		Page<WeiXinNews> p=this.findWeiXinNewsByPage(0, Integer.MAX_VALUE, weiXinNewsRS);
		List<Long> ids=new ArrayList<Long>();
		for(WeiXinNews s:p.getResult()){
			if(s!=null){

				ids.add(s.getInfoNewsId());
			}
		}
		Page<InfoNews> page=null;
		if(ids.size()>0){

			Query query = baseJpaDao.createQuery(InfoNews.class);
			query.in("id", ids);
			 page = baseJpaDao.queryPage(query, pageNo, pageSize);
		}else{
			page= new Page<InfoNews>();
		}
		log.info("执行 getInfoNewsByPage()完毕，正在返回");
		return page;
	}
	public Page<InfoNews> findInfoNewsNotInByPage(int pageNo, int pageSize,
			InfoNewsRS infoNewsRS,WeiXinNewsRS weiXinNewsRS) {
		log.info("正在执行  getInfoNewsByPage()");
		Page<WeiXinNews> p=this.findWeiXinNewsByPage(0, Integer.MAX_VALUE, weiXinNewsRS);
		List<Long> ids=new ArrayList<Long>();
		for(WeiXinNews s:p.getResult()){
			if(s!=null){

				ids.add(s.getInfoNewsId());
			}
		}
		Page<InfoNews> page=null;
		Query query = baseJpaDao.createQuery(InfoNews.class);
		if(ids.size()>0){

			query.notIn("id", ids);
		}
		 page = baseJpaDao.queryPage(query, pageNo, pageSize);
		log.info("执行 getInfoNewsByPage()完毕，正在返回");
		return page;
	}
}
