package cn.com.base.sys.security;

import javax.servlet.http.HttpServletRequest;

public interface SessionManagerService {

	boolean confirmLogin(HttpServletRequest request);

	void loginValidate(HttpServletRequest request);

	void adminLoginValidate(HttpServletRequest request);

	Object findLogonUser(HttpServletRequest request);

}
