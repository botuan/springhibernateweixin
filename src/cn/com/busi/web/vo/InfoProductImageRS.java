package cn.com.busi.web.vo;

public class InfoProductImageRS {
	private Long id;
	private Long productId;
	private Long imagesId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Long getImagesId() {
		return imagesId;
	}
	public void setImagesId(Long imagesId) {
		this.imagesId = imagesId;
	}

}
