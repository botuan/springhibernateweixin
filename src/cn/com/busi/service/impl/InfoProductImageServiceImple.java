package cn.com.busi.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.base.sys.common.BaseJpaDao;
import cn.com.base.sys.common.Page;
import cn.com.base.sys.common.Query;
import cn.com.base.sys.util.Strings;
import cn.com.busi.model.InfoProductImage;
import cn.com.busi.service.InfoProductImageService;
import cn.com.busi.web.vo.InfoProductImageRS;

@Service(value = "InfoProductImageService")
public class InfoProductImageServiceImple implements InfoProductImageService {
	@Autowired
	/**
	 * jpa的操作类
	 */
	private BaseJpaDao baseJpaDao;
	static Logger log = Logger.getLogger(InfoImagesServiceImpl.class);

	@Override
	public Page<InfoProductImage> findInfoNewsByPage(int pageNo, int pageSize,
			InfoProductImageRS infoProductImageRS) {
		log.info("正在执行  getInfoImagesByPage()");
		Query query = baseJpaDao.createQuery(InfoProductImage.class);
		this.initQuery(query, infoProductImageRS);
		Page page = baseJpaDao.queryPage(query, pageNo, pageSize);
		log.info("执行 getInfoImagesByPage()完毕，正在返回");
		return page;
	}

	@Override
	public InfoProductImage infoNewsUpdate(InfoProductImage infoProductImage) {
		baseJpaDao.update(infoProductImage);
		return infoProductImage;
	}

	/**
	 * 初始化查询条件
	 */
	private void initQuery(Query query, InfoProductImageRS infoProductImageRS) {
		log.info("正在执行  initQuery()");
		if (infoProductImageRS != null) {
			if (!Strings.isNullOrEmpty(infoProductImageRS.getId())) {
				query.eq("id", infoProductImageRS.getId());
			}
			if (!Strings.isNullOrEmpty(infoProductImageRS.getProductId())) {
				query.eq("productId", infoProductImageRS.getProductId());
			}
			if (!Strings.isNullOrEmpty(infoProductImageRS.getImagesId())) {
				query.eq("imageId", infoProductImageRS.getImagesId());
			}
		}
		log.info("执行 initQuery完毕，正在返回");
	}

	@Override
	public InfoProductImage infoNewsSave(InfoProductImage infoProductImage) {
		baseJpaDao.save(infoProductImage);
		return infoProductImage;
	}

	@Override
	public void infoNewsDelete(Long id) {
		List<Long> list = new ArrayList<Long>();
		list.add(id);
		baseJpaDao.delete(InfoProductImage.class, list);

	}

	@Override
	public InfoProductImage infoNewsGet(Long id) {
		InfoProductImage info = baseJpaDao.get(InfoProductImage.class, id);
		return info;
	}

	@Override
	public InfoProductImage findInfoNew(Long productId, Long imagesId) {
		InfoProductImageRS infoProductImageRS = new InfoProductImageRS();
		infoProductImageRS.setImagesId(imagesId);
		infoProductImageRS.setProductId(productId);
		Page<InfoProductImage> page = this.findInfoNewsByPage(0, 10,
				infoProductImageRS);
		if (page == null)
			return null;
		if (page.getResult() == null)
			return null;
		return page.getResult().get(0);
	}

}
