package cn.com.busi.web.vo;

import cn.com.base.sys.common.ReqModel;
import cn.com.busi.model.InfoProduct;
/**
 * ${modelFieldBean.desc}
 * @author 谢林
 */
public class InfoProductReq extends ReqModel {
	private static final long serialVersionUID = 1L;
	private InfoProduct infoProduct=new InfoProduct();
	private InfoProductRS infoProductRS=new InfoProductRS();
	public InfoProduct getInfoProduct() {
		return infoProduct;
	}
	public void setInfoProduct(InfoProduct infoProduct) {
		this.infoProduct = infoProduct;
	}
	public InfoProductRS getInfoProductRS() {
		return infoProductRS;
	}
	public void setInfoProductRS(InfoProductRS infoProductRS) {
		this.infoProductRS = infoProductRS;
	}
	/**
	 *主键
	 */
	private Long id=new Long(0);
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
		
	    
}
