package cn.com.busi.web.ctl;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.com.base.sys.common.Page;
import cn.com.base.sys.common.Result;
import cn.com.base.sys.security.SessionManager;
import cn.com.busi.model.InfoProductType;
import cn.com.busi.service.InfoProductTypeService;
import cn.com.busi.web.vo.InfoProductTypeRS;
import cn.com.busi.web.vo.InfoProductTypeReq;

@Controller
@RequestMapping("/admin")
public class InfoProductTypeCtl extends SessionManager{
	/**
	 *InfoProduct分页查询
	 */
	@RequestMapping("/infoProductTypeQuery")
	public ModelAndView  InfoProductTypeQuery(@ModelAttribute InfoProductTypeReq infoProductTypeReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		InfoProductTypeRS infoProductTypeRS=infoProductTypeReq.getInfoProductTypeRS();
		modelMap.put("pageSize",infoProductTypeReq.getPageSize());
		modelMap.put("pageNo", infoProductTypeReq.getPageNo());
		modelMap.put("ctx", request.getContextPath());
		modelMap.put("infoProductTypeRS", infoProductTypeRS);
		Page<InfoProductType> page=infoProductTypeService.findInfoProductTypeByPage(infoProductTypeReq.getPageNo(), infoProductTypeReq.getPageSize(),infoProductTypeRS);//.getMenuByAuthoritieId(principal.getName());
		modelMap.put("page",page);
		return new ModelAndView("admin/producttype/producttype_list",modelMap);
	}
	@Autowired
	private InfoProductTypeService infoProductTypeService;
	/**
	 *InfoProduct打开新增界面
	 */
	@RequestMapping("/prepareInfoProductTypeAdd")
	public ModelAndView  prepareInfoProductAdd(@ModelAttribute  InfoProductTypeReq infoProductTypeReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		Page page=infoProductTypeService.findInfoProductTypeByPage(0, Integer.MAX_VALUE, null);
		if(page!=null){
			modelMap.put("infoProductTypeList", page.getResult());
		}
		return new ModelAndView("admin/producttype/producttype_create",modelMap);
	}
	/**
	 *InfoProduct新增保存
	 */
	@RequestMapping("/infoProductTypeAdd")
	public @ResponseBody Result   infoProductAdd(@ModelAttribute  InfoProductTypeReq infoProductTypeReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		
		InfoProductType infoProductRS=infoProductTypeReq.getInfoProductType();
		infoProductRS.setCreateUser("test");
		infoProductRS.setUpdateUser("test");
		infoProductTypeService.infoProductTypeSave(infoProductRS);
		Result r=new Result();
		r.setCode("0");
		r.setMessage("操作成功");
		return r;
	}
	/**
	 *InfoProduct打开修改界面
	 */
	@RequestMapping("/prepareInfoProductTypeEdit")
	public ModelAndView  prepareInfoProductEdit(@ModelAttribute  InfoProductTypeReq infoProductTypeReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		InfoProductType infoProductType=infoProductTypeReq.getInfoProductType();
		infoProductType=infoProductTypeService.infoProductTypeGet(infoProductType.getId());
		
		modelMap.put("infoProductType", infoProductType);
		return new ModelAndView("admin/producttype/producttype_edit",modelMap);
	}
	@RequestMapping("/prepareInfoProductTypeUpLoad")
	public ModelAndView  prepareInfoProductTypeUpLoad(@ModelAttribute  InfoProductTypeReq infoProductTypeReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		
		InfoProductType infoProductType=infoProductTypeReq.getInfoProductType();
		infoProductType=infoProductTypeService.infoProductTypeGet(infoProductType.getId());
		modelMap.put("infoProductType", infoProductType);
		return new ModelAndView("admin/producttype/producttype_upload",modelMap);
	}
	/**
	 *InfoProduct修改保存
	 */
	@RequestMapping("/infoProductTypeEdit")
	public @ResponseBody Result   infoProductTypeEdit(@ModelAttribute  InfoProductTypeReq infoProductTypeReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		
		InfoProductType infoProductType=infoProductTypeReq.getInfoProductType();
		infoProductType.setUpdateUser("test");
		infoProductType.setUpdateDate(new Date());
		infoProductTypeService.infoProductTypeUpdate(infoProductType);
		Result r=new Result();
		r.setCode("0");
		r.setMessage("操作成功");
		return r;
	}
	/**
	 *InfoProduct删除操作
	 */
	@RequestMapping("/infoProductTypeDelete")
	public @ResponseBody Result  infoProductTypeDelete(@ModelAttribute  InfoProductTypeReq infoProductTypeReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		InfoProductType infoProductType=infoProductTypeReq.getInfoProductType();
		infoProductTypeService.infoProductTypeDelete(infoProductType.getId());	
		Result r=new Result();
		r.setCode("0");
		r.setMessage("删除成功");
		return r;
	}
}
