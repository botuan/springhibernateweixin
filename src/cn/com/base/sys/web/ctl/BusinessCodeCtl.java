package cn.com.base.sys.web.ctl;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cn.com.base.sys.common.Page;
import cn.com.base.sys.model.BusinessCode;
import cn.com.base.sys.service.BusinessCodeService;
import cn.com.base.sys.web.vo.BusinessCodeRS;
import cn.com.base.sys.web.vo.BusinessCodeReq;
/**
 *BusinessCode 的控制类
 */
@Controller
public class BusinessCodeCtl {
	@Autowired
	/**
	 *BusinessCode逻辑操作接口
	 */
	private BusinessCodeService businessCodeService;
	/**
	 *BusinessCode分页查询
	 */
	@RequestMapping("/businessCodeQuery")
	public ModelAndView  businessCodeQuery(@ModelAttribute BusinessCodeReq businessCodeReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		Principal principal=request.getUserPrincipal();
		BusinessCodeRS businessCodeRS=businessCodeReq.getBusinessCodeRS();
		modelMap.put("pageSize",businessCodeReq.getPageSize());
		modelMap.put("pageNo", businessCodeReq.getPageNo());
		modelMap.put("ctx", request.getContextPath());
		modelMap.put("businessCodeRS", businessCodeRS);
		Page<BusinessCodeRS> page=businessCodeService.findBusinessCodeByPage(businessCodeReq.getPageNo(), businessCodeReq.getPageSize(),businessCodeRS);//.getMenuByAuthoritieId(principal.getName());
		
		modelMap.put("page",page);
		return new ModelAndView("admin/businesscode/businessCodeMain",modelMap);
	}
	/**
	 *BusinessCode打开新增界面
	 */
	@RequestMapping("/prepareBusinessCodeAdd")
	public ModelAndView  prepareBusinessCodeAdd(@ModelAttribute BusinessCodeReq businessCodeReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		Principal principal=request.getUserPrincipal();
		return new ModelAndView("admin/businesscode/businessCodeAdd",modelMap);
	}
	/**
	 *BusinessCode新增保存
	 */
	@RequestMapping("/businessCodeAdd")
	public ModelAndView  businessCodeAdd(@ModelAttribute BusinessCodeReq businessCodeReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		Principal principal=request.getUserPrincipal();
		BusinessCode businessCode=businessCodeReq.getBusinessCode();
		businessCodeService.businessCodeSave(businessCode);
		return new ModelAndView("ajaxDone");
	}
	/**
	 *BusinessCode打开修改界面
	 */
	@RequestMapping("/prepareBusinessCodeEdit")
	public ModelAndView  prepareBusinessCodeEdit(@ModelAttribute BusinessCodeReq businessCodeReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		Principal principal=request.getUserPrincipal();
		//BusinessCode businessCode=businessCodeReq.getBusinessCode();
		//businessCode=businessCodeService.businessCodeGet(businessCode);
		
		
				
		Long businessCodeId=businessCodeReq.getBusinessCodeId();
		BusinessCode businessCode=businessCodeService.businessCodeGet(businessCodeId);
				
		
		modelMap.put("businessCode", businessCode);
		return new ModelAndView("admin/businesscode/businessCodeEdit",modelMap);
	}
	/**
	 *BusinessCode修改保存
	 */
	@RequestMapping("/businessCodeEdit")
	public ModelAndView  businessCodeEdit(@ModelAttribute BusinessCodeReq businessCodeReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		Principal principal=request.getUserPrincipal();
		BusinessCode businessCode=businessCodeReq.getBusinessCode();
		businessCodeService.businessCodeUpdate(businessCode);
		return new ModelAndView("ajaxDone");
	}
	/**
	 *BusinessCode删除操作
	 */
	@RequestMapping("/businessCodeDelete")
	public ModelAndView  businessCodeDelete(@ModelAttribute BusinessCodeReq businessCodeReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		Principal principal=request.getUserPrincipal();
				
		Long businessCodeId=businessCodeReq.getBusinessCodeId();
		businessCodeService.businessCodeDelete(businessCodeId);
				
		return new ModelAndView("ajaxDone");
	}
}
