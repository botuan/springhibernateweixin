package cn.com.base.sys.web.ctl;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;




import cn.com.base.sys.model.SysUsers;
import cn.com.base.sys.security.SessionManager;
import cn.com.base.sys.service.UserService;
import cn.com.base.sys.util.Strings;
import cn.com.base.sys.web.vo.UsersRS;
import cn.com.base.sys.web.vo.UsersReq;

@Controller
public class LoginCtl extends SessionManager  {
	@Autowired
	private UserService userService;
	@RequestMapping("/admin/main")
	public ModelAndView login(HttpServletRequest request,HttpServletResponse response, ModelMap modelMap) {
		super.adminLoginValidate(request);
		 /**普通转向  */
	   return new ModelAndView("admin/main",modelMap);  
   } 
	
	@RequestMapping("/manager/logout")
	public String logout(HttpServletRequest request,HttpServletResponse response, ModelMap modelMap) {  
	   request.getSession().invalidate();
	   System.out.println("退出登录。。。。。。");
	   /**重定向*/
	   return "redirect:/adminLogin.jsp";
    }  

	@RequestMapping("/adminLogin")
	public ModelAndView adminLogin(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap,HttpSession session) {
		String name=request.getParameter("name");
		String pawd=request.getParameter("password");
		if(!Strings.isNullOrEmpty(name)&&!Strings.isNullOrEmpty(pawd)){
			SysUsers sysUsers=userService.findByUserId(name);
			if(sysUsers!=null){
				if(pawd.equals(sysUsers.getUserPassword())){
					session.setAttribute(SessionManager.loginStatus, SessionManager.statusYes);
					session.setAttribute(SessionManager.loginType,SessionManager.typeAdmin);
					session.setAttribute(SessionManager.user, sysUsers);
					System.out.println("session:"+session.getId());
					return new ModelAndView("admin/main", modelMap);
				}
			}
		}
		return new ModelAndView("admin/adminLogin", modelMap);
	}
	@RequestMapping("/manager/logininit")
	public String logininit(@ModelAttribute UsersReq usersReq,HttpServletRequest request,HttpServletResponse response, ModelMap modelMap) {  
	   
	   System.out.println("退出登录。。。。。。");
	   /**重定向*/
	   UsersRS rs=usersReq.getUsersRS();
	   if(rs!=null&&!Strings.isNullOrEmpty(rs.getUserName())&&!Strings.isNullOrEmpty(rs.getUserPassword())){
		   request.getSession().setAttribute("user", rs);

		   return "redirect:/index.jsp";
	   }
	   return "redirect:/login.jsp";
    }  
	
	private void writeJson(HttpServletResponse response,String json){
		try {
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);
			response.getWriter().flush();
			response.getWriter().close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@RequestMapping("/manager/index")
	public ModelAndView index(@ModelAttribute UsersReq usersReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		return new ModelAndView("/admin/index",modelMap);
		
	}
	/*用户注册界面*/
	@RequestMapping("/manager/register")
	public ModelAndView  prepareUserAdd(@ModelAttribute UsersReq usersReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		UsersRS userRs =  usersReq.getUsersRS();
		SysUsers users =  usersReq.getSysUsers();
			String view="registerSuccess";
				String urlStr="main.do";
		if(Strings.isNullOrEmpty(userRs.getUserName())){
			urlStr="register.jsp";
			//view="ajaxError";
			modelMap.put("message", "用名称不为空！");
		}else if(!userRs.getUserPassword().equals(userRs.getOdlUserPassword())){
			//view="ajaxError";
			urlStr="register.jsp";
			modelMap.put("message", "两次密码不相同！");
		}else{
			urlStr="main.do";
			users.setUserId(userRs.getUserName());
			users.setUserName(userRs.getUserName());
			users.setUserPassword(userRs.getUserPassword());
			users.setEnabled(1);
			users.setIssys(0);
			users.setUserAccount(userRs.getUserName());
			users.setSubSystem(userRs.getUserName());
			userService.userSave(users);
			//view="ajaxDone";
			modelMap.put("message", "注册成功，请到登陆页登陆！");
			
		}
		modelMap.put("urlStr", urlStr);
		return new ModelAndView(view,modelMap);
	}



}
