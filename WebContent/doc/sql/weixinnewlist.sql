/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50620
Source Host           : localhost:3306
Source Database       : mvc

Target Server Type    : MYSQL
Target Server Version : 50620
File Encoding         : 65001

Date: 2016-07-21 19:44:18
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `weixinnewlist`
-- ----------------------------
DROP TABLE IF EXISTS `weixinnewlist`;
CREATE TABLE `weixinnewlist` (
  `id` bigint(20) NOT NULL,
  `digest` varchar(255) DEFAULT NULL,
  `isOk` tinyint(1) DEFAULT NULL,
  `media_id` varchar(255) DEFAULT NULL,
  `send` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of weixinnewlist
-- ----------------------------
INSERT INTO `weixinnewlist` VALUES ('1', 'sdfasdf', null, 'UQdSKDzksgIa5Z_K5hqDdcBRDGN2MKcSkFJewXj376w', null);
INSERT INTO `weixinnewlist` VALUES ('2', 'dasfasdf', null, 'UQdSKDzksgIa5Z_K5hqDdW-EHpUs7NUA5mmvP84fD5s', null);
INSERT INTO `weixinnewlist` VALUES ('3', 'sdafasdf', null, null, null);
INSERT INTO `weixinnewlist` VALUES ('4', 'sadfasdfa', null, null, null);
INSERT INTO `weixinnewlist` VALUES ('5', 'sadfasdfas', null, null, null);
INSERT INTO `weixinnewlist` VALUES ('6', 'adsfasdfasdgdsag', null, null, null);
INSERT INTO `weixinnewlist` VALUES ('7', 'asdgasdgasgda', null, null, null);
INSERT INTO `weixinnewlist` VALUES ('8', 'sadgsadgfasdg', null, null, null);
INSERT INTO `weixinnewlist` VALUES ('9', 'sdfsadghdfafdsa', null, null, null);
INSERT INTO `weixinnewlist` VALUES ('10', 'hfdshfad4eq32', null, null, null);
INSERT INTO `weixinnewlist` VALUES ('11', 'asheadfasdayh', null, null, null);
