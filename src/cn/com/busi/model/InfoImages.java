package cn.com.busi.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;

/**
 *  图片的实体类
 * @return
 */ 
@Entity
@TableGenerator(name = "SYSSEQUENCE", table = "SYSSEQUENCE", pkColumnName = "GenName", valueColumnName = "GenValue", pkColumnValue = "InfoImages_PK", allocationSize = 1)
@Table(name = "InfoImages")
public class InfoImages implements java.io.Serializable {
	private String name;

	@Column(name = "name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	private static final long serialVersionUID = 1L;
	private Long id;
	/**
	 * 返回 主键
	 * @return
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "SYSSEQUENCE")
	@Column(name = "Id")
	public Long getId (){
	 return id;
	}
	/**
	 * 设置 主键
	 * @param id
	 */
	public void setId (Long id){
	 this.id=id;
	}
	private String type;
	

	@Column(name = "type")
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	private String imageDesc;
	/**
	 * 返回 图片描述
	 * @return
	 */
	@Column(name = "ImageDesc")
	public String getImageDesc (){
	 return imageDesc;
	}
	/**
	 * 设置 图片描述
	 * @param imageDesc
	 */
	public void setImageDesc (String imageDesc){
	 this.imageDesc=imageDesc;
	}
	private String createUser;
	/**
	 * 返回 图片上传人
	 * @return
	 */
	@Column(name = "CreateUser")
	public String getCreateUser (){
	 return createUser;
	}
	/**
	 * 设置 图片上传人
	 * @param createUser
	 */
	public void setCreateUser (String createUser){
	 this.createUser=createUser;
	}
	private String imageUrl;
	/**
	 * 返回 图片地址
	 * @return
	 */
	@Column(name = "ImageUrl")
	public String getImageUrl (){
	 return imageUrl;
	}
	/**
	 * 设置 图片地址
	 * @param imageUrl
	 */
	public void setImageUrl (String imageUrl){
	 this.imageUrl=imageUrl;
	}
	private String realUrl;
	
	/**
	 * 返回 存放地址
	 * @return
	 */
	@Column(name = "realUrl")
	public String getRealUrl (){
	 return realUrl;
	}
	/**
	 * 设置 存放地址
	 * @param imageUrl
	 */
	public void setRealUrl (String realUrl){
	 this.realUrl=realUrl;
	}
	
	private Date createDate;
	/**
	 * 返回 图片上传时间
	 * @return
	 */
	@Column(name = "CreateDate")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getCreateDate (){
	 return createDate;
	}
	/**
	 * 设置 图片上传时间
	 * @param createDate
	 */
	public void setCreateDate (Date createDate){
	 this.createDate=createDate;
	}

	private String isUploadwx;
	private String wxMediaId;
	private String wxUrl;

	/**
	 * 是否上传到了微信
	 * @return
	 */
	@Column(name = "isUploadwx")
	public String getIsUploadwx() {
		return isUploadwx;
	}
	public void setIsUploadwx(String isUploadwx) {
		this.isUploadwx = isUploadwx;
	}
	/**
	 * 上传到了微信时的媒体编号
	 * @return
	 */
	@Column(name = "wxMediaId")
	public String getWxMediaId() {
		return wxMediaId;
	}
	public void setWxMediaId(String wxMediaId) {
		this.wxMediaId = wxMediaId;
	}
	/**
	 * 在微信中路径
	 * @return
	 */
	@Column(name = "wxUrl")
	public String getWxUrl() {
		return wxUrl;
	}
	public void setWxUrl(String wxUrl) {
		this.wxUrl = wxUrl;
	}
	
	
	
	
	
}