/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50620
Source Host           : localhost:3306
Source Database       : mvc

Target Server Type    : MYSQL
Target Server Version : 50620
File Encoding         : 65001

Date: 2016-07-21 19:43:40
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `infopages`
-- ----------------------------
DROP TABLE IF EXISTS `infopages`;
CREATE TABLE `infopages` (
  `Id` bigint(20) NOT NULL,
  `Content` varchar(15) NOT NULL,
  `CreateDate` datetime NOT NULL,
  `CreateUser` varchar(15) NOT NULL,
  `name` varchar(15) NOT NULL,
  `pageDescription` varchar(2000) DEFAULT NULL,
  `pageKeyWords` varchar(2000) DEFAULT NULL,
  `Type` varchar(15) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of infopages
-- ----------------------------
