package cn.com.busi.web.vo;


/**
 * 用户表
 * @author 谢林
 */
public class InfoCustomerRS implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String userName;
	private String confirmPassword;
	
	
	
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * 返回 邮箱
	 */
	private String email;
	/**
	 * 返回 邮箱
	 * @return
	 */
	public String getEmail (){
	 return email;
	}
	/**
	 * 设置 邮箱
	 * @param email
	 */
	public void setEmail (String email){
	 this.email=email;
	}
	/**
	 * 返回 密码
	 */
	private String password;
	/**
	 * 返回 密码
	 * @return
	 */
	public String getPassword (){
	 return password;
	}
	/**
	 * 设置 密码
	 * @param password
	 */
	public void setPassword (String password){
	 this.password=password;
	}
	/**
	 * 返回 地址
	 */
	private String addr;
	/**
	 * 返回 地址
	 * @return
	 */
	public String getAddr (){
	 return addr;
	}
	/**
	 * 设置 地址
	 * @param addr
	 */
	public void setAddr (String addr){
	 this.addr=addr;
	}
	/**
	 * 返回 电话
	 */
	private String phone;
	/**
	 * 返回 电话
	 * @return
	 */
	public String getPhone (){
	 return phone;
	}
	/**
	 * 设置 电话
	 * @param phone
	 */
	public void setPhone (String phone){
	 this.phone=phone;
	}
	
}