package cn.com.base.sys.web.vo;

import cn.com.base.sys.common.ReqModel;
import cn.com.base.sys.model.SysUsers;

public class UsersReq extends ReqModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SysUsers sysUsers=new SysUsers();
	private UsersRS usersRS=new UsersRS();
	public SysUsers getSysUsers() {
		return sysUsers;
	}
	public void setSysUsers(SysUsers sysUsers) {
		this.sysUsers = sysUsers;
	}
	public UsersRS getUsersRS() {
		return usersRS;
	}
	public void setUsersRS(UsersRS usersRS) {
		this.usersRS = usersRS;
	}
	
	

}
