package cn.com.busi.service;

import cn.com.base.sys.common.Page;
import cn.com.busi.model.InfoProductImage;
import cn.com.busi.web.vo.InfoProductImageRS;

public interface InfoProductImageService {
	/**
	 * 分页查询
	 */
	public Page<InfoProductImage> findInfoNewsByPage(int pageNo, int pageSize,
			InfoProductImageRS infoProductImageRS);
	

	public InfoProductImage findInfoNew(Long productId, Long imagesId);
	/**
	 * 修改数据
	 */
	public InfoProductImage infoNewsUpdate(InfoProductImage infoProductImage);

	/**
	 * 新增数据
	 */
	public InfoProductImage infoNewsSave(InfoProductImage infoProductImage);

	/**
	 * 删除数据
	 */
	public void infoNewsDelete(Long id);

	/**
	 * 通过主键查询数据
	 */
	public InfoProductImage infoNewsGet(Long id);
}
