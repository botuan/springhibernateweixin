package cn.com.base.sys.common;
 
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
 
/**
 * 分页对象
 * @author Administrator
 *
 * @param <T>
 */
public class Page<T> implements Serializable {
 
    private static final long serialVersionUID = 665620345605746930L;
    /** 总条数 */
    private Integer count;
    /** 页码 */
    private Integer pageNo;
    /** 每页显示多少条 */
    private Integer pageSize;
    /** 总页数 */
    private Integer totalPageCount;
    /** 起始条数 */
    private Integer firstRow;
    /** 结束条数 */
    private Integer lastRow;
    /** 查询结果集合形式的结果 */
    private List<T> result;
    /** 查询结果对象形式的结果 */
    public Object obj;
 
    public Integer code; // 返回码
    private boolean success = true;
    private String message;
 
    public Page() {
    }
 
    public Page(List<T> list) {
        this(list.size(), 1, list.size(), list);
    }
 
    public Page(Integer count, Integer pageNo, Integer rowsPerPage, List<T> result) {
        if (rowsPerPage < 1) {
            rowsPerPage = 1;
        }
        this.count = count;
        this.pageNo = pageNo;
        this.result = result;
        this.pageSize = rowsPerPage;
        if (this.result == null)
            this.result = new ArrayList<T>();
        totalPageCount = count / rowsPerPage;
        if (count - (count / rowsPerPage) * rowsPerPage > 0)
            totalPageCount++;
        if (count == 0) {
            totalPageCount = 0;
            pageNo = 0;
        }
 
        firstRow = (pageNo - 1) * rowsPerPage + 1;
        if (count == 0) {
            firstRow = 0;
        }
        lastRow = (pageNo) * rowsPerPage;
        if (lastRow > count) {
            lastRow = count;
        }
    }
 
    /** 返回每页的条数 */
    public Integer getCount() {
        return count;
    }
 
    public List<T> getResult() {
        return result;
    }
 
    public Integer getPageNo() {
        return pageNo;
    }
 
    /** 返回每页的条数 */
    public Integer getPageSize() {
        return pageSize;
    }
 
    /** 返回总的页数 */
    public Integer getTotalPageCount() {
        return totalPageCount;
    }
 
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }
 
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
 
    public Integer getFirstRow() {
        return firstRow;
    }
 
    public Integer getLastRow() {
        return lastRow;
    }
 
    public void setFirstRow(Integer firstRow) {
        this.firstRow = firstRow;
    }
 
    public void setLastRow(Integer lastRow) {
        this.lastRow = lastRow;
    }
 
    public void setCount(Integer count) {
        this.count = count;
    }
 
    public void setTotalPageCount(Integer totalPageCount) {
        this.totalPageCount = totalPageCount;
    }
 
    public void setResult(List<T> result) {
        this.result = result;
    }
 
    public Object getObj() {
        return obj;
    }
 
    public void setObj(Object obj) {
        this.obj = obj;
    }
 
    public boolean isSuccess() {
        return success;
    }
 
    public void setSuccess(boolean success) {
        this.success = success;
    }
 
    public String getMessage() {
        return message;
    }
 
    public void setMessage(String message) {
        this.message = message;
    }
 
    /**
     * 计算起始条数
     */
    public static Integer calc(Integer pageNo, Integer rowsPerPage, Integer count) {
        if (pageNo <= 0)
            pageNo = 1;
        if (rowsPerPage <= 0)
            rowsPerPage = 10;
 
        // 当把最后一页数据删除以后,页码会停留在最后一个上必须减一
        int totalPageCount = count / rowsPerPage;
        if (pageNo > totalPageCount && (count % rowsPerPage == 0)) {
            pageNo = totalPageCount;
        }
        if (pageNo - totalPageCount > 2) {
            pageNo = totalPageCount + 1;
        }
        int firstRow = (pageNo - 1) * rowsPerPage;
        if (firstRow < 0) {
            firstRow = 0;
        }
        return firstRow;
    }
 
}