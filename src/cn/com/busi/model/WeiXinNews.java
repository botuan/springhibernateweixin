package cn.com.busi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;


/**
 * 
 * @author Administrator
 *
 */
@Entity
@TableGenerator(name = "SYSSEQUENCE", table = "SYSSEQUENCE", pkColumnName = "GenName", valueColumnName = "GenValue", pkColumnValue = "WeiXinNews_PK", allocationSize = 1)
@Table(name = "WeiXinNews")
public class WeiXinNews {
	private Long id;
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "SYSSEQUENCE")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	private Long weiXinNewListId;
	private Long infoNewsId;
	// 标题
	private String title;
	// 图文消息的封面图片素材id（必须是永久mediaID）
	private String thumb_media_id;
	// 作者
	private String author;
	// 图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空
	private String digest;
	// 是否显示封面，0为false，即不显示，1为true，即显示
	private boolean show_cover_pic;
	// 图文消息的具体内容，支持HTML标签，必须少于2万字符，小于1M，且此处会去除JS
	private String content;
	// 图文消息的原文地址，即点击“阅读原文”后的URL
	private String content_source_url;

	@Column(name = "weiXinNewListId")
	public Long getWeiXinNewListId() {
		return weiXinNewListId;
	}
	public void setWeiXinNewListId(Long weiXinNewListId) {
		this.weiXinNewListId = weiXinNewListId;
	}

	@Column(name = "infoNewsId")
	public Long getInfoNewsId() {
		return infoNewsId;
	}
	public void setInfoNewsId(Long infoNewsId) {
		this.infoNewsId = infoNewsId;
	}
	@Column(name = "title")
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@Column(name = "thumb_media_id")
	public String getThumb_media_id() {
		return thumb_media_id;
	}
	public void setThumb_media_id(String thumb_media_id) {
		this.thumb_media_id = thumb_media_id;
	}
	@Column(name = "author")
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	@Column(name = "digest")
	public String getDigest() {
		return digest;
	}
	public void setDigest(String digest) {
		this.digest = digest;
	}

	@Column(name = "show_cover_pic")
	public boolean getShow_cover_pic() {
		return show_cover_pic;
	}
	public void setShow_cover_pic(boolean show_cover_pic) {
		this.show_cover_pic = show_cover_pic;
	}

	@Column(name = "content")
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "content_source_url")
	public String getContent_source_url() {
		return content_source_url;
	}
	public void setContent_source_url(String content_source_url) {
		this.content_source_url = content_source_url;
	}
	
	
}
