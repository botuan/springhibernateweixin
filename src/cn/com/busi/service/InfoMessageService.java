package cn.com.busi.service;

import org.springframework.stereotype.Service;

import cn.com.base.sys.common.Page;
import cn.com.busi.model.InfoMessage;
import cn.com.busi.web.vo.InfoMessageRS;
/**
 * 留言表的业务逻辑接口
 * @author 谢林
 */
public interface InfoMessageService {
	/**
	 * 分页查询
	 */
	public Page<InfoMessage> findInfoMessageByPage(int pageNo, int pageSize,InfoMessageRS infoMessageRS);
	/**
	 * 修改数据
	 */
	public InfoMessage infoMessageUpdate(InfoMessage infoMessage);
	/**
	 * 新增数据
	 */
	public InfoMessage infoMessageSave(InfoMessage infoMessage);
	/**
	 * 删除数据
	 */
	public void infoMessageDelete(InfoMessage infoMessage);
	/**
	 * 通过主键删除数据
	 */
		public void infoMessageDelete(Long  id);
		/**
	 * 通过主键查询数据
	 */
		public InfoMessage infoMessageGet(Long id);
	}
