package cn.com.busi.web.vo;

import cn.com.base.sys.common.ReqModel;
import cn.com.busi.model.WeiXinNewList;

public class WeiXinNewListReq extends ReqModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private WeiXinNewListRS weiXinNewListRS;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public WeiXinNewListRS getWeiXinNewListRS() {
		return weiXinNewListRS;
	}

	public void setWeiXinNewListRS(WeiXinNewListRS weiXinNewListRS) {
		this.weiXinNewListRS = weiXinNewListRS;
	}

	private WeiXinNewList weiXinNewList;

	public WeiXinNewList getWeiXinNewList() {
		return weiXinNewList;
	}

	public void setWeiXinNewList(WeiXinNewList weiXinNewList) {
		this.weiXinNewList = weiXinNewList;
	}

}
