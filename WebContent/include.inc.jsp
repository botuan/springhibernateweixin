<%@ page pageEncoding="UTF-8"%>
<%@page import="java.util.Enumeration"%>
<%@ taglib uri="/WEB-INF/tlds/sitemesh-decorator.tld" prefix="decorator"%>
<%@ taglib uri="/WEB-INF/tlds/sitemesh-page.tld" prefix="page"%> 
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<%@ taglib uri="/WEB-INF/tlds/CodeInputEL.tld" prefix="code"%>
<%@ taglib uri="/WEB-INF/tlds/PageInfoEL.tld" prefix="info"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<c:set var="user" value="${pageContext.getSession().getAttribute('user')}" />