package cn.com.busi.web.ctl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cn.com.base.sys.common.Page;
import cn.com.base.sys.security.SessionManager;
import cn.com.busi.model.WeiXinNewList;
import cn.com.busi.model.WeiXinNews;
import cn.com.busi.service.WeiXinNewListService;
import cn.com.busi.service.WeiXinNewsService;
import cn.com.busi.web.vo.WeiXinNewListReq;
import cn.com.busi.web.vo.WeiXinNewListRS;
import cn.com.busi.web.vo.WeiXinNewsRS;
import cn.com.weixin.sdk.api.ApiResult;
import cn.com.weixin.sdk.api.MediaApi;
import cn.com.weixin.sdk.api.MediaArticles;
import cn.com.weixin.sdk.api.MessageApi;

@Controller
@RequestMapping("/admin")
public class WeiXinNewListCtl  extends SessionManager{
	@Autowired
	private WeiXinNewListService weiXinNewListService;
	@Autowired
	private WeiXinNewsService weiXinNewsService;
	@RequestMapping("/weiXinNewListeQuery")
	public ModelAndView  weiXinNewListeQuery(@ModelAttribute WeiXinNewListReq weiXinNewListReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		WeiXinNewListRS weiXinNewListRS=weiXinNewListReq.getWeiXinNewListRS();
		modelMap.put("pageSize",weiXinNewListReq.getPageSize());
		modelMap.put("pageNo", weiXinNewListReq.getPageNo());
		modelMap.put("ctx", request.getContextPath());
		modelMap.put("weiXinNewListRS", weiXinNewListReq);
		Page<WeiXinNewList> page=weiXinNewListService.findWeiXinNewListByPage(weiXinNewListReq.getPageNo(), weiXinNewListReq.getPageSize(),weiXinNewListRS);//.getMenuByAuthoritieId(principal.getName());
		modelMap.put("page",page);
		return new ModelAndView("MediaArticles/list",modelMap);
	}
	
	/**
	 *WeiXinNewList打开新增界面
	 */
	@RequestMapping("/prepareWeiXinNewListAdd")
	public ModelAndView  prepareWeiXinNewListAdd(@ModelAttribute WeiXinNewListReq WeiXinNewListReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		return new ModelAndView("MediaArticles/edit",modelMap);
	}
	/**
	 *WeiXinNewList打开新增界面
	 */
	@RequestMapping("/weiXinNewListAdd")
	public ModelAndView  weiXinNewListAdd(@ModelAttribute WeiXinNewListReq weiXinNewListReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		WeiXinNewList weiXinNewList=weiXinNewListReq.getWeiXinNewList();
		weiXinNewListService.WeiXinNewListSave(weiXinNewList);
		modelMap.put("callbackUrl", "admin/weiXinNewListeQuery");
		modelMap.put("message", "添加成功");
		return new ModelAndView("success",modelMap);
	}
	
	@RequestMapping("/weiXinNewListDelete")
	public ModelAndView  weiXinNewListDelete(@ModelAttribute   WeiXinNewListReq weiXinNewListReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		WeiXinNewList weiXinNewList=weiXinNewListReq.getWeiXinNewList();
		weiXinNewList=	weiXinNewListService.WeiXinNewListGet(weiXinNewList.getId());
		weiXinNewListService.WeiXinNewListDelete(weiXinNewList.getId());
		
		if(weiXinNewList.getMedia_id()!=null&&!"".equals(weiXinNewList.getMedia_id().trim())){
			MediaApi.delMaterial(weiXinNewList.getMedia_id());
		}
		modelMap.put("message", "删除成功");
		modelMap.put("callbackUrl", "admin/weiXinNewListeQuery");
		return new ModelAndView("success",modelMap);
	}
	@RequestMapping("/weiXinNewListUploadWeiXin")
	public ModelAndView  weiXinNewListUploadWeiXin(@ModelAttribute   WeiXinNewListReq weiXinNewListReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		WeiXinNewList weiXinNewList=weiXinNewListReq.getWeiXinNewList();
		weiXinNewList=weiXinNewListService.WeiXinNewListGet(weiXinNewList.getId());
		WeiXinNewsRS weiXinNewsRS=new WeiXinNewsRS();
		weiXinNewsRS.setWeiXinNewListId(weiXinNewList.getId());
		Page<WeiXinNews> page=weiXinNewsService.findWeiXinNewsByPage(1, Integer.MAX_VALUE, weiXinNewsRS);
		List<MediaArticles>   mediaArticles=new ArrayList<MediaArticles>();
		for(WeiXinNews news:page.getResult()){
			MediaArticles art=new MediaArticles();
			art.setAuthor(news.getAuthor());
			art.setContent(news.getContent());
			art.setContent_source_url(news.getContent_source_url());
			art.setDigest(news.getDigest());
			art.setShow_cover_pic(news.getShow_cover_pic());
			art.setThumb_media_id(news.getThumb_media_id());
			art.setTitle(news.getTitle());
			mediaArticles.add(art);
		}
		ApiResult rs=MediaApi.addNews(mediaArticles);
		Integer error=rs.getErrorCode();
		if(error==null||error==0){
			String media_id=rs.getStr("media_id");
			weiXinNewList.setMedia_id(media_id);
			weiXinNewListService.WeiXinNewListUpdate(weiXinNewList);
			modelMap.put("message", "上传微信成功");
			modelMap.put("callbackUrl", "admin/weiXinNewListeQuery");
			return new ModelAndView("success",modelMap);
		}else{
			modelMap.put("message","错误代码："+error+",错误信息："+ rs.getErrorMsg());
			modelMap.put("callbackUrl", "admin/weiXinNewListeQuery");
			return new ModelAndView("error",modelMap);
		}
		
	}
	
	
	@RequestMapping("/weiXinNewListSendWeiXin")
	public ModelAndView  weiXinNewListSendWeiXin(@ModelAttribute   WeiXinNewListReq weiXinNewListReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		WeiXinNewList weiXinNewList = weiXinNewListReq.getWeiXinNewList();
		weiXinNewList = weiXinNewListService.WeiXinNewListGet(weiXinNewList.getId());
		if(weiXinNewList.getMedia_id()==null||"".equals(weiXinNewList.getMedia_id().trim())){
			modelMap.put("message", "还没有上传到微信，请先上传至微信");
			modelMap.put("callbackUrl", "admin/weiXinNewListeQuery");
			return new ModelAndView("error",modelMap);
		}
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append(" \"filter\":{");
		sb.append("  \"is_to_all\":true");
		sb.append("  },");
		sb.append("\"mpnews\":{");
		sb.append("\"media_id\":\""+weiXinNewList.getMedia_id()+"\"");
		sb.append(" },");
		sb.append("\"msgtype\":\"mpnews\"");
		sb.append("	}");
		ApiResult ar=MessageApi.sendAll(sb.toString());
		Integer errorcode=ar.getErrorCode();
		if(errorcode==null||errorcode!=0){
			modelMap.put("message", "群发失败，信息"+ar.getErrorMsg());
			modelMap.put("callbackUrl", "admin/weiXinNewListeQuery");
			return new ModelAndView("error",modelMap);
		}
		modelMap.put("message", "添加成功");
		modelMap.put("callbackUrl", "admin/weiXinNewListeQuery");
		return new ModelAndView("success",modelMap);
	}
	

	
}
