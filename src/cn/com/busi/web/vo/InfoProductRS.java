package cn.com.busi.web.vo;

import java.util.Date;

/**
 * ${modelFieldBean.desc}
 * @author 谢林
 */
public class InfoProductRS implements java.io.Serializable {
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		/**
	 * 返回 创建时间
	 */
	private Date createDate;
	/**
	 * 返回 创建时间
	 * @return
	 */
	public Date getCreateDate (){
	 return createDate;
	}
	/**
	 * 设置 创建时间
	 * @param createDate
	 */
	public void setCreateDate (Date createDate){
	 this.createDate=createDate;
	}
	/**
	 * 返回 修改时间
	 */
	private Date updateDate;
	/**
	 * 返回 修改时间
	 * @return
	 */
	public Date getUpdateDate (){
	 return updateDate;
	}
	/**
	 * 设置 修改时间
	 * @param updateDate
	 */
	public void setUpdateDate (Date updateDate){
	 this.updateDate=updateDate;
	}
	/**
	 * 返回 创建用户
	 */
	private String createUser;
	/**
	 * 返回 创建用户
	 * @return
	 */
	public String getCreateUser (){
	 return createUser;
	}
	/**
	 * 设置 创建用户
	 * @param createUser
	 */
	public void setCreateUser (String createUser){
	 this.createUser=createUser;
	}
	/**
	 * 返回 修改用户
	 */
	private String updateUser;
	/**
	 * 返回 修改用户
	 * @return
	 */
	public String getUpdateUser (){
	 return updateUser;
	}
	/**
	 * 设置 修改用户
	 * @param updateUser
	 */
	public void setUpdateUser (String updateUser){
	 this.updateUser=updateUser;
	}
	/**
	 * 返回 id
	 */
	private Long id;
	/**
	 * 返回 id
	 * @return
	 */
	public Long getId (){
	 return id;
	}
	/**
	 * 设置 id
	 * @param id
	 */
	public void setId (Long id){
	 this.id=id;
	}
	/**
	 * 返回 描述
	 */
	private String productDesc;
	/**
	 * 返回 描述
	 * @return
	 */
	public String getProductDesc (){
	 return productDesc;
	}
	/**
	 * 设置 描述
	 * @param productDesc
	 */
	public void setProductDesc (String productDesc){
	 this.productDesc=productDesc;
	}
	/**
	 * 返回 图片
	 */
	private String imageUrl;
	/**
	 * 返回 图片
	 * @return
	 */
	public String getImageUrl (){
	 return imageUrl;
	}
	/**
	 * 设置 图片
	 * @param imageUrl
	 */
	public void setImageUrl (String imageUrl){
	 this.imageUrl=imageUrl;
	}
	/**
	 * 返回 内容
	 */
	private String content;
	/**
	 * 返回 内容
	 * @return
	 */
	public String getContent (){
	 return content;
	}
	/**
	 * 设置 内容
	 * @param content
	 */
	public void setContent (String content){
	 this.content=content;
	}

	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	

	private Long type;
	public Long getType() {
		return type;
	}
	public void setType(Long type) {
		this.type = type;
	}
	
}