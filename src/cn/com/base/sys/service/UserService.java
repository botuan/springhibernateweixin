package cn.com.base.sys.service;

import java.util.Collection;
import java.util.List;


import cn.com.base.sys.common.Page;
import cn.com.base.sys.model.SysUsers;
import cn.com.base.sys.web.vo.UsersRS;

public interface UserService {
	/**
	 * 通过账号找到用户
	 * @param userAccount
	 * @return
	 */
	public List<SysUsers> findUser(String userAccount);
	/**
	 * 通话用户代码删除用户
	 * @param userCode
	 * @return
	 */
	public boolean deleteUser(String userCode);
	/**
	 * 通过账号找用户
	 * @param username
	 * @return
	 */
	public SysUsers findByUserAccount(String username);
	/**
	 * 通过id号查找用户
	 * @param userId
	 * @return
	 */
	public SysUsers findByUserId(String userId);
	/**
	 * 分页查询用户
	 * @param userId
	 * @param pageNo
	 * @param pageSize
	 * @param usersRS
	 * @return
	 */
	public Page<SysUsers> getUserByPage(String userId,int pageNo, int pageSize,UsersRS usersRS);
	public Page<SysUsers> findUserByPage(String userId,int pageNo, int pageSize,UsersRS usersRS);
	/**
	 * 更新用户
	 * @param sysUsers
	 * @return
	 */
	public SysUsers userUpdate(SysUsers sysUsers);
	/**
	 * 用户新增
	 * @param sysUsers
	 * @return
	 */
	public SysUsers userSave(SysUsers sysUsers);
	/**
	 * 删除
	 * @param sysUsers
	 */
	public void userDelete(SysUsers sysUsers);
	

}
