package cn.com.busi.service;

import org.springframework.stereotype.Service;

import cn.com.base.sys.common.Page;
import cn.com.busi.model.InfoCustomer;
import cn.com.busi.web.vo.InfoCustomerRS;
/**
 * 用户表的业务逻辑接口
 * @author 谢林
 */
public interface InfoCustomerService {
	/**
	 * 分页查询
	 */
	public Page<InfoCustomer> findInfoCustomerByPage(int pageNo, int pageSize,InfoCustomerRS infoCustomerRS);
	/**
	 * 修改数据
	 */
	public InfoCustomer infoCustomerUpdate(InfoCustomer infoCustomer);
	/**
	 * 新增数据
	 */
	public InfoCustomer infoCustomerSave(InfoCustomer infoCustomer);
	/**
	 * 删除数据
	 */
	public void infoCustomerDelete(InfoCustomer infoCustomer);
	/**
	 * 通过主键删除数据
	 */
		public void infoCustomerDelete(String  email);
		/**
	 * 通过主键查询数据
	 */
		public InfoCustomer infoCustomerGet(String email);
	}
