/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50620
Source Host           : localhost:3306
Source Database       : mvc

Target Server Type    : MYSQL
Target Server Version : 50620
File Encoding         : 65001

Date: 2016-07-21 19:43:25
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `infomessage`
-- ----------------------------
DROP TABLE IF EXISTS `infomessage`;
CREATE TABLE `infomessage` (
  `Id` bigint(20) NOT NULL,
  `LinkerEmail` varchar(70) NOT NULL,
  `LinkerName` varchar(300) NOT NULL,
  `LinkerPhone` varchar(300) NOT NULL,
  `Message` varchar(300) NOT NULL,
  `Title` varchar(300) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of infomessage
-- ----------------------------
