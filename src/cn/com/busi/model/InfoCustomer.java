package cn.com.busi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;

/**
 *  用户表的实体类
 * @return
 */ 
@Entity
@Table(name = "InfoCustomer")
public class InfoCustomer implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	private String userName;
	


	@Column(name = "userName",  length=50)
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	private String email;
	/**
	 * 主键 返回 邮箱
	 * @return
	 */
	@Id
	@Column(name = "Email", nullable = false, length=50)
	public String getEmail (){
	 return email;
	}
	/**
	 * 主键 设置 邮箱
	 * @param email
	 */
	public void setEmail (String email){
	 this.email=email;
	}
	private String password;
	/**
	 * 主键 返回 密码
	 * @return
	 */
	@Column(name = "Password", nullable = false, length=50)
	public String getPassword (){
	 return password;
	}
	/**
	 * 主键 设置 密码
	 * @param password
	 */
	public void setPassword (String password){
	 this.password=password;
	}
	private String addr;
	/**
	 * 主键 返回 地址
	 * @return
	 */
	@Column(name = "Addr", length=50)
	public String getAddr (){
	 return addr;
	}
	/**
	 * 主键 设置 地址
	 * @param addr
	 */
	public void setAddr (String addr){
	 this.addr=addr;
	}
	private String phone;
	/**
	 * 主键 返回 电话
	 * @return
	 */
	@Column(name = "Phone",  length=50)
	public String getPhone (){
	 return phone;
	}
	/**
	 * 主键 设置 电话
	 * @param phone
	 */
	public void setPhone (String phone){
	 this.phone=phone;
	}
}