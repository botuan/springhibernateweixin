package cn.com.busi.web.vo;


/**
 * 留言表
 * @author 谢林
 */
public class InfoMessageRS implements java.io.Serializable {
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		/**
	 * 返回 主键
	 */
	private Long id;
	/**
	 * 返回 主键
	 * @return
	 */
	public Long getId (){
	 return id;
	}
	/**
	 * 设置 主键
	 * @param id
	 */
	public void setId (Long id){
	 this.id=id;
	}
	/**
	 * 返回 标题
	 */
	private String title;
	/**
	 * 返回 标题
	 * @return
	 */
	public String getTitle (){
	 return title;
	}
	/**
	 * 设置 标题
	 * @param title
	 */
	public void setTitle (String title){
	 this.title=title;
	}
	/**
	 * 返回 标题
	 */
	private String linkerName;
	/**
	 * 返回 标题
	 * @return
	 */
	public String getLinkerName (){
	 return linkerName;
	}
	/**
	 * 设置 标题
	 * @param linkerName
	 */
	public void setLinkerName (String linkerName){
	 this.linkerName=linkerName;
	}
	/**
	 * 返回 标题
	 */
	private String linkerPhone;
	/**
	 * 返回 标题
	 * @return
	 */
	public String getLinkerPhone (){
	 return linkerPhone;
	}
	/**
	 * 设置 标题
	 * @param linkerPhone
	 */
	public void setLinkerPhone (String linkerPhone){
	 this.linkerPhone=linkerPhone;
	}
	/**
	 * 返回 标题
	 */
	private String message;
	/**
	 * 返回 标题
	 * @return
	 */
	public String getMessage (){
	 return message;
	}
	/**
	 * 设置 标题
	 * @param message
	 */
	public void setMessage (String message){
	 this.message=message;
	}
	/**
	 * 返回 标题
	 */
	private String linkerEmail;
	/**
	 * 返回 标题
	 * @return
	 */
	public String getLinkerEmail (){
	 return linkerEmail;
	}
	/**
	 * 设置 标题
	 * @param linkerEmail
	 */
	public void setLinkerEmail (String linkerEmail){
	 this.linkerEmail=linkerEmail;
	}
	
}