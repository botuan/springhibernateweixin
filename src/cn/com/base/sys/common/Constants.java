package cn.com.base.sys.common;

public class Constants {

	/**
	 * 权限资源类型
	 */
	public static final class Resoures {
		public static final String codeType = "ResouresType";

		/**
		 * 资源菜单类型
		 */
		public static final String MenuType = "menuType";

		/**
		 * Url资源类型
		 */
		public static final String UrlType = "url";

	}

	/**
	 * 组织类型
	 * 
	 * @author Administrator
	 *
	 */
	public static final class UnitType {
		public static final String codeType = "UnitType";
	}

	/**
	 * 项目抽取状态
	 * 
	 * @author Administrator
	 *
	 */
	public static final class ProjectStatu {
		public static final String codeType = "ProjectStatu";
		/**
		 * 初始化状态
		 */
		public static final String init = "0";
		/**
		 * 规避单位设置中
		 */
		public static final String edut = "1";
		/**
		 * 专家抽取
		 */
		public static final String draw = "2";
		/**
		 * 评标中
		 */
		public static final String projecting = "3";
		/**
		 * 评标结束
		 */
		public static final String projected = "4";
		/**
		 * 补抽专家中
		 */
		public static final String tdraw = "5";
		/**
		 * 已备案
		 */
		public static final String record = "6";

	}

	/**
	 * 职称
	 * 
	 * @author Administrator
	 *
	 */
	public static final class Duty {
		public static final String codeType = "Duty";
	}

	/**
	 * 性别
	 * 
	 * @author Administrator
	 *
	 */
	public static final class Sex {
		public static final String codeType = "Sex";
	}

	/**
	 * 是否
	 * 
	 * @author Administrator
	 *
	 */
	public static final class YesOrNo {
		public static final String codeType = "YesOrNo";
		public static final String yes = "1";
		public static final String no = "0";
	}

	/**
	 * 职务
	 * 
	 * @author Administrator
	 *
	 */
	public static final class Officer {
		public static final String codeType = "Officer";
	}

	/**
	 * 学位
	 * 
	 * @author Administrator
	 *
	 */
	public static final class DegreeId {
		public static final String codeType = "Degree";

	}

	/**
	 * 部门
	 * 
	 * @author Administrator
	 *
	 */
	public static final class DeptId {
		public static final String codeType = "Dept";

	}

	/**
	 * 执行力
	 * 
	 * @author Administrator
	 *
	 */
	public static final class ExeCompetenceId {
		public static final String codeType = "ExeCompetence";

	}

	/**
	 * 是否备用专家
	 * 
	 * @author Administrator
	 *
	 */
	public static final class IsBackUp {
		public static final String codeType = "ExeCompetence";

	}

	/**
	 * 职务所属行业
	 * 
	 * @author Administrator
	 *
	 */
	public static final class OfficerIndustry {
		public static final String codeType = "OfficerIndustry";

	}

	/**
	 * 学历
	 * 
	 * @author Administrator
	 *
	 */
	public static final class PositionId {
		public static final String codeType = "PositionId";

	}

	/**
	 * 配置状态
	 * 
	 * @author Administrator
	 *
	 */
	public static final class TrainState {
		public static final String codeType = "TrainState";

	}

	/**
	 * 地区
	 * 
	 * @author Administrator
	 *
	 */
	public static final class Area {
		public static final String codeType = "Area";

	}

	/**
	 * 专家
	 * 
	 * @author Administrator
	 *
	 */
	public static final class Expert {
		public static final String codeType = "Expert";

	}

	/**
	 * 身份证
	 * 
	 * @author Administrator
	 *
	 */
	public static final class CadTypeId {
		public static final String codeType = "CardType";

	}

	/**
	 * 是否退休
	 * 
	 * @author Administrator
	 *
	 */
	public static final class IsRetire {
		public static final String codeType = "YesOrNo";

	}

	/**
	 * 账号名称
	 * 
	 * @author Administrator
	 *
	 */
	public static final class UserId {
		public static final String codeType = "User";

	}

	/**
	 * 专业
	 * 
	 * @author Administrator
	 *
	 */
	public static final class IndustryId {
		public static final String codeType = "Industry";

	}

	public static final class Project {
		public static final String codeType = "Project";

	}

	/**
	 * 专业
	 * 
	 * @author Administrator
	 *
	 */
	public static final class Major0 {
		public static final String codeType = "Major0";

	}

	/**
	 * 行业
	 * 
	 * @author Administrator
	 *
	 */
	public static final class Major1 {
		public static final String codeType = "Major1";

	}

	public static final class ValidStatus {
		public static final String codeType = "ValidStatus";
		/**
		 * 有效
		 */
		public static final String valid = "1";
		/**
		 * 无效
		 */
		public static final String inValid = "0";

	}

	
	public static final class PageType{
		public static final String codeType = "PageType";
		/**
		 * 有效
		 */
		public static final String slides = "2";
		/**
		 * 无效
		 */
		public static final String common = "1";
	}
}
