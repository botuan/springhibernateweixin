package cn.com.busi.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;

/**
 *  新闻通知的实体类
 * @return
 */ 
@Entity
@TableGenerator(name = "SYSSEQUENCE", table = "SYSSEQUENCE", pkColumnName = "GenName", valueColumnName = "GenValue", pkColumnValue = "InfoNews_PK", allocationSize = 1)
@Table(name = "InfoNews")
public class InfoNews implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private Date createDate;
	/**
	 * 返回 创建时间
	 * @return
	 */
	@Column(name = "CreateDate", nullable = false, length=15)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getCreateDate (){
	 return createDate;
	}
	/**
	 * 设置 创建时间
	 * @param createDate
	 */
	public void setCreateDate (Date createDate){
	 this.createDate=createDate;
	}
	private String validStatus;
	/**
	 * 返回 记录状态
	 * @return
	 */
	@Column(name = "ValidStatus", nullable = false, length=15)
	public String getValidStatus (){
	 return validStatus;
	}
	/**
	 * 设置 记录状态
	 * @param validStatus
	 */
	public void setValidStatus (String validStatus){
	 this.validStatus=validStatus;
	}
	private String createUser;
	/**
	 * 返回 创建人
	 * @return
	 */
	@Column(name = "CreateUser", nullable = false, length=15)
	public String getCreateUser (){
	 return createUser;
	}
	/**
	 * 设置 创建人
	 * @param createUser
	 */
	public void setCreateUser (String createUser){
	 this.createUser=createUser;
	}
	private Date updateDate;
	/**
	 * 返回 修改时间
	 * @return
	 */
	@Column(name = "UpdateDate", nullable = false, length=15)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getUpdateDate (){
	 return updateDate;
	}
	/**
	 * 设置 修改时间
	 * @param updateDate
	 */
	public void setUpdateDate (Date updateDate){
	 this.updateDate=updateDate;
	}
	private String updateUser;
	/**
	 * 返回 修改人
	 * @return
	 */
	@Column(name = "UpdateUser", nullable = false, length=15)
	public String getUpdateUser (){
	 return updateUser;
	}
	/**
	 * 设置 修改人
	 * @param updateUser
	 */
	public void setUpdateUser (String updateUser){
	 this.updateUser=updateUser;
	}
	private String title;
	/**
	 * 返回 标题
	 * @return
	 */
	@Column(name = "Title", nullable = false, length=15)
	public String getTitle (){
	 return title;
	}
	/**
	 * 设置 标题
	 * @param title
	 */
	public void setTitle (String title){
	 this.title=title;
	}
	private String titleDesc;
	/**
	 * 返回 标题描述
	 * @return
	 */
	@Column(name = "TitleDesc", nullable = false, length=15)
	public String getTitleDesc (){
	 return titleDesc;
	}
	/**
	 * 设置 标题描述
	 * @param titleDesc
	 */
	public void setTitleDesc (String titleDesc){
	 this.titleDesc=titleDesc;
	}
	private String contents;
	/**
	 * 返回 内容
	 * @return
	 */
	@Column(name = "Contents", nullable = false, length=15)
	public String getContents (){
	 return contents;
	}
	/**
	 * 设置 内容
	 * @param contents
	 */
	public void setContents (String contents){
	 this.contents=contents;
	}
	private String status;
	/**
	 * 返回 状态
	 * @return
	 */
	@Column(name = "Status", nullable = false, length=15)
	public String getStatus (){
	 return status;
	}
	/**
	 * 设置 状态
	 * @param status
	 */
	public void setStatus (String status){
	 this.status=status;
	}
	private String type;
	/**
	 * 返回 类型
	 * @return
	 */
	@Column(name = "Type", nullable = false, length=15)
	public String getType (){
	 return type;
	}
	/**
	 * 设置 类型
	 * @param type
	 */
	public void setType (String type){
	 this.type=type;
	}
	private Long id;
	/**
	 * 主键 返回 主键
	 * @return
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "SYSSEQUENCE")
	@Column(name = "Id", nullable = false, length=15)
	public Long getId (){
	 return id;
	}
	/**
	 * 主键 设置 主键
	 * @param id
	 */
	public void setId (Long id){
	 this.id=id;
	}
	
	private String imageUrl;
	/**
	 * 封面图片
	 * @return
	 */
	@Column(name = "imageUrl")
	public String getImageUrl() {
		return imageUrl;
	}	
	/**
	 * 封面图片
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	private Long imageId;
	@Column(name = "imageId")
	public Long getImageId() {
		return imageId;
	}
	public void setImageId(Long imageId) {
		this.imageId = imageId;
	}
	private String isWeiXin;
	@Column(name = "isWeiXin")
	public String getIsWeiXin() {
		return isWeiXin;
	}
	public void setIsWeiXin(String isWeiXin) {
		this.isWeiXin = isWeiXin;
	}
}