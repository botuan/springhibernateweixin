package cn.com.base.sys.util;

import java.text.NumberFormat;

/**
 * 数字工具
 *
 */
public final class NumberUtil {
	private static NumberFormat numFormater =  NumberFormat.getInstance();
	
	private NumberUtil(){
		
	}
	
	/**
	 * 解释对象为int
	 * @param obj
	 * @param defalue
	 * @return
	 */
	public static int parseInt(String value, int defalue){
		try {
			return Integer.parseInt(value.toString());
		} catch (Exception e) {
			return defalue;
		}
	}
	
	public static boolean parseBoolan(String value, boolean def){
		if(Strings.isNullOrEmpty(value)){
			return def;
		}
		if("yes".equalsIgnoreCase(value) || "true".equalsIgnoreCase(value)){
			return true;
		}
		return false;
	}
	
	/**
	 * 格式化数字
	 * @param value
	 * @return
	 */
	public static String formateNum(double value){
		return numFormater.format(value);
	}
	
	/**
	 * 格式化字符串数字
	 * @param string
	 * @return
	 */
	public static String formateNum(String value) {
        try {
            NumberFormat nf = NumberFormat.getInstance();
            return nf.format(Long.parseLong(value));
		} catch (Exception e) {
			return value;
		}
    }
	
	/**
	 * 格式化时间值
	 * @param value
	 * @return
	 */
	public static String formateTime(long value){
		return value/1000/60/60 + "小时 " +  value/1000/60%60 + "分 " +  value/1000%60 + "秒 " + value%1000 + "毫秒";
	}
	
}
