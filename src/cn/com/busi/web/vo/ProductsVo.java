package cn.com.busi.web.vo;

import java.util.List;

import cn.com.busi.model.InfoProduct;
import cn.com.busi.model.InfoProductType;

public class ProductsVo {
	private InfoProductType infoProductType;
	private List<InfoProduct> infoProductList;
	public InfoProductType getInfoProductType() {
		return infoProductType;
	}
	public void setInfoProductType(InfoProductType infoProductType) {
		this.infoProductType = infoProductType;
	}
	public List<InfoProduct> getInfoProductList() {
		return infoProductList;
	}
	public void setInfoProductList(List<InfoProduct> infoProductList) {
		this.infoProductList = infoProductList;
	}
	
	

}
