package cn.com.busi.web.ctl;


import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.com.base.sys.common.Page;
import cn.com.base.sys.common.Result;
import cn.com.base.sys.security.SessionManager;
import cn.com.busi.model.InfoNews;
import cn.com.busi.service.InfoNewsService;
import cn.com.busi.web.vo.InfoNewsRS;
import cn.com.busi.web.vo.InfoNewsReq;
/**
 *InfoNews 的控制类
 */
@Controller
@RequestMapping("/admin")
public class InfoNewsCtl  extends SessionManager{

	@Autowired
	private InfoNewsService infoNewsService;
	/**
	 *InfoNews分页查询
	 */
	@RequestMapping("/infoNewsQuery")
	public ModelAndView  infoNewsQuery(@ModelAttribute InfoNewsReq infoNewsReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		InfoNewsRS infoNewsRS=infoNewsReq.getInfoNewsRS();
		modelMap.put("pageSize",infoNewsReq.getPageSize());
		modelMap.put("pageNo", infoNewsReq.getPageNo());
		modelMap.put("ctx", request.getContextPath());
		modelMap.put("infoNewsRS", infoNewsRS);
		Page<InfoNews> page=infoNewsService.findInfoNewsByPage(infoNewsReq.getPageNo(), infoNewsReq.getPageSize(),infoNewsRS);//.getMenuByAuthoritieId(principal.getName());
		modelMap.put("page",page);
		return new ModelAndView("news/list",modelMap);
	}
	/**
	 *InfoNews打开新增界面
	 */
	@RequestMapping("/prepareInfoNewsAdd")
	public ModelAndView  prepareInfoNewsAdd(@ModelAttribute InfoNewsReq infoNewsReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		return new ModelAndView("news/edit",modelMap);
	}
	
	/**
	 *InfoNews打开新增界面
	 */
	@RequestMapping("/prepareInfoNewsAddLast")
	public ModelAndView  prepareInfoNewsAddLast(@ModelAttribute InfoNewsReq infoNewsReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		Long id=infoNewsReq.getId();
		InfoNews infoNews=null;
		if(id==null||id==0){
			infoNews=infoNewsReq.getInfoNews();
			infoNewsService.infoNewsSave(infoNews);
		}else{
			 infoNews=infoNewsService.infoNewsGet(id);
			 InfoNews infoNews1=infoNewsReq.getInfoNews();
			 infoNews.setTitle(infoNews1.getTitle());
			 infoNews.setTitleDesc(infoNews1.getTitleDesc());
			 infoNews.setType(infoNews1.getType());
			infoNewsService.infoNewsUpdate(infoNews);
		}
		modelMap.put("infoNews", infoNews);
		return new ModelAndView("news/contentsEdit",modelMap);
	}
	
	@RequestMapping("/prepareInfoNewsUpLoad")
	public ModelAndView  prepareInfoProductUpLoad(@ModelAttribute InfoNewsReq infoNewsReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		
		Long id=infoNewsReq.getId();
		InfoNews infoNews=infoNewsService.infoNewsGet(id);
						
		
		modelMap.put("infoNews", infoNews);
		return new ModelAndView("admin/news/news_upload",modelMap);
	}
	@RequestMapping("/infoNewsEditLast")
	public ModelAndView   infoNewsEditLast(@ModelAttribute InfoNewsReq infoNewsReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		InfoNews infoNews1=infoNewsReq.getInfoNews();
		Long id=infoNewsReq.getId();
		InfoNews infoNews=infoNewsService.infoNewsGet(id);
		Date date=new Date();
		infoNews.setUpdateDate(date);
		infoNews.setUpdateUser("aa");
		infoNews.setContents(infoNews1.getContents());
		infoNewsService.infoNewsUpdate(infoNews);
		modelMap.put("code", "1");
		modelMap.put("message", "操作成功");
		modelMap.put("callbackUrl", "admin/infoNewsQuery");
		return new ModelAndView("success",modelMap);
	}
	
	/**
	 *InfoNews新增保存
	 */
	@RequestMapping("/infoNewsAdd")
	public ModelAndView   infoNewsAdd(@ModelAttribute InfoNewsReq infoNewsReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		InfoNews infoNews=infoNewsReq.getInfoNews();
		Date date=new Date();
		infoNews.setCreateDate(date);
		infoNews.setUpdateDate(date);
		infoNewsService.infoNewsSave(infoNews);
		modelMap.put("code", "1");
		modelMap.put("message", "操作成功");
		modelMap.put("callbackUrl", "admin/infoNewsQuery");
		return new ModelAndView("success",modelMap);
	}
	/**
	 *InfoNews打开修改界面
	 */
	@RequestMapping("/prepareInfoNewsEdit")
	public ModelAndView  prepareInfoNewsEdit(@ModelAttribute InfoNewsReq infoNewsReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		Long id=infoNewsReq.getId();
		InfoNews infoNews=infoNewsService.infoNewsGet(id);
		modelMap.put("infoNews", infoNews);
		return new ModelAndView("news/edit",modelMap);
	}
	
	@RequestMapping("/prepareInfoNewsEditLast")
	public ModelAndView  prepareInfoNewsEditLast(@ModelAttribute InfoNewsReq infoNewsReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		Long id=infoNewsReq.getId();
		InfoNews infoNews=infoNewsService.infoNewsGet(id);
		modelMap.put("infoNews", infoNews);
		return new ModelAndView("news/contentsEdit",modelMap);
	}
	/**
	 *InfoNews修改保存
	 */
	@RequestMapping("/infoNewsEdit")
	public @ResponseBody Result   infoNewsEdit(@ModelAttribute InfoNewsReq infoNewsReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
		InfoNews infoNews=infoNewsReq.getInfoNews();
		infoNewsService.infoNewsUpdate(infoNews);
		Result r=new Result();
		r.setCode("0");
		r.setMessage("上传成功");
		return r;
	}
	/**
	 *InfoNews删除操作
	 */
	@RequestMapping("/infoNewsDelete")
	public ModelAndView   infoNewsDelete(@ModelAttribute InfoNewsReq infoNewsReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		//super.adminLoginValidate(request);
					
		Long id=infoNewsReq.getId();
		infoNewsService.infoNewsDelete(id);
		modelMap.put("code", "1");
		modelMap.put("message", "操作成功");
		modelMap.put("callbackUrl", "admin/infoNewsQuery");
		return new ModelAndView("success",modelMap);
	}
	
	
}
