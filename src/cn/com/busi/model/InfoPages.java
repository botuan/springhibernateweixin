package cn.com.busi.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;

/**
 *  页面的实体类
 * @return
 */ 
@Entity
@TableGenerator(name = "SYSSEQUENCE", table = "SYSSEQUENCE", pkColumnName = "GenName", valueColumnName = "GenValue", pkColumnValue = "InfoPages_PK", allocationSize = 1)
@Table(name = "InfoPages")
public class InfoPages implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private String pageKeyWords;
	private String pageDescription;
	
	@Column(name = "pageKeyWords",  length=2000)
	public String getPageKeyWords() {
		return pageKeyWords;
	}
	public void setPageKeyWords(String pageKeyWords) {
		this.pageKeyWords = pageKeyWords;
	}
	@Column(name = "pageDescription",  length=2000)
	public String getPageDescription() {
		return pageDescription;
	}
	public void setPageDescription(String pageDescription) {
		this.pageDescription = pageDescription;
	}
	private String name;
	

	@Column(name = "name", nullable = false, length=15)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	private String type;
	/**
	 * 返回 类型
	 * @return
	 */
	@Column(name = "Type", nullable = false, length=15)
	public String getType (){
	 return type;
	}
	/**
	 * 设置 类型
	 * @param type
	 */
	public void setType (String type){
	 this.type=type;
	}
	private String content;
	/**
	 * 返回 内容页
	 * @return
	 */
	@Column(name = "Content", nullable = false, length=15)
	public String getContent (){
	 return content;
	}
	/**
	 * 设置 内容页
	 * @param content
	 */
	public void setContent (String content){
	 this.content=content;
	}
	private Date createDate;
	/**
	 * 返回 创建时间
	 * @return
	 */
	@Column(name = "CreateDate", nullable = false, length=15)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getCreateDate (){
	 return createDate;
	}
	/**
	 * 设置 创建时间
	 * @param createDate
	 */
	public void setCreateDate (Date createDate){
	 this.createDate=createDate;
	}
	private String createUser;
	/**
	 * 返回 创建人
	 * @return
	 */
	@Column(name = "CreateUser", nullable = false, length=15)
	public String getCreateUser (){
	 return createUser;
	}
	/**
	 * 设置 创建人
	 * @param createUser
	 */
	public void setCreateUser (String createUser){
	 this.createUser=createUser;
	}
	private Long id;
	/**
	 * 主键 返回 主键
	 * @return
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "SYSSEQUENCE")
	@Column(name = "Id", nullable = false, length=15)
	public Long getId (){
	 return id;
	}
	/**
	 * 主键 设置 主键
	 * @param id
	 */
	public void setId (Long id){
	 this.id=id;
	}
}