package cn.com.busi.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.base.sys.common.BaseJpaDao;
import cn.com.base.sys.common.Page;
import cn.com.base.sys.common.Query;
import cn.com.base.sys.util.Strings;
import cn.com.busi.model.InfoProduct;
import cn.com.busi.model.InfoProductType;
import cn.com.busi.service.InfoProductTypeService;
import cn.com.busi.web.vo.InfoProductTypeRS;

@Service(value = "infoProductTypeService")
public class InfoProductTypeServiceImpl implements InfoProductTypeService {

	@Autowired
	private BaseJpaDao baseJpaDao;
	static Logger log = Logger.getLogger(InfoProductTypeServiceImpl.class);

	@Override
	public Page<InfoProductType> findInfoProductTypeByPage(int pageNo,
			int pageSize, InfoProductTypeRS infoProductTypeRS) {
		log.info("正在执行  getInfoProductByPage()");
		Query query = baseJpaDao.createQuery(InfoProductType.class);
		this.initQuery(query, infoProductTypeRS);
		Page page = baseJpaDao.queryPage(query, pageNo, pageSize);
		log.info("执行 getInfoProductByPage()完毕，正在返回");
		return page;
	}

	private void initQuery(Query query, InfoProductTypeRS infoProductTypeRS) {
		log.info("正在执行  initQuery()");
		if (infoProductTypeRS != null) {
			if (!Strings.isNullOrEmpty(infoProductTypeRS.getCreateDate())) {
				query.eq("createDate", infoProductTypeRS.getCreateDate());
			}
			if (!Strings.isNullOrEmpty(infoProductTypeRS.getUpdateDate())) {
				query.eq("updateDate", infoProductTypeRS.getUpdateDate());
			}
			if (!Strings.isNullOrEmpty(infoProductTypeRS.getCreateUser())) {
				query.eq("createUser", infoProductTypeRS.getCreateUser());
			}
			if (!Strings.isNullOrEmpty(infoProductTypeRS.getUpdateUser())) {
				query.eq("updateUser", infoProductTypeRS.getUpdateUser());
			}
			if (!Strings.isNullOrEmpty(infoProductTypeRS.getId())) {
				query.eq("id", infoProductTypeRS.getId());
			}
			if (!Strings.isNullOrEmpty(infoProductTypeRS.getTypeDesc())) {
				query.like("typeDesc", infoProductTypeRS.getTypeDesc());
			}
			if (!Strings.isNullOrEmpty(infoProductTypeRS.getTypeImageUrl())) {
				query.eq("typeImageUrl", infoProductTypeRS.getTypeImageUrl());
			}
			if (!Strings.isNullOrEmpty(infoProductTypeRS.getName())) {
				query.like("name", infoProductTypeRS.getName());
			}

		}

		log.info("执行 initQuery完毕，正在返回");
	}

	@Override
	public InfoProductType infoProductTypeUpdate(InfoProductType infoProductType) {
		baseJpaDao.update(infoProductType);
		return infoProductType;
	}

	@Override
	public InfoProductType infoProductTypeSave(InfoProductType infoProductType) {
		baseJpaDao.save(infoProductType);
		return infoProductType;
	}

	@Override
	public void infoProductTypeDelete(Long id) {
		List<Long> list=new ArrayList<Long>();
		list.add(id);
		baseJpaDao.delete(InfoProductType.class, list);

	}

	@Override
	public InfoProductType infoProductTypeGet(Long id) {
		InfoProductType infoProductType=baseJpaDao.get(InfoProductType.class, id);
		return infoProductType;
	}

}
