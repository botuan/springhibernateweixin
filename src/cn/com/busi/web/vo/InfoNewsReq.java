package cn.com.busi.web.vo;

import cn.com.base.sys.common.ReqModel;
import cn.com.busi.model.InfoNews;
/**
 * ${modelFieldBean.desc}
 * @author 谢林
 */
public class InfoNewsReq extends ReqModel {
	private static final long serialVersionUID = 1L;
	private InfoNews infoNews=new InfoNews();
	private InfoNewsRS infoNewsRS=new InfoNewsRS();
	public InfoNews getInfoNews() {
		return infoNews;
	}
	public void setInfoNews(InfoNews infoNews) {
		this.infoNews = infoNews;
	}
	public InfoNewsRS getInfoNewsRS() {
		return infoNewsRS;
	}
	public void setInfoNewsRS(InfoNewsRS infoNewsRS) {
		this.infoNewsRS = infoNewsRS;
	}
		
		
	/**
	 *主键
	 */
	private Long id=new Long(0);
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	    
}
