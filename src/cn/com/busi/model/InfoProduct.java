package cn.com.busi.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;

/**
 *  产品的实体类
 * @return
 */ 
@Entity
@TableGenerator(name = "SYSSEQUENCE", table = "SYSSEQUENCE", pkColumnName = "GenName", valueColumnName = "GenValue", pkColumnValue = "InfoProduct_PK", allocationSize = 1)
@Table(name = "InfoProduct")
public class InfoProduct implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private Date createDate;
	/**
	 * 返回 创建时间
	 * @return
	 */
	@Column(name = "CreateDate")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getCreateDate (){
	 return createDate;
	}
	/**
	 * 设置 创建时间
	 * @param createDate
	 */
	public void setCreateDate (Date createDate){
	 this.createDate=createDate;
	}
	private Date updateDate;
	/**
	 * 返回 修改时间
	 * @return
	 */
	@Column(name = "UpdateDate")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getUpdateDate (){
	 return updateDate;
	}
	/**
	 * 设置 修改时间
	 * @param updateDate
	 */
	public void setUpdateDate (Date updateDate){
	 this.updateDate=updateDate;
	}
	private String createUser;
	/**
	 * 返回 创建用户
	 * @return
	 */
	@Column(name = "CreateUser")
	public String getCreateUser (){
	 return createUser;
	}
	/**
	 * 设置 创建用户
	 * @param createUser
	 */
	public void setCreateUser (String createUser){
	 this.createUser=createUser;
	}
	private String updateUser;
	/**
	 * 返回 修改用户
	 * @return
	 */
	@Column(name = "UpdateUser")
	public String getUpdateUser (){
	 return updateUser;
	}
	/**
	 * 设置 修改用户
	 * @param updateUser
	 */
	public void setUpdateUser (String updateUser){
	 this.updateUser=updateUser;
	}
	
	
	private String name;
	@Column(name = "name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	private Long id;
	/**
	 * 返回 id
	 * @return
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "SYSSEQUENCE")
	@Column(name = "Id")
	public Long getId (){
	 return id;
	}
	/**
	 * 设置 id
	 * @param id
	 */
	public void setId (Long id){
	 this.id=id;
	}
	private String productDesc;
	/**
	 * 返回 描述
	 * @return
	 */
	@Column(name = "ProductDesc")
	public String getProductDesc (){
	 return productDesc;
	}
	/**
	 * 设置 描述
	 * @param productDesc
	 */
	public void setProductDesc (String productDesc){
	 this.productDesc=productDesc;
	}
	private String imageUrl;
	/**
	 * 返回 图片
	 * @return
	 */
	@Column(name = "ImageUrl")
	public String getImageUrl (){
	 return imageUrl;
	}
	/**
	 * 设置 图片
	 * @param imageUrl
	 */
	public void setImageUrl (String imageUrl){
	 this.imageUrl=imageUrl;
	}
	private String content;
	/**
	 * 返回 内容
	 * @return
	 */
	@Column(name = "Content")
	public String getContent (){
	 return content;
	}
	/**
	 * 设置 内容
	 * @param content
	 */
	public void setContent (String content){
	 this.content=content;
	}
	
	private Long type;
	@Column(name = "type")
	public Long getType() {
		return type;
	}
	public void setType(Long type) {
		this.type = type;
	}
	
	private String produtUrl;
	private String fileUrl;

	@Column(name = "produtUrl")
	public String getProdutUrl() {
		return produtUrl;
	}
	public void setProdutUrl(String produtUrl) {
		this.produtUrl = produtUrl;
	}
	@Column(name = "fileUrl")
	public String getFileUrl() {
		return fileUrl;
	}
	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}
	
}