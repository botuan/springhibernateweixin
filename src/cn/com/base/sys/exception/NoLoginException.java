package cn.com.base.sys.exception;

public class NoLoginException  extends BaseException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoLoginException(String message) {
		super(message);
	}

}
