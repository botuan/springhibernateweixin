package cn.com.base.sys.web.vo;

import cn.com.base.sys.common.ReqModel;
import cn.com.base.sys.model.BusinessCode;
/**
 * ${modelFieldBean.desc}
 * @author 胡呈
 */
public class BusinessCodeReq extends ReqModel {
	private static final long serialVersionUID = 1L;
	private BusinessCode businessCode=new BusinessCode();
	private BusinessCodeRS businessCodeRS=new BusinessCodeRS();
	public BusinessCode getBusinessCode() {
		return businessCode;
	}
	public void setBusinessCode(BusinessCode businessCode) {
		this.businessCode = businessCode;
	}
	public BusinessCodeRS getBusinessCodeRS() {
		return businessCodeRS;
	}
	public void setBusinessCodeRS(BusinessCodeRS businessCodeRS) {
		this.businessCodeRS = businessCodeRS;
	}
		
	/**
	 *复合主键
	 */
	private Long businessCodeId=new Long(0);
	public Long getBusinessCodeId() {
		return businessCodeId;
	}
	public void setBusinessCodeId(Long businessCodeId) {
		this.businessCodeId = businessCodeId;
	}
	
}
