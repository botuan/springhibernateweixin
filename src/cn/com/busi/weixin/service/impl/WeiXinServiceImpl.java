package cn.com.busi.weixin.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;







import cn.com.weixin.sdk.msg.in.InImageMsg;
import cn.com.weixin.sdk.msg.in.InLinkMsg;
import cn.com.weixin.sdk.msg.in.InLocationMsg;
import cn.com.weixin.sdk.msg.in.InShortVideoMsg;
import cn.com.weixin.sdk.msg.in.InTextMsg;
import cn.com.weixin.sdk.msg.in.InVideoMsg;
import cn.com.weixin.sdk.msg.in.InVoiceMsg;
import cn.com.weixin.sdk.msg.in.event.InCustomEvent;
import cn.com.weixin.sdk.msg.in.event.InFollowEvent;
import cn.com.weixin.sdk.msg.in.event.InLocationEvent;
import cn.com.weixin.sdk.msg.in.event.InMassEvent;
import cn.com.weixin.sdk.msg.in.event.InMenuEvent;
import cn.com.weixin.sdk.msg.in.event.InPoiCheckNotifyEvent;
import cn.com.weixin.sdk.msg.in.event.InQrCodeEvent;
import cn.com.weixin.sdk.msg.in.event.InShakearoundUserShakeEvent;
import cn.com.weixin.sdk.msg.in.event.InTemplateMsgEvent;
import cn.com.weixin.sdk.msg.in.event.InVerifyFailEvent;
import cn.com.weixin.sdk.msg.in.event.InVerifySuccessEvent;
import cn.com.weixin.sdk.msg.in.event.InWifiEvent;
import cn.com.weixin.sdk.msg.in.speech_recognition.InSpeechRecognitionResults;
import cn.com.weixin.sdk.msg.out.News;
import cn.com.weixin.sdk.msg.out.OutCustomMsg;
import cn.com.weixin.sdk.msg.out.OutMsg;
import cn.com.weixin.sdk.msg.out.OutNewsMsg;
import cn.com.weixin.sdk.serviceImpl.WXAbstractServiceImpl;

@Service(value="weiXinService")
public class WeiXinServiceImpl  extends WXAbstractServiceImpl{
static Logger logger=Logger.getLogger(WeiXinServiceImpl.class);
	@Override
	public OutMsg processInTextMsg(InTextMsg inTextMsg) {
		logger.info("processInTextMsg");
		//转发给多客服PC客户端
		OutNewsMsg outNewsMsg = new OutNewsMsg(inTextMsg);
		List<News> articles=new ArrayList<News>();
		for(int i=1;i<=5;i++){
			News news=new News();
			news.setDescription("广东省碳排放配额托管业务指引");
			news.setPicUrl("http://www.gdzhxny.com/upload/image/IMG20160123183927.jpg");
			news.setTitle("广东省碳排放配额托管业务指引");
			news.setUrl("http://www.gdzhxny.com/web/news/29.html");
			articles.add(news);
		}
		logger.info(articles.size());
		outNewsMsg.setArticles(articles);
		return outNewsMsg;
	}

	@Override
	public OutMsg processInImageMsg(InImageMsg inImageMsg) {
		//转发给多客服PC客户端
		OutCustomMsg outCustomMsg = new OutCustomMsg(inImageMsg);
		return outCustomMsg;
	}

	@Override
	public OutMsg processInVoiceMsg(InVoiceMsg inVoiceMsg) {
		//转发给多客服PC客户端
		OutCustomMsg outCustomMsg = new OutCustomMsg(inVoiceMsg);
		return outCustomMsg;
	}

	@Override
	public OutMsg processInVideoMsg(InVideoMsg inVideoMsg) {
		//转发给多客服PC客户端
		OutCustomMsg outCustomMsg = new OutCustomMsg(inVideoMsg);
		return outCustomMsg;
	}

	@Override
	public OutMsg processInShortVideoMsg(InShortVideoMsg inShortVideoMsg) {
		//转发给多客服PC客户端
		OutCustomMsg outCustomMsg = new OutCustomMsg(inShortVideoMsg);
		return outCustomMsg;
	}

	@Override
	public OutMsg processInLocationMsg(InLocationMsg inLocationMsg) {
		//转发给多客服PC客户端
		OutCustomMsg outCustomMsg = new OutCustomMsg(inLocationMsg);
		return outCustomMsg;
	}

	@Override
	public OutMsg processInLinkMsg(InLinkMsg inLinkMsg) {
		//转发给多客服PC客户端
		OutCustomMsg outCustomMsg = new OutCustomMsg(inLinkMsg);
		return outCustomMsg;
	}

	@Override
	public OutMsg processInCustomEvent(InCustomEvent inCustomEvent) {
		//转发给多客服PC客户端
		OutCustomMsg outCustomMsg = new OutCustomMsg(inCustomEvent);
		return outCustomMsg;
	}

	@Override
	public OutMsg processInFollowEvent(InFollowEvent inFollowEvent) {
		//转发给多客服PC客户端
		OutCustomMsg outCustomMsg = new OutCustomMsg(inFollowEvent);
		return outCustomMsg;
	}

	@Override
	public OutMsg processInQrCodeEvent(InQrCodeEvent inQrCodeEvent) {
		//转发给多客服PC客户端
		OutCustomMsg outCustomMsg = new OutCustomMsg(inQrCodeEvent);
		return outCustomMsg;
	}

	@Override
	public OutMsg processInLocationEvent(InLocationEvent inLocationEvent) {
		//转发给多客服PC客户端
		OutCustomMsg outCustomMsg = new OutCustomMsg(inLocationEvent);
		return outCustomMsg;
	}

	@Override
	public OutMsg processInMassEvent(InMassEvent inMassEvent) {
		//转发给多客服PC客户端
		OutCustomMsg outCustomMsg = new OutCustomMsg(inMassEvent);
		return outCustomMsg;
	}

	@Override
	public OutMsg processInMenuEvent(InMenuEvent inMenuEvent) {
		//转发给多客服PC客户端
		OutCustomMsg outCustomMsg = new OutCustomMsg(inMenuEvent);
		return outCustomMsg;
	}

	@Override
	public OutMsg processInSpeechRecognitionResults(
			InSpeechRecognitionResults inSpeechRecognitionResults) {
		//转发给多客服PC客户端
		OutCustomMsg outCustomMsg = new OutCustomMsg(inSpeechRecognitionResults);
		return outCustomMsg;
	}

	@Override
	public OutMsg processInTemplateMsgEvent(
			InTemplateMsgEvent inTemplateMsgEvent) {
		//转发给多客服PC客户端
		OutCustomMsg outCustomMsg = new OutCustomMsg(inTemplateMsgEvent);
		return outCustomMsg;
	}

	@Override
	public OutMsg processInShakearoundUserShakeEvent(
			InShakearoundUserShakeEvent inShakearoundUserShakeEvent) {
		//转发给多客服PC客户端
		OutCustomMsg outCustomMsg = new OutCustomMsg(inShakearoundUserShakeEvent);
		return outCustomMsg;
	}

	@Override
	public OutMsg processInVerifySuccessEvent(
			InVerifySuccessEvent inVerifySuccessEvent) {
		//转发给多客服PC客户端
		OutCustomMsg outCustomMsg = new OutCustomMsg(inVerifySuccessEvent);
		return outCustomMsg;
	}

	@Override
	public OutMsg processInVerifyFailEvent(InVerifyFailEvent inVerifyFailEvent) {
		//转发给多客服PC客户端
		OutCustomMsg outCustomMsg = new OutCustomMsg(inVerifyFailEvent);
		return outCustomMsg;
	}

	@Override
	public OutMsg processInPoiCheckNotifyEvent(
			InPoiCheckNotifyEvent inPoiCheckNotifyEvent) {
		//转发给多客服PC客户端
		OutCustomMsg outCustomMsg = new OutCustomMsg(inPoiCheckNotifyEvent);
		return outCustomMsg;
	}

	@Override
	public OutMsg processInWifiEvent(InWifiEvent inWifiEvent) {
		//转发给多客服PC客户端
		OutCustomMsg outCustomMsg = new OutCustomMsg(inWifiEvent);
		return outCustomMsg;
	}

}
