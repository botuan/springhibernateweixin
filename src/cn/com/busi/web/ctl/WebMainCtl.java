package cn.com.busi.web.ctl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cn.com.base.sys.common.Constants;
import cn.com.base.sys.common.Page;
import cn.com.base.sys.common.Result;
import cn.com.base.sys.security.SessionManager;
import cn.com.base.sys.util.Strings;
import cn.com.busi.model.InfoCustomer;
import cn.com.busi.model.InfoMessage;
import cn.com.busi.model.InfoPages;
import cn.com.busi.model.InfoProduct;
import cn.com.busi.model.InfoProductType;
import cn.com.busi.service.InfoCustomerService;
import cn.com.busi.service.InfoMessageService;
import cn.com.busi.service.InfoPagesService;
import cn.com.busi.service.InfoProductService;
import cn.com.busi.service.InfoProductTypeService;
import cn.com.busi.web.vo.InfoCustomerRS;
import cn.com.busi.web.vo.InfoCustomerReq;
import cn.com.busi.web.vo.InfoMessageReq;
import cn.com.busi.web.vo.InfoPagesRS;
import cn.com.busi.web.vo.InfoProductRS;
import cn.com.busi.web.vo.ProductsVo;

@Controller
public class WebMainCtl extends SessionManager {

	@Autowired
	/**
	 *InfoCustomer逻辑操作接口
	 */
	private InfoCustomerService infoCustomerService;
	@Autowired
	private InfoProductTypeService infoProductTypeService;
	@Autowired
	private InfoMessageService infoMessageService;

	@Autowired
	private InfoPagesService infoPagesService;

	@Autowired
	private InfoProductService infoProductService;
	@RequestMapping("/index")
	public ModelAndView index(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) {

		//loadProjectTypes(modelMap);
		//loadPages(modelMap);
		return new ModelAndView("index", modelMap);

	}
	
	@RequestMapping("/{name}")
	public ModelAndView index(@PathVariable("name") String name,  HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) {
		StringBuilder sb=new StringBuilder();
		sb.append("http://");
		sb.append(request.getServerName());
		Integer port=request.getServerPort();
		if(port!=80){
			sb.append(":"+request.getServerPort());
		}
		if(!"".equals(request.getContextPath())){
			sb.append(request.getContextPath());
		};
		
		sb.append("/upload.do");

		System.out.println(sb.toString());
		return new ModelAndView("example/"+name, modelMap);
	}

	private void loadPages(ModelMap modelMap) {
		InfoPagesRS infoPagesRS = new InfoPagesRS();
		infoPagesRS.setType(Constants.PageType.common);
		Page page = infoPagesService.findInfoPagesByPage(0, Integer.MAX_VALUE,
				infoPagesRS);
		if (page != null && page.getResult() != null) {
			modelMap.put("pages", page.getResult());
		}
	}

	private void loadProjectTypes(ModelMap modelMap) {
		Page<InfoProductType> page = infoProductTypeService
				.findInfoProductTypeByPage(0, 6, null);
		if (page != null) {
			List<InfoProductType> list = page.getResult();
			if (list != null && list.size() > 0) {
				List<List<InfoProductType>> lis1 = new ArrayList<List<InfoProductType>>();

				List<InfoProductType> l = new ArrayList<InfoProductType>();
				int i = 0;
				int size = list.size();
				int count = 0;
				for (InfoProductType info : list) {
					count++;
					if (i <= 2) {
						i++;
						l.add(info);
						if (count == size) {

							lis1.add(l);
						}

					} else {
						lis1.add(l);
						l = new ArrayList<InfoProductType>();
						l.add(info);
						if (count == size) {

							lis1.add(l);
						}
						i = 0;
					}
				}
				modelMap.put("productTypes", lis1);

			}
		}
	}

	@RequestMapping("/login")
	public ModelAndView login(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) {
		String email = request.getParameter("email");
		String pawd = request.getParameter("password");
		if (!Strings.isNullOrEmpty(email) && !Strings.isNullOrEmpty(pawd)) {
			InfoCustomer infoCustomer = infoCustomerService
					.infoCustomerGet(email);
			if (infoCustomer != null) {
				if (pawd.equals(infoCustomer.getPassword())) {
					HttpSession session = request.getSession();
					session.setAttribute(SessionManager.loginStatus,
							SessionManager.statusYes);
					session.setAttribute(SessionManager.loginType,
							SessionManager.typeAdmin);
					session.setAttribute(SessionManager.user, infoCustomer);
				}
				return new ModelAndView("web/main/index", modelMap);
			}
		}

		return new ModelAndView("login", modelMap);
	}

	@RequestMapping("/loginOut")
	public ModelAndView loginOut(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) {
		request.getSession().invalidate();
		return new ModelAndView("web/main/index", modelMap);
	}

	@RequestMapping("/about")
	public ModelAndView about(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) {
		loadProjectTypes(modelMap);
		loadPages(modelMap);
		return new ModelAndView("web/main/about", modelMap);
	}

	@RequestMapping("/contact")
	public ModelAndView contact(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) {
		loadProjectTypes(modelMap);
		loadPages(modelMap);
		return new ModelAndView("web/main/contact", modelMap);
	}

	@RequestMapping("/projects")
	public ModelAndView projects(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) {
		loadProjectTypes(modelMap);
		loadPages(modelMap);
		List<ProductsVo> list=new ArrayList<ProductsVo>();
		Page<InfoProductType> page1=infoProductTypeService.findInfoProductTypeByPage(0, Integer.MAX_VALUE, null);
		if(page1!=null&&page1.getResult()!=null&&page1.getResult().size()>0){
			List<InfoProductType> page1list=page1.getResult();
			for(InfoProductType info:page1list){

				InfoProductRS infoProductRS=new InfoProductRS();
				infoProductRS.setType(info.getId());
				Page<InfoProduct> page2=infoProductService.findInfoProductByPage(0, Integer.MAX_VALUE, infoProductRS);
				if(page2!=null&&page2.getResult()!=null&&page2.getResult().size()>0){
					List<InfoProduct> page2list=page2.getResult();
					ProductsVo vo=new ProductsVo();
					vo.setInfoProductType(info);
					vo.setInfoProductList(page2list);
					list.add(vo);
				}
			}
			//Page page2=infoProductService.findInfoProductByPage(0, Integer.MAX_VALUE, null);
		}
		modelMap.put("pList", list);
		return new ModelAndView("web/main/project", modelMap);
	}
	@RequestMapping("/register")
	public ModelAndView register(HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) {
		loadProjectTypes(modelMap);
		loadPages(modelMap);
		return new ModelAndView("web/main/register", modelMap);
	}
	@RequestMapping("/registerSave")
	public ModelAndView registerSave(@ModelAttribute InfoCustomerReq infoCustomerReq,HttpServletRequest request,
			HttpServletResponse response, ModelMap modelMap) {
		
		InfoCustomerRS infoCustomerRS=infoCustomerReq.getInfoCustomerRS();
		if(!infoCustomerRS.getPassword().equals(infoCustomerRS.getPassword())){
			modelMap.put("message", "俩次密码不一致");
			modelMap.put("infoCustomerRS", infoCustomerRS);
			return new ModelAndView("web/main/register", modelMap);
		}
		InfoCustomer infoCustomer=new InfoCustomer();
		infoCustomer.setEmail(infoCustomerRS.getEmail());
		infoCustomer.setUserName(infoCustomerRS.getUserName());
		infoCustomer.setPassword(infoCustomerRS.getPassword());
		infoCustomerService.infoCustomerSave(infoCustomer);
		loadProjectTypes(modelMap);
		loadPages(modelMap);
		return new ModelAndView("web/main/index", modelMap);
	}
	
	

	@RequestMapping("/infoMessageSave")
	public ModelAndView infoMessageAdd(
			@ModelAttribute InfoMessageReq infoMessageReq,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap) {
		InfoMessage infoMessage = infoMessageReq.getInfoMessage();
		infoMessageService.infoMessageSave(infoMessage);
		Result r = new Result();
		modelMap.put("message", "已经成功留言");
		modelMap.put("infoMessage", infoMessage);
		loadProjectTypes(modelMap);
		loadPages(modelMap);
		return new ModelAndView("web/main/contact", modelMap);
	}

	@RequestMapping("/page/{id}")
	public ModelAndView page(@PathVariable("id") Long id,
			HttpServletRequest request, HttpServletResponse response,
			ModelMap modelMap) {
		loadProjectTypes(modelMap);
		loadPages(modelMap);
		InfoPages page = infoPagesService.infoPagesGet(id);
		modelMap.put("page", page);
		return new ModelAndView("web/main/page", modelMap);
	}
}
