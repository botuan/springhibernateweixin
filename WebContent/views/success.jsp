<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include.inc.jsp"%>
<!DOCTYPE html>
<html lang="cn">
<head>
    <meta charset="utf-8">
    <title>操作成功 - 微信管理平台</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
	    
	<link href="${ctx }/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="${ctx }/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
	
	<link href="${ctx }/css/font-awesome.css" rel="stylesheet">
	 
	<link href="${ctx }/css/style.css" rel="stylesheet" type="text/css" />

</head>

<body>
	
	<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">
			
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<a class="brand" href="index.html">
				微信管理平台					
			</a>		
			
			<div class="nav-collapse">
				<ul class="nav pull-right">
					
					<li class="">						
						<a href="${ctx }/index.html" class="">
							<i class="icon-chevron-left"></i>
							返回首页
						</a>
						
					</li>
				</ul>
				
			</div><!--/.nav-collapse -->	
	
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->



<div class="container">
	
	<div class="row">
		
		<div class="span12">
			
			<div class="error-container">
				<img alt="操作无误" src="${ctx }/img/success.png" style="width:220px;heigt:220px;">
				<div class="error-details">
					<c:if test="${message==null||message==''}">
					操作正确，请返回
					</c:if>
					<c:if test="${message!=null&&message!=''}">
					${message}
					</c:if>
				</div> <!-- /error-details -->
				
				<div class="error-actions">
					<a href="${ctx}/${callbackUrl}.do${para}" class="btn btn-large btn-primary">
						<i class="icon-chevron-left"></i>
						&nbsp;
						返  回						
					</a>
				</div> <!-- /error-actions -->
							
			</div> <!-- /error-container -->			
			
		</div> <!-- /span12 -->
		
	</div> <!-- /row -->
	
</div> <!-- /container -->


<script src="${ctx }/js/jquery-1.7.2.min.js"></script>
<script src="${ctx }/js/bootstrap.js"></script>

</body>

</html>
