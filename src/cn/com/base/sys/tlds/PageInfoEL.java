package cn.com.base.sys.tlds;

import java.util.List;

import org.springframework.util.Assert;

import cn.com.base.sys.common.BusinessFactory;
import cn.com.base.sys.common.Page;
import cn.com.busi.model.InfoPages;
import cn.com.busi.model.InfoProduct;
import cn.com.busi.service.InfoPagesService;
import cn.com.busi.service.InfoProductService;

public class PageInfoEL {

	public static List<InfoPages> listPage() {
		InfoPagesService service = BusinessFactory.getInstance().getService(
				"infoPagesService");
		Assert.notNull(service, "服务类不能为空");
		List<InfoPages> pages = service.findCommPage();
		return pages;
	}

	public static List<InfoProduct> listProduct() {
		InfoProductService service = BusinessFactory.getInstance().getService(
				"infoProductService");
		Assert.notNull(service, "服务类不能为空");
		Page<InfoProduct> pages = service.findInfoProductByPage(0, 4, null);
		List<InfoProduct> list = pages.getResult();
		return list;
	}

}
