package cn.com.busi.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.base.sys.common.BaseJpaDao;
import cn.com.base.sys.common.Page;
import cn.com.base.sys.common.Query;
import cn.com.busi.model.InfoCustomer;
import cn.com.busi.service.InfoCustomerService;
import cn.com.base.sys.util.Strings;
import cn.com.busi.web.vo.InfoCustomerRS;

/**
 * ${tableBean.desc}的业务逻辑类
 * 
 * @author 谢林
 */
@Service(value = "infoCustomerService")
public class InfoCustomerServiceImpl implements InfoCustomerService {
	@Autowired
	/**
	 * jpa的操作类
	 */
	private BaseJpaDao baseJpaDao;
	static Logger log = Logger.getLogger(InfoCustomerServiceImpl.class);

	@Override
	/**
	 * jpa分页
	 */
	public Page<InfoCustomer> findInfoCustomerByPage(int pageNo, int pageSize,
			InfoCustomerRS infoCustomerRS) {
		log.info("正在执行  getInfoCustomerByPage()");
		Query query = baseJpaDao.createQuery(InfoCustomer.class);
		this.initQuery(query, infoCustomerRS);
		Page page = baseJpaDao.queryPage(query, pageNo, pageSize);
		log.info("执行 getInfoCustomerByPage()完毕，正在返回");
		return page;
	}

	/**
	 * 初始化查询条件
	 */
	private void initQuery(Query query, InfoCustomerRS infoCustomerRS) {
		log.info("正在执行  initQuery()");
		if (infoCustomerRS != null) {
		}

		log.info("执行 initQuery完毕，正在返回");
	}

	@Override
	/**
	 * 修改操作
	 */
	public InfoCustomer infoCustomerUpdate(InfoCustomer infoCustomer) {
		log.info("正在执行  infoCustomerUpdate(InfoCustomer)");
		baseJpaDao.update(infoCustomer);
		log.info("执行 infoCustomerUpdate完毕，正在返回");
		return infoCustomer;
	}

	@Override
	/**
	 * 新增操作
	 */
	public InfoCustomer infoCustomerSave(InfoCustomer infoCustomer) {
		log.info("正在执行  infoCustomerSave(infoCustomer)");
		baseJpaDao.save(infoCustomer);
		log.info("执行 infoCustomerSave完毕，正在返回");
		return infoCustomer;
	}

	@Override
	/**
	 * 删除操作
	 */
	public void infoCustomerDelete(InfoCustomer infoCustomer) {
		log.info("正在执行  infoCustomerDelete(InfoCustomer)");
		// baseJpaDao.delete(infoCustomer);
		log.info("执行 Delete()完毕，正在返回");
	}

	@Override
	/**
	 * 通过主键删除操作
	 */
	public void infoCustomerDelete(String email) {
		List<String> list = new ArrayList<String>();
		list.add(email);
		baseJpaDao.delete(InfoCustomer.class, list);
	}

	/**
	 * 通过主键查找一个记录
	 */
	public InfoCustomer infoCustomerGet(String addr) {
		log.info("正在执行  infoCustomerGet(Char)");
		InfoCustomer infoCustomer = baseJpaDao.get(InfoCustomer.class, addr);
		log.info("执行 infoCustomerGet完毕，正在返回");
		return infoCustomer;
	}

}
