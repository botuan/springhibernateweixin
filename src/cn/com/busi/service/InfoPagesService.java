package cn.com.busi.service;

import java.util.List;

import cn.com.base.sys.common.Page;
import cn.com.busi.model.InfoPages;
import cn.com.busi.web.vo.InfoPagesRS;

/**
 * 页面的业务逻辑接口
 * 
 * @author 谢林
 */
public interface InfoPagesService {
	/**
	 * 分页查询
	 */
	public Page<InfoPages> findInfoPagesByPage(int pageNo, int pageSize,
			InfoPagesRS infoPagesRS);

	/**
	 * 修改数据
	 */
	public InfoPages infoPagesUpdate(InfoPages infoPages);

	/**
	 * 新增数据
	 */
	public InfoPages infoPagesSave(InfoPages infoPages);

	/**
	 * 删除数据
	 */
	public void infoPagesDelete(InfoPages infoPages);

	/**
	 * 通过主键删除数据
	 */
	public void infoPagesDelete(Long id);

	/**
	 * 通过对象查询数据
	 */
	public InfoPages infoPagesGet(InfoPages infoPages);

	/**
	 * 通过主键查询数据
	 */
	public InfoPages infoPagesGet(Long id);
	/**
	 * 查询全部
	 * @return
	 */
	public List<InfoPages> queryAll();
	/**
	 * 查询全部
	 * @return
	 */
	public List<InfoPages> findCommPage();
}
