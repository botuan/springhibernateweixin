package cn.com.busi.web.vo;

import cn.com.base.sys.common.ReqModel;
import cn.com.busi.model.InfoImages;
/**
 * ${modelFieldBean.desc}
 * @author 谢林
 */
public class InfoImagesReq extends ReqModel {
	private static final long serialVersionUID = 1L;
	private InfoImages infoImages=new InfoImages();
	private InfoImagesRS infoImagesRS=new InfoImagesRS();
	public InfoImages getInfoImages() {
		return infoImages;
	}
	public void setInfoImages(InfoImages infoImages) {
		this.infoImages = infoImages;
	}
	public InfoImagesRS getInfoImagesRS() {
		return infoImagesRS;
	}
	public void setInfoImagesRS(InfoImagesRS infoImagesRS) {
		this.infoImagesRS = infoImagesRS;
	}
		
	
	
	private String bllId;
	private String bllType;
	private String bllFlag;
	private String bllBackUrl;
	private String values;
	
	
	public String getValues() {
		return values;
	}
	public void setValues(String values) {
		this.values = values;
	}
	public String getBllBackUrl() {
		return bllBackUrl;
	}
	public void setBllBackUrl(String bllBackUrl) {
		this.bllBackUrl = bllBackUrl;
	}
	public String getBllId() {
		return bllId;
	}
	public void setBllId(String bllId) {
		this.bllId = bllId;
	}
	public String getBllType() {
		return bllType;
	}
	public void setBllType(String bllType) {
		this.bllType = bllType;
	}
	public String getBllFlag() {
		return bllFlag;
	}
	public void setBllFlag(String bllFlag) {
		this.bllFlag = bllFlag;
	}
	
	
	    
}
