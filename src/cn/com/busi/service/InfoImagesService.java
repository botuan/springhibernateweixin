package cn.com.busi.service;

import cn.com.base.sys.common.Page;
import cn.com.busi.model.InfoImages;
import cn.com.busi.web.vo.InfoImagesRS;
/**
 * 图片的业务逻辑接口
 * @author 谢林
 */
public interface InfoImagesService {
	/**
	 * 分页查询
	 */
	public Page<InfoImages> findInfoImagesByPage(int pageNo, int pageSize,InfoImagesRS infoImagesRS);
	/**
	 * 修改数据
	 */
	public InfoImages infoImagesUpdate(InfoImages infoImages);
	/**
	 * 新增数据
	 */
	public InfoImages infoImagesSave(InfoImages infoImages);
	/**
	 * 删除数据
	 */
	public void infoImagesDelete(InfoImages infoImages);
	/**
	 * 通过主键删除数据
	 */

	public void infoNewsDelete(Long id);
	/**
	 * 通过对象查询数据
	 */
	public InfoImages infoImagesGet(InfoImages infoImages);
	/**
	 * 通过主键查询数据
	 */
	public InfoImages infoImagesGet(Long id);
			}
