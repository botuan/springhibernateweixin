package cn.com.busi.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.base.sys.common.BaseJpaDao;
import cn.com.base.sys.common.Page;
import cn.com.base.sys.common.Query;
import cn.com.busi.model.InfoProductImage;
import cn.com.busi.model.InfoProductType;
import cn.com.busi.model.WeiXinNewList;
import cn.com.busi.service.WeiXinNewListService;
import cn.com.busi.web.vo.InfoProductImageRS;
import cn.com.busi.web.vo.WeiXinNewListRS;

@Service(value = "weiXinNewListService")
public class WeiXinNewListServiceImpl implements WeiXinNewListService {
	@Autowired
	private BaseJpaDao baseJpaDao;
	static Logger log = Logger.getLogger(WeiXinNewListServiceImpl.class);
	@Override
	public Page<WeiXinNewList> findWeiXinNewListByPage(int pageNo,
			int pageSize, WeiXinNewListRS weiXinNewListRS) {
		log.info("正在执行  getInfoImagesByPage()");
		Query query = baseJpaDao.createQuery(WeiXinNewList.class);
		this.initQuery(query, weiXinNewListRS);
		Page<WeiXinNewList> page = baseJpaDao.queryPage(query, pageNo, pageSize);
		log.info("执行 getInfoImagesByPage()完毕，正在返回");
		return page;
	}
	/**
	 * 初始化查询条件
	 */
	private void initQuery(Query query,WeiXinNewListRS weiXinNewListRS) {
		
	}
	@Override
	public WeiXinNewList WeiXinNewListUpdate(WeiXinNewList weiXinNewList) {
		baseJpaDao.update(weiXinNewList);
		return weiXinNewList;
	}

	@Override
	public WeiXinNewList WeiXinNewListSave(WeiXinNewList weiXinNewList) {
		baseJpaDao.save(weiXinNewList);
		return weiXinNewList;
	}

	@Override
	public void WeiXinNewListDelete(WeiXinNewList WeiXinNewList) {
		// TODO Auto-generated method stub
		WeiXinNewListDelete(WeiXinNewList.getId());
	}

	@Override
	public WeiXinNewList WeiXinNewListGet(Long id) {
		WeiXinNewList weiXinNewList=baseJpaDao.get(WeiXinNewList.class, id);
		return weiXinNewList;
	}

	@Override
	public void WeiXinNewListDelete(Long id) {
		List<Long> list=new ArrayList<Long>();
		list.add(id);
		baseJpaDao.delete(WeiXinNewList.class, list);
	}

	@Override
	public WeiXinNewList WeiXinNewListGet(WeiXinNewList weiXinNewList) {

		weiXinNewList=baseJpaDao.get(WeiXinNewList.class, weiXinNewList.getId());
		return weiXinNewList;
	}

}
