package cn.com.base.sys.common;

import org.springframework.util.Assert;

public class BusinessFactory {

	private static BusinessFactory bf;

	private BusinessFactory() {

	}

	public static BusinessFactory getInstance() {
		if (bf == null) {
			synchronized (BusinessFactory.class) {
				bf = new BusinessFactory();
			}
		}
		return bf;
	}

	@SuppressWarnings("unchecked")
	public <T> T getService(String serviceName) {
		Assert.hasText(serviceName);
		return (T) SpringContextHolder.getBean(serviceName);
	}
}
