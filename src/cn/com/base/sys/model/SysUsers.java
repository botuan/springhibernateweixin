package cn.com.base.sys.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "SysUsers")
public class SysUsers implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8680337263599302062L;

	//用户id
	private String userId;
	
	//用户账号 与 用户id相同，具有唯一性。
	private String userAccount;
	
	//中文用户名。
	private String userName;
	
	//密码原文 ＋ 用户名作为盐值 的字串经过Md5加密后形成的密文。
	private String userPassword;
	
	//用户备注
	private String userDesc;
	
	//是否能用。
	private Integer enabled;
	
	//是否是超级用户。
	private Integer issys;
	
	//用户所在的单位。
	private String userDept;
	
	//用户的职位：比如主任、经理等。
	private String userDuty;
	
	//该用户所负责的子系统
	private String subSystem;

	@Id
	@Column(name="userId")
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	@Column(name="userAccount")
	public String getUserAccount() {
		return userAccount;
	}

	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

	@Column(name="userName")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name="userPassword")
	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	@Column(name="userDesc")
	public String getUserDesc() {
		return userDesc;
	}

	public void setUserDesc(String userDesc) {
		this.userDesc = userDesc;
	}

	@Column(name="enabled")
	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	@Column(name="issys")
	public Integer getIssys() {
		return issys;
	}

	public void setIssys(Integer issys) {
		this.issys = issys;
	}

	@Column(name="userDept")
	public String getUserDept() {
		return userDept;
	}

	public void setUserDept(String userDept) {
		this.userDept = userDept;
	}

	@Column(name="userDuty")
	public String getUserDuty() {
		return userDuty;
	}

	public void setUserDuty(String userDuty) {
		this.userDuty = userDuty;
	}

	@Column(name="subSystem")
	public String getSubSystem() {
		return subSystem;
	}

	public void setSubSystem(String subSystem) {
		this.subSystem = subSystem;
	}

}
