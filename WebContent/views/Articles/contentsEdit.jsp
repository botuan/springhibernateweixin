<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include.inc.jsp"%>
<!DOCTYPE html>
<html lang="cn">
  
<head>
    <meta charset="utf-8">
    <title>微信管理平台-微信内容编辑</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="${ctx }/css/bootstrap.min.css" rel="stylesheet">
    <link href="${ctx }/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="${ctx }/css/font-awesome.css" rel="stylesheet">
    <link href="${ctx }/css/style.css" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<style type="text/css">
#richtext {
            height: 500px;
        }
</style>
  </head>

<body>

<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<a class="brand" href="index.html">
				微信管理平台				
			</a>		
			
			<div class="nav-collapse">
				 <ul class="nav pull-right">
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-cog"></i> 账户 <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">设置</a></li>
              <li><a href="javascript:;">帮助</a></li>
            </ul>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> 用户名称<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">个人中心</a></li>
              <li><a href="javascript:;">退出</a></li>
            </ul>
          </li>
        </ul>
        <form class="navbar-search pull-right">
          <input type="text" class="search-query" placeholder="搜索">
        </form>
			</div><!--/.nav-collapse -->	
	
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->
    



    
<div class="subnavbar">
	<div class="subnavbar-inner">
		<div class="container">
			<ul class="mainnav">
        <li><a href="${ctx }/index.do"><i class="icon-home "></i><span>首页</span> </a> </li>
        <li><a href="${ctx }/admin/infoNewsQuery.do"><i class="icon-list-alt"></i><span>新闻动态</span> </a> </li>
        <li><a href="${ctx }/admin/infoImagesQuery.do"><i class=" icon-picture"></i><span>图片管理</span> </a></li>
         <li  class="active"><a href="${ctx }/admin/weiXinNewListeQuery.do"><i class="icon-list "></i><span>微信图文</span> </a> </li>
       <li><a href="charts.html"><i class="icon-bar-chart"></i><span>Charts</span> </a> </li>
        <li><a href="shortcodes.html"><i class="icon-code"></i><span>Shortcodes</span> </a> </li>
        <!--<li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Drops</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="icons.html">Icons</a></li>
            <li><a href="faq.html">FAQ</a></li>
            <li><a href="pricing.html">Pricing Plans</a></li>
            <li><a href="login.html">Login</a></li>
            <li><a href="signup.html">Signup</a></li>
            <li><a href="error.html">404</a></li>
          </ul>
        </li>-->
      </ul>

		</div> <!-- /container -->
	
	</div> <!-- /subnavbar-inner -->

</div> <!-- /subnavbar -->
    
    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			<!--
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>内容编辑</h3>
	      				
	  				</div>  /widget-header -->
					<div class="widget-content">
			<p>微信的文章内容中如过含有链接标签，请确认微信公众是否拥挤支付功能，如果没有支付功能，请去掉超链接</p>
						<form id="edit-profile" class="form-horizontal" method="post" action="${ctx }/admin/weiXinNewsEdit.do">
									<fieldset>
										<input type="hidden"  name="weiXinNews.id" value="${weiXinNews.id }" >
										<textarea rows="60" name="weiXinNews.content" style="width: 100%;">
											${weiXinNews.content }
										</textarea>
										<div class="form-actions">
											<button type="submit" class="btn btn-primary btn-large icon-save">保存</button> 
											<a class="btn icon-undo  btn-large" href="${ctx }/admin/infoNewsInQuery.do?weiXinNewsRS.weiXinNewListId=${weiXinNews.weiXinNewListId }">取消</a>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 

<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    <div class="span3">
                        <h4>软件定制</h4>
                        <ul>
                            <li><a href="javascript:;">企业网站</a></li>
                            <li><a href="javascript:;">ERP管理系统</a></li>
                            <li><a href="javascript:;">财务系统</a></li>
                            <li><a href="javascript:;">分销系统</a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>
                            App开发</h4>
                        <ul>
                            <li><a href="javascript:;">crm客户管理APP</a></li>
                            <li><a href="javascript:;">通讯录App</a></li>
                            <li><a href="javascript:;">娱乐App</a></li>
                            <li><a href="javascript:;">业务定制App</a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>
                            微信开发</h4>
                        <ul>
                            <li><a href="javascript:;">微信商城</a></li>
                            <li><a href="javascript:;">微信支付</a></li>
                            <li><a href="javascript:;">微信营销</a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>
                            电子商务</h4>
                        <ul>
                            <li><a href="http://sc.chinaz.com">商店系统</a></li>
                            <li><a href="http://sc.chinaz.com;">商城系统</a></li>
                            <li><a href="http://sc.chinaz.com;">众筹系统</a></li>
                            <li><a href="http://sc.chinaz.com;">特别定制商城</a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015-2020 <a href="http://main.wolf1688.com/index.html">雅狼工作</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
      


<script src="${ctx}/js/jquery-1.7.2.min.js"></script>
<script src="${ctx}/js/bootstrap.js"></script>
<script src="${ctx}/js/base.js"></script>
		<script src="${ctx}/kindeditor/kindeditor-all.js"></script>
		<script>
			//document.domain = 'domain.com';
			KindEditor.ready(function(K) {
				window.editor = K.create('textarea', {
					allowFileManager : true,
					langType : 'zh-CN',
					autoHeightMode : true,
					cssData : ['${ctx }/css/bootstrap.min.css',"${ctx }/css/bootstrap-responsive.min.css","${ctx }/css/font-awesome.css","${ctx }/css/style.css"] 
				});
			});
		</script>
  </body>

</html>
