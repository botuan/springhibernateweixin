package cn.com.base.sys.exception;

public class BaseException extends RuntimeException{

	private String message;
	
	
	private Throwable throwable;
	
	@Override
	public String getLocalizedMessage() {
		 return this.message;
	}
	
	@Override
	public String getMessage() {
		 return this.message;
	}
	
	
	public BaseException(String message){
		this.message=message;
	}
	
	
	public BaseException(Throwable throwable){
		this.throwable=throwable;
		this.message=this.throwable.getMessage();
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
