package cn.com.busi.web.vo;

import java.util.Date;

/**
 * ${modelFieldBean.desc}
 * @author 谢林
 */
public class InfoNewsRS implements java.io.Serializable {
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		/**
	 * 返回 创建时间
	 */
	private Date createDate;
	/**
	 * 返回 创建时间
	 * @return
	 */
	public Date getCreateDate (){
	 return createDate;
	}
	/**
	 * 设置 创建时间
	 * @param createDate
	 */
	public void setCreateDate (Date createDate){
	 this.createDate=createDate;
	}
	/**
	 * 返回 记录状态
	 */
	private String validStatus;
	/**
	 * 返回 记录状态
	 * @return
	 */
	public String getValidStatus (){
	 return validStatus;
	}
	/**
	 * 设置 记录状态
	 * @param validStatus
	 */
	public void setValidStatus (String validStatus){
	 this.validStatus=validStatus;
	}
	/**
	 * 返回 创建人
	 */
	private String createUser;
	/**
	 * 返回 创建人
	 * @return
	 */
	public String getCreateUser (){
	 return createUser;
	}
	/**
	 * 设置 创建人
	 * @param createUser
	 */
	public void setCreateUser (String createUser){
	 this.createUser=createUser;
	}
	/**
	 * 返回 修改时间
	 */
	private Date updateDate;
	/**
	 * 返回 修改时间
	 * @return
	 */
	public Date getUpdateDate (){
	 return updateDate;
	}
	/**
	 * 设置 修改时间
	 * @param updateDate
	 */
	public void setUpdateDate (Date updateDate){
	 this.updateDate=updateDate;
	}
	/**
	 * 返回 修改人
	 */
	private String updateUser;
	/**
	 * 返回 修改人
	 * @return
	 */
	public String getUpdateUser (){
	 return updateUser;
	}
	/**
	 * 设置 修改人
	 * @param updateUser
	 */
	public void setUpdateUser (String updateUser){
	 this.updateUser=updateUser;
	}
	/**
	 * 返回 主键
	 */
	private Long id;
	/**
	 * 返回 主键
	 * @return
	 */
	public Long getId (){
	 return id;
	}
	/**
	 * 设置 主键
	 * @param id
	 */
	public void setId (Long id){
	 this.id=id;
	}
	/**
	 * 返回 标题
	 */
	private String title;
	/**
	 * 返回 标题
	 * @return
	 */
	public String getTitle (){
	 return title;
	}
	/**
	 * 设置 标题
	 * @param title
	 */
	public void setTitle (String title){
	 this.title=title;
	}
	/**
	 * 返回 标题描述
	 */
	private String titleDesc;
	/**
	 * 返回 标题描述
	 * @return
	 */
	public String getTitleDesc (){
	 return titleDesc;
	}
	/**
	 * 设置 标题描述
	 * @param titleDesc
	 */
	public void setTitleDesc (String titleDesc){
	 this.titleDesc=titleDesc;
	}
	/**
	 * 返回 内容
	 */
	private String contents;
	/**
	 * 返回 内容
	 * @return
	 */
	public String getContents (){
	 return contents;
	}
	/**
	 * 设置 内容
	 * @param contents
	 */
	public void setContents (String contents){
	 this.contents=contents;
	}
	/**
	 * 返回 状态
	 */
	private String status;
	/**
	 * 返回 状态
	 * @return
	 */
	public String getStatus (){
	 return status;
	}
	/**
	 * 设置 状态
	 * @param status
	 */
	public void setStatus (String status){
	 this.status=status;
	}
	/**
	 * 返回 类型
	 */
	private String type;
	/**
	 * 返回 类型
	 * @return
	 */
	public String getType (){
	 return type;
	}
	/**
	 * 设置 类型
	 * @param type
	 */
	public void setType (String type){
	 this.type=type;
	}
	
	private String imageUrl;
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	
}