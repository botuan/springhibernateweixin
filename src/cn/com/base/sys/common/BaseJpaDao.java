package cn.com.base.sys.common;
 
import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
 
/**
 * 查询基本DAO
 * @author Administrator
 *
 */
public interface BaseJpaDao {
     
    public EntityManager getEntityManager();
    
    
    /**
     * 创建复杂查询器
     * @param clazz
     * @return
     */
    public Query createQuery(Class clazz);
    
 
    public <E> E get(Class clazz, Serializable id);
 
    /**
     * 插入记录
     * 
     * @param entity
     *            要插入的记录
     */
    public void save(Object entity);
 
    /**
     * 更新记录
     * 
     * @param entity
     *            要更新的记录
     */
    public void update(Object entity);
 
    /** 更新list */
    public void updateList(List list);
 
    /**
     * 删除记录
     * 
     * @param entity
     *            要删除的记录
     */
    public void delete(Object entity);
    
    
    /**
     * 删除记录
     * 
     * @param entity
     *            要删除的记录
     */
    public void delete(Class entity, List ids);
 
    /**
     * 删除记录
     * 
     * @param entity
     *            要删除的记录
     */
    public void delete(Class entity, String jpqlCondition);
 
    /**
     * 统计记录
     * 
     * @param query
     *            统计条件
     */
    public Long getCount(Query query);
 
    public Long getCount(String jpql,Object... values);
 
    /**
     * 分页查询
     * 
     * @param query
     *            查询条件
     * @param pageNo
     *            页号
     * @param rowsPerPage
     *            每页显示条数
     */
    public Page queryPage(Query query, int pageNo, int rowsPerPage);
 
    /**
     * 根据query查找记录
     * 
     * @param query
     *            查询条件
     * @param firstResult
     *            起始行
     * @param maxResults
     *            结束行
     */
    public <E extends Serializable> List<E> query(Query query, int firstResult, int maxResults);
 
    /**
     * 根据query查找记录
     * 
     * @param query
     *            查询条件
     */
    public <E extends Serializable> List<E> query(Query query);
 
    /**
     * 执行更新操作的jpql语句
     * 
     * @param jpql
     *            要执行的jpql语句
     */
    public <E extends Serializable> List<E> query(String jpql,Object... values);
 
    public <E extends Serializable> List<E> queryAll(Class clazz);
 
    public <E extends Serializable> List<E> query(String jpql, int firstResult, int maxResults,Object... values);
 
    /**
     * 执行查询操作的sql语句
     * 
     * @param sql
     *            要执行的sql语句
     */
    public <E extends Serializable> List<E> queryBySql(String sql,Class clazz,Object... values);
 
    public <E extends Serializable> List<E> queryListBySql(String sql,Class clazz, int firstResult, int maxResults,Object... values);
 
    /**
     * 查询记录
     * 
     * @param clazz
     *            要查询的实体类
     * @param hqlCondition
     *            查询条件
     */
    public <E extends Serializable> List<E> query(Class clazz, String hqlCondition,Object... values);
 
    /**
     * 执行更新操作的sql语句
     * 
     * @param sql
     *            要执行的sql语句
     */
    public Integer updateSql(String sql,Object... values);
 
    public Integer updateJpql(String jpql,Object... values);
 
    public Page queryPageByJpql(String hql, int pageNo, int rowsPerPage,Object... values);
 
    public void updateJpql(String jpql, List paramList);
    
    public Page queryPageBySql(String sql, Class clazz,int pageNo, int rowsPerPage,Object...values);
 
}