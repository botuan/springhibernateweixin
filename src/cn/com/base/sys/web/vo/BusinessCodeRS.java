package cn.com.base.sys.web.vo;

import java.util.Date;

/**
 * ${modelFieldBean.desc}
 * @author 胡呈
 */
public class BusinessCodeRS implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	
		public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
		/**
	 * 返回 代码值
	 */
	private String codeCode;
	/**
	 * 返回 代码值
	 * @return
	 */
	public String getCodeCode (){
	 return codeCode;
	}
	/**
	 * 设置 代码值
	 * @param codeCode
	 */
	public void setCodeCode (String codeCode){
	 this.codeCode=codeCode;
	}
	/**
	 * 返回 代码类型
	 */
	private String codeType;
	/**
	 * 返回 代码类型
	 * @return
	 */
	public String getCodeType (){
	 return codeType;
	}
	/**
	 * 设置 代码类型
	 * @param codeType
	 */
	public void setCodeType (String codeType){
	 this.codeType=codeType;
	}
	/**
	 * 返回 中文名称
	 */
	private String codeCName;
	/**
	 * 返回 中文名称
	 * @return
	 */
	public String getCodeCName (){
	 return codeCName;
	}
	/**
	 * 设置 中文名称
	 * @param codeCName
	 */
	public void setCodeCName (String codeCName){
	 this.codeCName=codeCName;
	}
	/**
	 * 返回 英文名称
	 */
	private String codeEEname;
	/**
	 * 返回 英文名称
	 * @return
	 */
	public String getCodeEEname (){
	 return codeEEname;
	}
	/**
	 * 设置 英文名称
	 * @param codeEEname
	 */
	public void setCodeEEname (String codeEEname){
	 this.codeEEname=codeEEname;
	}
	/**
	 * 返回 标识自动
	 */
	private String flag;
	/**
	 * 返回 标识自动
	 * @return
	 */
	public String getFlag (){
	 return flag;
	}
	/**
	 * 设置 标识自动
	 * @param flag
	 */
	public void setFlag (String flag){
	 this.flag=flag;
	}
	/**
	 * 返回 数据插入时间
	 */
	private Date insertTimeForHis;
	private String insertTimeForHisStr;
	/**
	 * 返回 数据插入时间
	 * @return
	 */
	public Date getInsertTimeForHis (){
	 return insertTimeForHis;
	}
	/**
	 * 设置 数据插入时间
	 * @param insertTimeForHis
	 */
	public void setInsertTimeForHis (Date insertTimeForHis){
	 this.insertTimeForHis=insertTimeForHis;
	}
	/**
	 * 返回 新代码值
	 */
	private String newCodeCode;
	/**
	 * 返回 新代码值
	 * @return
	 */
	public String getNewCodeCode (){
	 return newCodeCode;
	}
	/**
	 * 设置 新代码值
	 * @param newCodeCode
	 */
	public void setNewCodeCode (String newCodeCode){
	 this.newCodeCode=newCodeCode;
	}
	/**
	 * 返回 修改时间
	 */
	private Date operateTimeForHis;
	private String operateTimeForHisStr;
	/**
	 * 返回 修改时间
	 * @return
	 */
	public Date getOperateTimeForHis (){
	 return operateTimeForHis;
	}
	/**
	 * 设置 修改时间
	 * @param operateTimeForHis
	 */
	public void setOperateTimeForHis (Date operateTimeForHis){
	 this.operateTimeForHis=operateTimeForHis;
	}
	/**
	 * 返回 有效状态
	 */
	private String validStatus;
	private String validStatusName;
	/**
	 * 返回 有效状态
	 * @return
	 */
	public String getValidStatus (){
	 return validStatus;
	}
	/**
	 * 设置 有效状态
	 * @param validStatus
	 */
	public void setValidStatus (String validStatus){
	 this.validStatus=validStatus;
	}
	/**
	 * 返回 系统版本
	 */
	private Integer version;
	/**
	 * 返回 系统版本
	 * @return
	 */
	public Integer getVersion (){
	 return version;
	}
	/**
	 * 设置 系统版本
	 * @param version
	 */
	public void setVersion (Integer version){
	 this.version=version;
	}
	public String getInsertTimeForHisStr() {
		return insertTimeForHisStr;
	}
	public void setInsertTimeForHisStr(String insertTimeForHisStr) {
		this.insertTimeForHisStr = insertTimeForHisStr;
	}
	public String getOperateTimeForHisStr() {
		return operateTimeForHisStr;
	}
	public void setOperateTimeForHisStr(String operateTimeForHisStr) {
		this.operateTimeForHisStr = operateTimeForHisStr;
	}
	public String getValidStatusName() {
		return validStatusName;
	}
	public void setValidStatusName(String validStatusName) {
		this.validStatusName = validStatusName;
	}
	
}