package cn.com.base.sys.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import cn.com.base.sys.exception.NoAdminException;
import cn.com.base.sys.exception.NoLoginException;
import cn.com.base.sys.util.Strings;

public abstract class SessionManager implements SessionManagerService {

	
	
	public static String loginStatus="loginStatus";
	public static String statusYes="yes";
	public static String statusNo="no";
	public static String loginType="loginType";
	public static String typeAdmin="admin";
	public static String typeUser="user";

	public static String user="user";
	@Override
	public boolean confirmLogin(HttpServletRequest request) {
		HttpSession session = request.getSession();
		Object status = session.getAttribute(SessionManager.loginStatus);
		if (Strings.isNullOrEmpty(status)) {
			return false;
		}
		return true;
	}

	@Override
	public void loginValidate(HttpServletRequest request) {
		HttpSession session = request.getSession();
		Object status = session.getAttribute(SessionManager.loginStatus);
		if (Strings.isNullOrEmpty(status)) {
			throw new NoLoginException("用户没有登录");
		}
	}

	@Override
	public void adminLoginValidate(HttpServletRequest request) {
		HttpSession session = request.getSession();

		System.out.println("session:"+session.getId());
		Object status = session.getAttribute(SessionManager.loginStatus);
		if (Strings.isNullOrEmpty(status)) {
			throw new NoAdminException("用户没有登录");
		}
		Object loginType = session.getAttribute(SessionManager.loginType);
		if (Strings.isNullOrEmpty(loginType)) {
			throw new NoAdminException("无效账号");
		}
		String loginTypes = String.valueOf(loginType);
		if (!SessionManager.typeAdmin.equals(loginTypes)) {
			throw new NoAdminException("无效账号");
		}
	}

	@Override
	public Object findLogonUser(HttpServletRequest request) {
		this.loginValidate(request);
		HttpSession session = request.getSession();
		Object user = session.getAttribute(SessionManager.user);
		return user;
	}

}
