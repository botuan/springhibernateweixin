package cn.com.base.sys.service;

import java.util.List;

import org.springframework.ui.ModelMap;

import cn.com.base.sys.model.BusinessCode;
import cn.com.base.sys.web.vo.BusinessCodeRS;

public interface CodeInputService {
    static String select="select";
    static String AREA="area";
	public void codeQuery(String codeType,String inputType,String param,ModelMap modelMap);
	public void codeQuery(String codeType,String codeCode,String inputType,String param,ModelMap modelMap);
	public List<BusinessCode> codeQuery( BusinessCodeRS businessCodeRS);
    public String translator(String codeType,String codeCode);
}
