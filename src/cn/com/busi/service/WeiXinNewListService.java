package cn.com.busi.service;

import cn.com.base.sys.common.Page;
import cn.com.busi.model.WeiXinNewList;
import cn.com.busi.web.vo.WeiXinNewListRS;

public interface WeiXinNewListService {
	/**
	 * 分页查询
	 */
	public Page<WeiXinNewList> findWeiXinNewListByPage(int pageNo,
			int pageSize, WeiXinNewListRS WeiXinNewListRS);

	/**
	 * 修改数据
	 */
	public WeiXinNewList WeiXinNewListUpdate(WeiXinNewList WeiXinNewList);

	/**
	 * 新增数据
	 */
	public WeiXinNewList WeiXinNewListSave(WeiXinNewList WeiXinNewList);

	/**
	 * 删除数据
	 */
	public void WeiXinNewListDelete(WeiXinNewList WeiXinNewList);

	/**
	 * 通过主键查询数据
	 */
	public WeiXinNewList WeiXinNewListGet(Long id);

	/**
	 * 通过主键删除数据
	 */
	public void WeiXinNewListDelete(Long id);

	/**
	 * 通过对象查询数据
	 */
	public WeiXinNewList WeiXinNewListGet(WeiXinNewList WeiXinNewList);
}
