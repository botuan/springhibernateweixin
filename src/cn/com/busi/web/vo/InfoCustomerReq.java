package cn.com.busi.web.vo;

import cn.com.base.sys.common.ReqModel;
import cn.com.busi.model.InfoCustomer;
/**
 * ${tableBean.desc}
 * @author л��
 */
public class InfoCustomerReq extends ReqModel {
	private static final long serialVersionUID = 1L;
	private InfoCustomer infoCustomer=new InfoCustomer();
	private InfoCustomerRS infoCustomerRS=new InfoCustomerRS();
	public InfoCustomer getInfoCustomer() {
		return infoCustomer;
	}
	public void setInfoCustomer(InfoCustomer infoCustomer) {
		this.infoCustomer = infoCustomer;
	}
	public InfoCustomerRS getInfoCustomerRS() {
		return infoCustomerRS;
	}
	public void setInfoCustomerRS(InfoCustomerRS infoCustomerRS) {
		this.infoCustomerRS = infoCustomerRS;
	}
	
	
		
	/**
	 *����
	 */
	private String email=new String();
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
