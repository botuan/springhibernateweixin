package cn.com.base.sys.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import cn.com.base.sys.common.BaseJpaDao;
import cn.com.base.sys.common.Constants;
import cn.com.base.sys.common.Page;
import cn.com.base.sys.common.Query;
import cn.com.base.sys.model.BusinessCode;
import cn.com.base.sys.service.BusinessCodeService;
import cn.com.base.sys.service.CodeInputService;
import cn.com.base.sys.util.DataUtil;
import cn.com.base.sys.util.DateUtil;
import cn.com.base.sys.util.Strings;
import cn.com.base.sys.web.vo.BusinessCodeRS;
/**
 * 代码字典的业务逻辑类
 * @author 胡呈
 */
@Service(value="businessCodeService")
public class BusinessCodeServiceImpl implements BusinessCodeService {
	@Autowired
	/**
	 * jpa的操作类
	 */
	private BaseJpaDao baseJpaDao;
	@Autowired
	private CodeInputService codeInputService;
    static Logger log=Logger.getLogger(BusinessCodeServiceImpl.class);
	@Override
	/**
	 * jpa分页
	 */
	public Page<BusinessCodeRS> findBusinessCodeByPage(int pageNo,
			int pageSize, BusinessCodeRS businessCodeRS) {
		log.info("正在执行翻译函数 getBusinessCodeByPage(),pageNo="+pageNo+";pageSize"+pageSize);
		Query query=baseJpaDao.createQuery(BusinessCode.class);
		this.initQuery(query, businessCodeRS);
		Page<BusinessCode> page=baseJpaDao.queryPage(query, pageNo, pageSize);
		Page<BusinessCodeRS> newPage= new Page<BusinessCodeRS>();
		DataUtil.copy(newPage, page, false);
		newPage.setResult(copyList(page.getResult()));
		log.info("执行翻译函数  getBusinessCodeByPage()完毕，正在返回");
		return newPage;
	}
	@Override
	public List<BusinessCode> findBusinessCodeList(BusinessCodeRS businessCodeRS) {
		log.info("正在执行  findBusinessCodeList()");
		Query query=baseJpaDao.createQuery(BusinessCode.class);
		this.initQuery(query, businessCodeRS);
		List<BusinessCode> list=baseJpaDao.query(query);
		log.info("执行 findBusinessCodeList()完毕，正在返回");
		return list;
	}
	private void copy(BusinessCode model,BusinessCodeRS rs){
		if(model!=null){
			DataUtil.copy(rs, model, false);
			translator(rs);
		}else{
			if(rs==null){
				rs=new BusinessCodeRS();
			}
		}
	}
	
	private void copyList(List<BusinessCode> modelList,List<BusinessCodeRS> rsList){
		if(rsList==null)rsList=new ArrayList<BusinessCodeRS>();
		for(BusinessCode model:modelList){
			BusinessCodeRS rs=new BusinessCodeRS();
			copy(model, rs);
			rsList.add(rs);
		}
		
	}
	private List<BusinessCodeRS> copyList(List<BusinessCode> modelList){
		List<BusinessCodeRS> rsList=new ArrayList<BusinessCodeRS>();
		copyList(modelList,rsList);
		return rsList;
		
	}
	/**
	 * 字典翻译
	 * @param rs
	 */
	private void translator(BusinessCodeRS rs){
		if(rs==null)return;
		rs.setValidStatusName(codeInputService.translator(Constants.YesOrNo.codeType, Strings.trim(rs.getValidStatus())));
		rs.setInsertTimeForHisStr(DateUtil.toDateString(rs.getInsertTimeForHis()));
		rs.setOperateTimeForHisStr(DateUtil.toDateString(rs.getOperateTimeForHis()));
	}
   /**
	 * 初始化查询条件
	 */
	private void initQuery(Query query,BusinessCodeRS businessCodeRS){
		log.info("正在执行  initQuery()");
		if(businessCodeRS!=null){
							if(!Strings.isNullOrEmpty(businessCodeRS.getCodeCName())){
				query.eq("codeCName",businessCodeRS.getCodeCName());
			}
					if(!Strings.isNullOrEmpty(businessCodeRS.getCodeEEname())){
				query.eq("codeEEname",businessCodeRS.getCodeEEname());
			}
					if(!Strings.isNullOrEmpty(businessCodeRS.getFlag())){
				query.eq("flag",businessCodeRS.getFlag());
			}
					if(!Strings.isNullOrEmpty(businessCodeRS.getInsertTimeForHis())){
				query.eq("insertTimeForHis",businessCodeRS.getInsertTimeForHis());
			}
					if(!Strings.isNullOrEmpty(businessCodeRS.getNewCodeCode())){
				query.eq("newCodeCode",businessCodeRS.getNewCodeCode());
			}
					if(!Strings.isNullOrEmpty(businessCodeRS.getOperateTimeForHis())){
				query.eq("operateTimeForHis",businessCodeRS.getOperateTimeForHis());
			}
					if(!Strings.isNullOrEmpty(businessCodeRS.getValidStatus())){
				query.eq("validStatus",businessCodeRS.getValidStatus());
			}
					if(!Strings.isNullOrEmpty(businessCodeRS.getVersion())){
				query.eq("version",businessCodeRS.getVersion());
			}
							if(!Strings.isNullOrEmpty(businessCodeRS.getCodeCode())){
				query.eq("codeCode",businessCodeRS.getCodeCode());
			}
					if(!Strings.isNullOrEmpty(businessCodeRS.getCodeType())){
				query.eq("codeType",businessCodeRS.getCodeType());
			}
						}
		
		log.info("执行 initQuery完毕，正在返回");
	}

	@Override
	/**
	 * 修改操作
	 */
	public BusinessCode businessCodeUpdate(BusinessCode businessCode) {
		log.info("正在执行  businessCodeUpdate(BusinessCode)");
		baseJpaDao.update(businessCode);
		log.info("执行 businessCodeUpdate完毕，正在返回");
		return businessCode;
	}

	@Override
	
	/**
	 * 新增操作
	 */
	public BusinessCode businessCodeSave(BusinessCode businessCode) {
		log.info("正在执行  businessCodeSave(businessCode)");
		baseJpaDao.save(businessCode);
		log.info("执行 businessCodeSave完毕，正在返回");
		return businessCode;
	}

	@Override
	
	/**
	 * 删除操作
	 */
	public void businessCodeDelete(BusinessCode businessCode) {
		log.info("正在执行  businessCodeDelete(BusinessCode)");
		//baseJpaDao.delete(businessCode);
				businessCodeDelete(businessCode.getId());
				log.info("执行 Delete()完毕，正在返回");
	}
	
	@Override
	/**
	 * 通过主键删除操作
	 */
    	/*public void businessCodeDelete(BusinessCodeId businessCodeId){
	    List<BusinessCodeId> list=new ArrayList<BusinessCodeId>();
		list.add(businessCodeId);
		baseJpaDao.delete(BusinessCode.class, list);
	}*/
	public void businessCodeDelete(Long businessCodeId){
	    List<Long> list=new ArrayList<Long>();
		list.add(businessCodeId);
		baseJpaDao.delete(BusinessCode.class, list);
	}
		@Override
	
	/**
	 * 通过对象查找一个记录，对象中主键必须有
	 */
	 
		public BusinessCode businessCodeGet(BusinessCode businessCode) {
		log.info("正在执行  businessCodeGet(BusinessCode)");
		businessCode=businessCodeGet(businessCode.getId());
		log.info("执行 businessCodeGet 完毕，正在返回");
		return businessCode;
	}
		@Override
		/**
	 * 通过主键查找一个记录
	 */
	/*public BusinessCode businessCodeGet(BusinessCodeId businessCodeId) {
		log.info("正在执行  businessCodeGet(businessCodeId)");
		BusinessCode businessCode=baseJpaDao.get(BusinessCode.class, businessCodeId);
		
		log.info("执行 businessCodeGet完毕，正在返回");
		return businessCode;
	}*/
		public BusinessCode businessCodeGet(Long businessCodeId) {
			log.info("正在执行  businessCodeGet(businessCodeId)");
			BusinessCode businessCode=baseJpaDao.get(BusinessCode.class, businessCodeId);
			
			log.info("执行 businessCodeGet完毕，正在返回");
			return businessCode;
		}
		@Override
		public String translate(String codeType, String codeCode) {
			Query query=baseJpaDao.createQuery(BusinessCode.class);
			query.eq("codeType", codeType);
			query.eq("codeCode", codeCode);
			List<BusinessCode> list=baseJpaDao.query(query);
			if(null!=list&&list.size()>0){
				BusinessCode code=list.get(0);
				return code.getCodeCName();
			}
			return "";
			
			
			
		}
		
		
		
}
