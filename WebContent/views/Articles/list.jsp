<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include.inc.jsp"%>
<!DOCTYPE html>
<html lang="cn">
<head>
<meta charset="utf-8">
<title>微信管理平台-图片详细列表</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="${ctx}/css/bootstrap.min.css" rel="stylesheet">
<link href="${ctx}/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="${ctx}/css/font-awesome.css" rel="stylesheet">
<link href="${ctx}/css/style.css" rel="stylesheet">
<link href="${ctx}/css/pages/dashboard.css" rel="stylesheet">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="index.html">微信管理平台 </a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-cog"></i> 账户 <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">设置</a></li>
              <li><a href="javascript:;">帮助</a></li>
            </ul>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> 用户名称<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">个人中心</a></li>
              <li><a href="javascript:;">退出</a></li>
            </ul>
          </li>
        </ul>
        <form class="navbar-search pull-right">
          <input type="text" class="search-query" placeholder="搜索">
        </form>
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        <li><a href="${ctx }/index.do"><i class="icon-home "></i><span>首页</span> </a> </li>
        <li><a href="${ctx }/admin/infoNewsQuery.do"><i class="icon-list-alt"></i><span>新闻动态</span> </a> </li>
        <li><a href="${ctx }/admin/infoImagesQuery.do"><i class=" icon-picture"></i><span>图片管理</span> </a></li>
        <li   class="active"><a href="${ctx }/admin/weiXinNewListeQuery.do"><i class="icon-list "></i><span>微信图文</span> </a> </li>
       <!--<li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Drops</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="icons.html">Icons</a></li>
            <li><a href="faq.html">FAQ</a></li>
            <li><a href="pricing.html">Pricing Plans</a></li>
            <li><a href="login.html">Login</a></li>
            <li><a href="signup.html">Signup</a></li>
            <li><a href="error.html">404</a></li>
          </ul>
        </li>-->
      </ul>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>
<!-- /subnavbar -->
<div class="main">
  <div class="main-inner">
    <div class="container">
	<div  class="row">
	<div class="span12">
	     <div id="target-1" class="widget">
	     <div class="widget-header"> <i class="icon-file"></i>
              <h3> 群发列表</h3>
            </div>
	      		<div class="widget-content">
	      	<form id="searchForm" class="form-horizontal" method="${ctx }/admin/infoNewsInQuery.do?weiXinNewsRS.weiXinNewListId=${weiXinNewsRS.weiXinNewListId}">
				<fieldset>
				<div class="input-append">
					 <input class="span10 m-wrap" id="appendedInputButton" placeholder="关键字" type="text">
					<button class="btn" type="submit">搜索</button>
					<a href="${ctx }/admin/prepareInfoImagesAdd.do" class="btn" type="button">上传</a>
				</div
				</fieldset>
			</form>	
			<p></p>
			<p>微信的文章内容中如何含有链接标签，请确认微信公众是否拥挤支付功能，如果没有支付功能，请去掉超链接</p>
	      		 <table class="table table-striped table-bordered">
	                <thead>
	                  <tr>
	                    <th> 封面图</th>
	                    <th> 标题 </th>
	                    <th> 摘要</th>
	                    <th class="td-actions"> </th>
	                  </tr>
	                </thead>
	                <tbody>
	                <c:forEach var="infoNew" items="${page.result}" varStatus="s">
						<tr>
							<td>
							<img src="${infoNew.imageUrl}" style="height:50px;width:80px;">
							</td>
							<td>${infoNew.title}</td>
							<td>${infoNew.titleDesc}</td>
						    <td class="td-actions"><a href="${ctx }/admin/prepareInfoNewsEdit.do?id=${infoNew.id}" class="btn btn-small btn-success"><i class="btn-icon-only icon-pencil"> </i>基本信息</a><a href="${ctx }/admin/prepareInfoNewsEditLast.do?id=${infoNew.id}" class="btn btn-small btn-success"><i class="btn-icon-only icon-pencil"> </i>内容信息</a><a href="${ctx }/admin/infoNewsDelete.do?id=${infoNew.id}" class="btn btn-danger btn-small"><i class="btn-icon-only icon-remove"> </i>删除</a></td>
	                   </tr>
					</c:forEach>
	                </tbody>
	              </table>
              <c:if test="${page.pageNo>0}">
                <ul class="pagination pull-right">
					   <c:if test="${page.pageNo>1}">
						 <li><a href="#"  class="pageBtn" pageNo="${page.pageNo-1}">&laquo;</a></li>
					   </c:if>
					  <c:forEach var="no" begin="1" end="${page.pageNo-1}">
						 <li><a href="#" class="pageBtn" pageNo="${no }">${no }</a></li>
					  </c:forEach>
					  <li><a href="#" class="pageBtn active" pageNo="${page.pageNo}">${page.pageNo}</a></li>
					  <c:forEach var="no" begin="${page.pageNo+1}" end="${page.totalPageCount}">
					  	<li><a href="#" class="pageBtn" pageNo="${no }">${no }</a></li>
					  </c:forEach>
					  <c:if test="${page.pageNo<page.totalPageCount}">
						 <li><a href="#"  class="pageBtn" pageNo="${page.pageNo+1}">&raquo;</a></li>
					  </c:if>
					</ul></c:if>
	      		</div> <!-- /widget-content -->
	      </div> <!-- /widget -->
     </div> <!-- /span12 -->
      		
      		
	
	
	
	
	</div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    <div class="span3">
                        <h4>软件定制</h4>
                        <ul>
                            <li><a href="javascript:;">企业网站</a></li>
                            <li><a href="javascript:;">ERP管理系统</a></li>
                            <li><a href="javascript:;">财务系统</a></li>
                            <li><a href="javascript:;">分销系统</a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>
                            App开发</h4>
                        <ul>
                            <li><a href="javascript:;">crm客户管理APP</a></li>
                            <li><a href="javascript:;">通讯录App</a></li>
                            <li><a href="javascript:;">娱乐App</a></li>
                            <li><a href="javascript:;">业务定制App</a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>
                            微信开发</h4>
                        <ul>
                            <li><a href="javascript:;">微信商城</a></li>
                            <li><a href="javascript:;">微信支付</a></li>
                            <li><a href="javascript:;">微信营销</a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>
                            电子商务</h4>
                        <ul>
                            <li><a href="http://sc.chinaz.com">商店系统</a></li>
                            <li><a href="http://sc.chinaz.com;">商城系统</a></li>
                            <li><a href="http://sc.chinaz.com;">众筹系统</a></li>
                            <li><a href="http://sc.chinaz.com;">特别定制商城</a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015-2020 <a href="http://main.wolf1688.com/index.html">雅狼工作</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
   
<!-- /footer --> 
<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="${ctx}/js/jquery-1.7.2.min.js"></script> 
<script src="${ctx}/js/excanvas.min.js"></script> 
<script src="${ctx}/js/chart.min.js" type="text/javascript"></script> 
<script src="${ctx}/js/bootstrap.js"></script>
<script language="javascript" type="text/javascript" src="${ctx}/js/full-calendar/fullcalendar.min.js"></script>
<script src="${ctx}/js/base.js"></script> 
<script src="${ctx}/js/pagination.js"></script> 
</body>
</html>
