package cn.com.base.sys.util;

import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

public class ModelAndViewUtil {
	public static String statusCode="statusCode";
	public static String message="message";
	public static String statusCode_success="200";
	public static String statusCode_error="300";
	public static String message_success="操作成功";
	public static String message_error="操作失败";
	public static String viewPath="json";
	public static String closeCurrent="closeCurrent";
	public static String closeCurrent_false="false";
	public static String closeCurrent_true="true";
	public static String refreshCurrent="refreshCurrent";
	public static String refreshCurrent_false="false";
	public static String refreshCurrent_true="true";
	public static String forward="forward";
	public static String forward_rul="";
	public static String forwardConfirm="forwardConfirm";
	public static String forwardConfirm_message="确定？";
	
	public static ModelAndView success(String viewPath,ModelMap modelMap){
		return new ModelAndView(viewPath,modelMap);
	}
	public static ModelAndView success(String viewPath){
		return new ModelAndView(viewPath);
	}

	public static String splitFlag="#";
	public static ModelAndView ajaxSuccess(){
		ModelMap modelMap=new ModelMap();
		modelMap.put(ModelAndViewUtil.message, ModelAndViewUtil.message_success);
		modelMap.put(ModelAndViewUtil.statusCode, ModelAndViewUtil.statusCode_success);
		modelMap.put(ModelAndViewUtil.forward, ModelAndViewUtil.forward_rul);
		modelMap.put(ModelAndViewUtil.closeCurrent, ModelAndViewUtil.closeCurrent_false);
		modelMap.put(ModelAndViewUtil.refreshCurrent, ModelAndViewUtil.refreshCurrent_true);
		modelMap.put(ModelAndViewUtil.forwardConfirm, ModelAndViewUtil.forwardConfirm_message);
		return new ModelAndView(viewPath,modelMap);
	}
	public static ModelAndView ajaxReturnWithCommand(String mess){
		String str[]=mess.split(splitFlag);
		ModelMap modelMap=new ModelMap();
		modelMap.put(ModelAndViewUtil.statusCode, str[0]);
		modelMap.put(ModelAndViewUtil.message, str[1]);
		modelMap.put(ModelAndViewUtil.forward, ModelAndViewUtil.forward_rul);
		modelMap.put(ModelAndViewUtil.closeCurrent, ModelAndViewUtil.closeCurrent_false);
		modelMap.put(ModelAndViewUtil.refreshCurrent, ModelAndViewUtil.refreshCurrent_true);
		modelMap.put(ModelAndViewUtil.forwardConfirm, ModelAndViewUtil.forwardConfirm_message);
		return new ModelAndView(viewPath,modelMap);
	}
	public static ModelAndView ajaxReturnWithCommand(String mess,String closeCurrent){
		String str[]=mess.split(splitFlag);
		ModelMap modelMap=new ModelMap();
		modelMap.put(ModelAndViewUtil.statusCode, str[0]);
		modelMap.put(ModelAndViewUtil.message, str[1]);
		modelMap.put(ModelAndViewUtil.forward, ModelAndViewUtil.forward_rul);
		modelMap.put(ModelAndViewUtil.closeCurrent, closeCurrent);
		modelMap.put(ModelAndViewUtil.refreshCurrent, ModelAndViewUtil.refreshCurrent_true);
		modelMap.put(ModelAndViewUtil.forwardConfirm, ModelAndViewUtil.forwardConfirm_message);
		return new ModelAndView(viewPath,modelMap);
	}
	public static ModelAndView ajaxSuccess(String message){
		ModelMap modelMap=new ModelMap();
		modelMap.put(ModelAndViewUtil.message, message);
		modelMap.put(ModelAndViewUtil.statusCode, ModelAndViewUtil.statusCode_success);
		modelMap.put(ModelAndViewUtil.forward, ModelAndViewUtil.forward_rul);
		modelMap.put(ModelAndViewUtil.closeCurrent, ModelAndViewUtil.closeCurrent_false);
		modelMap.put(ModelAndViewUtil.refreshCurrent, ModelAndViewUtil.refreshCurrent_true);
		modelMap.put(ModelAndViewUtil.forwardConfirm, ModelAndViewUtil.forwardConfirm_message);
		return new ModelAndView(ModelAndViewUtil.viewPath,modelMap);
	}
	public static ModelAndView ajaxSuccess(String message,String closeCurrent){
		ModelMap modelMap=new ModelMap();
		modelMap.put(ModelAndViewUtil.message, message);
		modelMap.put(ModelAndViewUtil.statusCode, ModelAndViewUtil.statusCode_success);
		modelMap.put(ModelAndViewUtil.closeCurrent, ModelAndViewUtil.closeCurrent_false);
		modelMap.put(ModelAndViewUtil.forward, ModelAndViewUtil.forward_rul);
		modelMap.put(ModelAndViewUtil.closeCurrent, closeCurrent);
		modelMap.put(ModelAndViewUtil.refreshCurrent, ModelAndViewUtil.refreshCurrent_true);
		modelMap.put(ModelAndViewUtil.forwardConfirm, ModelAndViewUtil.forwardConfirm_message);
		return new ModelAndView(ModelAndViewUtil.viewPath,modelMap);
	}
	
	public static ModelAndView ajaxSuccessAndForward(String successMessage,String forwardUrl){
		ModelMap modelMap=new ModelMap();
		modelMap.put(ModelAndViewUtil.message, successMessage);
		modelMap.put(ModelAndViewUtil.statusCode, ModelAndViewUtil.statusCode_success);
		modelMap.put(ModelAndViewUtil.closeCurrent, ModelAndViewUtil.closeCurrent_false);
		modelMap.put(ModelAndViewUtil.forward, forwardUrl);
		modelMap.put(ModelAndViewUtil.closeCurrent, closeCurrent);
		modelMap.put(ModelAndViewUtil.refreshCurrent, ModelAndViewUtil.refreshCurrent_true);
		modelMap.put(ModelAndViewUtil.forwardConfirm, ModelAndViewUtil.forwardConfirm_message);
		return new ModelAndView(ModelAndViewUtil.viewPath,modelMap);
	}
	
	public static ModelAndView ajaxSuccessAndForward(String successMessage,String forwardUrl,String forwardConfirmMessage){
		ModelMap modelMap=new ModelMap();
		modelMap.put(ModelAndViewUtil.message, successMessage);
		modelMap.put(ModelAndViewUtil.statusCode, ModelAndViewUtil.statusCode_success);
		modelMap.put(ModelAndViewUtil.closeCurrent, ModelAndViewUtil.closeCurrent_false);
		modelMap.put(ModelAndViewUtil.forward, forwardUrl);
		modelMap.put(ModelAndViewUtil.closeCurrent, closeCurrent);
		modelMap.put(ModelAndViewUtil.refreshCurrent, ModelAndViewUtil.refreshCurrent_true);
		modelMap.put(ModelAndViewUtil.forwardConfirm, forwardConfirmMessage);
		return new ModelAndView(ModelAndViewUtil.viewPath,modelMap);
	}
	
	public static ModelAndView ajaxError(){
		ModelMap modelMap=new ModelMap();
		modelMap.put(ModelAndViewUtil.message, ModelAndViewUtil.message_error);
		modelMap.put(ModelAndViewUtil.statusCode, ModelAndViewUtil.statusCode_error);
		modelMap.put(ModelAndViewUtil.forward, ModelAndViewUtil.forward_rul);
		modelMap.put(ModelAndViewUtil.closeCurrent, ModelAndViewUtil.closeCurrent_false);
		modelMap.put(ModelAndViewUtil.refreshCurrent, refreshCurrent_true);
		modelMap.put(ModelAndViewUtil.forwardConfirm, ModelAndViewUtil.forwardConfirm_message);
		return new ModelAndView(ModelAndViewUtil.viewPath,modelMap);
	}
	public static ModelAndView ajaxError(String message){
		ModelMap modelMap=new ModelMap();
		modelMap.put(ModelAndViewUtil.message, message);
		modelMap.put(ModelAndViewUtil.statusCode, ModelAndViewUtil.statusCode_error);
		modelMap.put(ModelAndViewUtil.forward, ModelAndViewUtil.forward_rul);
		modelMap.put(ModelAndViewUtil.closeCurrent, ModelAndViewUtil.closeCurrent_false);
		modelMap.put(ModelAndViewUtil.refreshCurrent, refreshCurrent_true);
		modelMap.put(ModelAndViewUtil.forwardConfirm, ModelAndViewUtil.forwardConfirm_message);
		return new ModelAndView(ModelAndViewUtil.viewPath,modelMap);
	}
	public static ModelAndView ajaxError(String message,String closeCurrent){
		ModelMap modelMap=new ModelMap();
		modelMap.put(ModelAndViewUtil.message, message);
		modelMap.put(ModelAndViewUtil.statusCode, ModelAndViewUtil.statusCode_error);
		modelMap.put(ModelAndViewUtil.forward, ModelAndViewUtil.forward_rul);
		modelMap.put(ModelAndViewUtil.closeCurrent, closeCurrent);
		modelMap.put(ModelAndViewUtil.refreshCurrent, refreshCurrent_true);
		modelMap.put(ModelAndViewUtil.forwardConfirm, ModelAndViewUtil.forwardConfirm_message);
		return new ModelAndView(ModelAndViewUtil.viewPath,modelMap);
	}
	public static ModelAndView ajaxErrorAndForward(String errorMessage,String forwardUrl){
		ModelMap modelMap=new ModelMap();
		modelMap.put(ModelAndViewUtil.message, errorMessage);
		modelMap.put(ModelAndViewUtil.statusCode, ModelAndViewUtil.statusCode_error);
		modelMap.put(ModelAndViewUtil.forward, forwardUrl);
		modelMap.put(ModelAndViewUtil.closeCurrent, closeCurrent_false);
		modelMap.put(ModelAndViewUtil.refreshCurrent, refreshCurrent_true);
		modelMap.put(ModelAndViewUtil.forwardConfirm, ModelAndViewUtil.forwardConfirm_message);
		return new ModelAndView(ModelAndViewUtil.viewPath,modelMap);
	}
	
	public static ModelAndView ajaxErrorAndForward(String errorMessage,String forwardUrl,String forwardConfirmMessage){
		ModelMap modelMap=new ModelMap();
		modelMap.put(ModelAndViewUtil.message, errorMessage);
		modelMap.put(ModelAndViewUtil.statusCode, ModelAndViewUtil.statusCode_error);
		modelMap.put(ModelAndViewUtil.forward, ModelAndViewUtil.forward_rul);
		modelMap.put(ModelAndViewUtil.closeCurrent, closeCurrent_false);
		modelMap.put(ModelAndViewUtil.refreshCurrent, refreshCurrent_true);
		modelMap.put(ModelAndViewUtil.forwardConfirm, forwardConfirmMessage);
		return new ModelAndView(ModelAndViewUtil.viewPath,modelMap);
	}

}
