package cn.com.base.sys.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

import cn.com.base.sys.model.BusinessCode;
import cn.com.base.sys.service.BusinessCodeService;
import cn.com.base.sys.service.CodeInputService;
import cn.com.base.sys.service.UserService;
import cn.com.base.sys.web.vo.BusinessCodeRS;

@Component("codeInputService")
public class CodeInputServiceImpl implements CodeInputService {

	@Autowired
	private BusinessCodeService businessCodeService;
	Logger logger = Logger.getLogger(CodeInputServiceImpl.class);

	@Override
	public void codeQuery(String codeType, String inputType, String param,
			ModelMap modelMap) {
		if (select.equals(inputType)) {
			initSelect(codeType, "", param, modelMap);
		}
	}

	private void initSelect(String codeType, String codeCode, String param,
			ModelMap modelMap) {
		BusinessCodeRS businessCodeRS = new BusinessCodeRS();
		businessCodeRS.setCodeType(codeType);
		logger.info("codeQuery begin");
		StringBuilder itemBuilder = new StringBuilder();
		List<BusinessCode> list = businessCodeService
				.findBusinessCodeList(businessCodeRS);
		if (list != null && list.size() > 0) {
			for (BusinessCode item : list) {
				if (item != null) {
					String selected = "";
					if (codeCode.equals(item.getCodeCode())) {
						selected = "selected";
					}
					;
					itemBuilder.append("<option value=\""
							+ item.getCodeCode() + "\" " + selected
							+ ">" + item.getCodeCName() + "</option>");
				}
			}
		}
		logger.info("jsonString:" + itemBuilder.toString());
		logger.info("codeQuery end");
		modelMap.put(select+"_"+codeType, itemBuilder.toString());
	}

	@Override
	public void codeQuery(String codeType, String codeCode, String inputType,
			String param, ModelMap modelMap) {
		if (select.equals(inputType)) {
			if(AREA.equals(codeType)){
				
			}else{
				initSelect(codeType, codeCode, param, modelMap);
			}
		}
	}
	@Autowired
	private  UserService userService;
	@Override
	public String translator(String codeType, String codeCode) {
		logger.info("翻译：codeType:"+codeType+";codeCode:"+codeCode+" start");
		String name="";
		if(codeCode==null||"".equals(codeCode)){
			return "";
		}
		logger.info("codeType:"+codeType+";codeCode"+codeCode+";name:"+name);
		return name;
	}
	@Override
	public List<BusinessCode> codeQuery(BusinessCodeRS businessCodeRS) {
		List<BusinessCode> list=new ArrayList<BusinessCode>();
		list=businessCodeService.findBusinessCodeList(businessCodeRS);
		
		return list;
		
	}
}
