<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/include.inc.jsp"%>
<!DOCTYPE html>
<html>
<head lang="en">
  <meta charset="UTF-8">
  <title>注册</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="format-detection" content="telephone=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="alternate icon" type="image/png" href="${ctx}/assets/i/favicon.png">
  <link rel="stylesheet" href="${ctx}/assets/css/amazeui.min.css"/>
  <script type="text/javascript"  src="${ctx}/assets/js/jquery.min.js"></script>
  <style>
    .header {
      text-align: center;
    }
    .header h1 {
      font-size: 200%;
      color: #333;
      margin-top: 30px;
    }
    .header p {
      font-size: 14px;
    }
  </style>
  <script type="text/javascript">
  $(function() {
  	$("#register_form").submit(function(){
  		var p1= $("#j_password").val;
  		var p2= $("#j_password1").val;
  		if(p1!=p2){
  			alert("俩次密码不一致");
  			return false;
  		}
  		//$("#login_ok").attr("disabled", true).val('登陆中..');
          return true;
  	});
  });
  </script>
</head>
<body>
<div class="header">
  <div class="am-g">
    <p>Integrated Development Environment<br/>代码编辑，代码生成，界面设计，调试，编译</p>
  </div>
  <hr />
</div>
<div class="am-g">
  <div class="am-u-lg-6 am-u-md-8 am-u-sm-centered">
   <form method="post" class="am-form" id="register_form" action="<c:url value='j_spring_security_check'/>">
	 <input type="hidden" value="${randomKey }" id="j_randomKey" />
      <label for="j_username">邮箱:</label>
      <input type="text" name="j_username" id="j_username" value="">
      <br>
      <label for="j_password">密码:</label>
      <input type="password" name="j_password" id="j_password" value="">
      <br>
      <label for="j_password1">确认密码:</label>
      <input type="password"  id="j_password1" value="">
      <br>
      <div class="am-cf">
        <input type="submit" name="" value="注 册" class="am-btn am-btn-primary am-btn-sm am-fl">
        <a href="${ctx}/login.jsp" class="am-btn am-btn-default am-btn-sm am-fr">已有账号 </a>
      </div>
    </form>
    <hr>
    <p>© 2014 AllMobilize, Inc. Licensed under MIT license.</p>
  </div>
</div>
</body>
</html>