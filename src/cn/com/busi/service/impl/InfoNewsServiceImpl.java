package cn.com.busi.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.base.sys.common.BaseJpaDao;
import cn.com.base.sys.common.Page;
import cn.com.base.sys.common.Query;
import cn.com.base.sys.util.Strings;
import cn.com.busi.model.InfoNews;
import cn.com.busi.service.InfoNewsService;
import cn.com.busi.web.vo.InfoNewsRS;

/**
 * 新闻通知的业务逻辑类
 * 
 * @author 谢林
 */
@Service(value = "infoNewsService")
public class InfoNewsServiceImpl implements InfoNewsService {
	@Autowired
	private BaseJpaDao baseJpaDao;
	static Logger log = Logger.getLogger(InfoNewsServiceImpl.class);
	@Override
	/**
	 * jpa分页
	 */
	public Page<InfoNews> findInfoNewsByPage(int pageNo, int pageSize,
			InfoNewsRS infoNewsRS) {
		log.info("正在执行  getInfoNewsByPage()");
		Query query = baseJpaDao.createQuery(InfoNews.class);
		this.initQuery(query, infoNewsRS);
		Page page = baseJpaDao.queryPage(query, pageNo, pageSize);
		log.info("执行 getInfoNewsByPage()完毕，正在返回");
		return page;
	}

	/**
	 * 初始化查询条件
	 */
	private void initQuery(Query query, InfoNewsRS infoNewsRS) {
		log.info("正在执行  initQuery()");
		if (infoNewsRS != null) {
			if (!Strings.isNullOrEmpty(infoNewsRS.getCreateDate())) {
				query.eq("createDate", infoNewsRS.getCreateDate());
			}
			if (!Strings.isNullOrEmpty(infoNewsRS.getValidStatus())) {
				query.eq("validStatus", infoNewsRS.getValidStatus());
			}
			if (!Strings.isNullOrEmpty(infoNewsRS.getCreateUser())) {
				query.eq("createUser", infoNewsRS.getCreateUser());
			}
			if (!Strings.isNullOrEmpty(infoNewsRS.getUpdateDate())) {
				query.eq("updateDate", infoNewsRS.getUpdateDate());
			}
			if (!Strings.isNullOrEmpty(infoNewsRS.getUpdateUser())) {
				query.eq("updateUser", infoNewsRS.getUpdateUser());
			}
			if (!Strings.isNullOrEmpty(infoNewsRS.getId())) {
				query.eq("id", infoNewsRS.getId());
			}
			if (!Strings.isNullOrEmpty(infoNewsRS.getTitle())) {
				query.like("title", infoNewsRS.getTitle());
			}
			if (!Strings.isNullOrEmpty(infoNewsRS.getTitleDesc())) {
				query.eq("titleDesc", infoNewsRS.getTitleDesc());
			}
			if (!Strings.isNullOrEmpty(infoNewsRS.getContents())) {
				query.eq("contents", infoNewsRS.getContents());
			}
			if (!Strings.isNullOrEmpty(infoNewsRS.getStatus())) {
				query.eq("status", infoNewsRS.getStatus());
			}
			if (!Strings.isNullOrEmpty(infoNewsRS.getType())) {
				query.eq("type", infoNewsRS.getType());
			}
		}

		log.info("执行 initQuery完毕，正在返回");
	}

	@Override
	/**
	 * 修改操作
	 */
	public InfoNews infoNewsUpdate(InfoNews infoNews) {
		log.info("正在执行  infoNewsUpdate(InfoNews)");
		baseJpaDao.update(infoNews);
		log.info("执行 infoNewsUpdate完毕，正在返回");
		return infoNews;
	}

	@Override
	/**
	 * 新增操作
	 */
	public InfoNews infoNewsSave(InfoNews infoNews) {
		log.info("正在执行  infoNewsSave(infoNews)");
		baseJpaDao.save(infoNews);
		log.info("执行 infoNewsSave完毕，正在返回");
		return infoNews;
	}

	@Override
	/**
	 * 删除操作
	 */
	public void infoNewsDelete(InfoNews infoNews) {
		log.info("正在执行  infoNewsDelete(InfoNews)");
		// baseJpaDao.delete(infoNews);
		infoNewsDelete(infoNews.getId());
		log.info("执行 Delete()完毕，正在返回");
	}

	@Override
	/**
	 * 通过主键删除操作
	 */
	public void infoNewsDelete(Long id) {
		List<Long> list = new ArrayList<Long>();
		list.add(id);
		baseJpaDao.delete(InfoNews.class, list);
	}

	@Override
	/**
	 * 通过对象查找一个记录，对象中主键必须有
	 */
	public InfoNews infoNewsGet(InfoNews infoNews) {
		log.info("正在执行  infoNewsGet(InfoNews)");
		infoNews = infoNewsGet(infoNews.getId());
		log.info("执行 infoNewsGet 完毕，正在返回");
		return infoNews;
	}

	@Override
	/**
	 * 通过主键查找一个记录
	 */
	public InfoNews infoNewsGet(Long id) {
		log.info("正在执行  infoNewsGet(Long)");
		InfoNews infoNews = baseJpaDao.get(InfoNews.class, id);
		log.info("执行 infoNewsGet完毕，正在返回");
		return infoNews;
	}

}
