package cn.com.busi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@TableGenerator(name = "SYSSEQUENCE", table = "SYSSEQUENCE", pkColumnName = "GenName", valueColumnName = "GenValue", pkColumnValue = "InfoProductImage_PK", allocationSize = 1)
@Table(name = "InfoProductImage")
public class InfoProductImage {
	private Long id;
	private Long productId;
	private Long imagesId;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "SYSSEQUENCE")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name = "productId")
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	@Column(name = "imagesId")
	public Long getImagesId() {
		return imagesId;
	}
	public void setImagesId(Long imagesId) {
		this.imagesId = imagesId;
	}
	

}
