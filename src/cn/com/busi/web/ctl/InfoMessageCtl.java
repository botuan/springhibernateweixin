package cn.com.busi.web.ctl;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.com.base.sys.common.Page;
import cn.com.base.sys.common.Result;
import cn.com.base.sys.security.SessionManager;
import cn.com.busi.model.InfoMessage;
import cn.com.busi.service.InfoMessageService;
import cn.com.busi.web.vo.InfoMessageRS;
import cn.com.busi.web.vo.InfoMessageReq;
/**
 *留言表 的控制类
 */
@Controller
@RequestMapping("/admin")
public class InfoMessageCtl extends SessionManager{
	@Autowired
	/**
	 *InfoMessage逻辑操作接口
	 */
	private InfoMessageService infoMessageService;
	/**
	 *InfoMessage分页查询
	 */
	@RequestMapping("/infoMessageQuery")
	public ModelAndView  infoMessageQuery(@ModelAttribute InfoMessageReq infoMessageReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		InfoMessageRS infoMessageRS=infoMessageReq.getInfoMessageRS();
		modelMap.put("pageSize",infoMessageReq.getPageSize());
		modelMap.put("pageNo", infoMessageReq.getPageNo());
		modelMap.put("ctx", request.getContextPath());
		modelMap.put("infoMessageRS", infoMessageRS);
		Page<InfoMessage> page=infoMessageService.findInfoMessageByPage(infoMessageReq.getPageNo(), infoMessageReq.getPageSize(),infoMessageRS);//.getMenuByAuthoritieId(principal.getName());
		modelMap.put("page",page);
		return new ModelAndView("admin/message/message_list",modelMap);
	}
	/**
	 *InfoMessage打开新增界面
	 */
	@RequestMapping("/prepareInfoMessageAdd")
	public ModelAndView  prepareInfoMessageAdd(@ModelAttribute InfoMessageReq infoMessageReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		return new ModelAndView("admin/message/message_create",modelMap);
	}
	/**
	 *InfoMessage新增保存
	 */
	@RequestMapping("/infoMessageAdd")
	public  @ResponseBody Result  infoMessageAdd(@ModelAttribute InfoMessageReq infoMessageReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		InfoMessage infoMessage=infoMessageReq.getInfoMessage();
		infoMessageService.infoMessageSave(infoMessage);Result r=new Result();
		r.setCode("0");
		r.setMessage("上传成功");
		return r;
	}
	/**
	 *InfoMessage打开修改界面
	 */
	@RequestMapping("/prepareInfoMessageEdit")
	public ModelAndView  prepareInfoMessageEdit(@ModelAttribute InfoMessageReq infoMessageReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		InfoMessage infoMessage=infoMessageReq.getInfoMessage();
		infoMessage=infoMessageService.infoMessageGet(infoMessage.getId());
		modelMap.put("infoMessage", infoMessage);
		return new ModelAndView("admin/message/message_edit",modelMap);
	}
	/**
	 *InfoMessage修改保存
	 */
	@RequestMapping("/infoMessageEdit")
	public  @ResponseBody Result  infoMessageEdit(@ModelAttribute InfoMessageReq infoMessageReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		InfoMessage infoMessage=infoMessageReq.getInfoMessage();
		infoMessageService.infoMessageUpdate(infoMessage);Result r=new Result();
		r.setCode("0");
		r.setMessage("上传成功");
		return r;
	}
	/**
	 *InfoMessage删除操作
	 */
	@RequestMapping("/infoMessageDelete")
	public  @ResponseBody Result  infoMessageDelete(@ModelAttribute InfoMessageReq infoMessageReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		infoMessageService.infoMessageDelete(infoMessageReq.getId());
		Result r=new Result();
		r.setCode("0");
		r.setMessage("上传成功");
		return r;
	}
}
