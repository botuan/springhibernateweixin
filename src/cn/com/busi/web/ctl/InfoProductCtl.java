package cn.com.busi.web.ctl;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.com.base.sys.common.Page;
import cn.com.base.sys.common.Result;
import cn.com.base.sys.security.SessionManager;
import cn.com.busi.model.InfoProduct;
import cn.com.busi.service.InfoProductService;
import cn.com.busi.service.InfoProductTypeService;
import cn.com.busi.web.vo.InfoProductRS;
import cn.com.busi.web.vo.InfoProductReq;
/**
 *InfoProduct 的控制类
 */
@Controller
@RequestMapping("/admin")
public class InfoProductCtl  extends SessionManager{
	@Autowired
	/**
	 *InfoProduct逻辑操作接口
	 */
	private InfoProductService infoProductService;
	/**
	 *InfoProduct分页查询
	 */
	@RequestMapping("/infoProductQuery")
	public ModelAndView  infoProductQuery(@ModelAttribute InfoProductReq infoProductReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		InfoProductRS infoProductRS=infoProductReq.getInfoProductRS();
		modelMap.put("pageSize",infoProductReq.getPageSize());
		modelMap.put("pageNo", infoProductReq.getPageNo());
		modelMap.put("ctx", request.getContextPath());
		modelMap.put("infoProductRS", infoProductRS);
		Page<InfoProduct> page=infoProductService.findInfoProductByPage(infoProductReq.getPageNo(), infoProductReq.getPageSize(),infoProductRS);//.getMenuByAuthoritieId(principal.getName());
		modelMap.put("page",page);
		return new ModelAndView("admin/product/product_list",modelMap);
	}
	@Autowired
	private InfoProductTypeService infoProductTypeService;
	/**
	 *InfoProduct打开新增界面
	 */
	@RequestMapping("/prepareInfoProductAdd")
	public ModelAndView  prepareInfoProductAdd(@ModelAttribute InfoProductReq infoProductReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		Page page=infoProductTypeService.findInfoProductTypeByPage(0, Integer.MAX_VALUE, null);
		if(page!=null){
			modelMap.put("infoProductTypeList", page.getResult());
		}
		return new ModelAndView("admin/product/product_create",modelMap);
	}
	/**
	 *InfoProduct新增保存
	 */
	@RequestMapping("/infoProductAdd")
	public @ResponseBody Result   infoProductAdd(@ModelAttribute InfoProductReq infoProductReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		InfoProduct infoProduct=infoProductReq.getInfoProduct();
		infoProduct.setCreateUser("test");
		infoProduct.setUpdateUser("test");
		infoProductService.infoProductSave(infoProduct);
		Result r=new Result();
		r.setCode("0");
		r.setMessage("操作成功");
		return r;
	}
	/**
	 *InfoProduct打开修改界面
	 */
	@RequestMapping("/prepareInfoProductEdit")
	public ModelAndView  prepareInfoProductEdit(@ModelAttribute InfoProductReq infoProductReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		InfoProduct infoProduct=infoProductReq.getInfoProduct();
		infoProduct=infoProductService.infoProductGet(infoProduct);
		
		modelMap.put("infoProduct", infoProduct);
		return new ModelAndView("admin/product/product_edit",modelMap);
	}
	
	@RequestMapping("/prepareInfoProductUpLoad")
	public ModelAndView  prepareInfoProductUpLoad(@ModelAttribute InfoProductReq infoProductReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		InfoProduct infoProduct=infoProductReq.getInfoProduct();
		infoProduct=infoProductService.infoProductGet(infoProduct);
		modelMap.put("infoProduct", infoProduct);
		return new ModelAndView("admin/product/product_upload",modelMap);
	}
	/**
	 *InfoProduct修改保存
	 */
	@RequestMapping("/infoProductEdit")
	public @ResponseBody Result   infoProductEdit(@ModelAttribute InfoProductReq infoProductReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		
		InfoProduct infoProduct=infoProductReq.getInfoProduct();
		infoProduct.setUpdateUser("test");
		infoProduct.setUpdateDate(new Date());
		infoProductService.infoProductUpdate(infoProduct);
		Result r=new Result();
		r.setCode("0");
		r.setMessage("操作成功");
		return r;
	}
	/**
	 *InfoProduct删除操作
	 */
	@RequestMapping("/infoProductDelete")
	public @ResponseBody Result  infoProductDelete(@ModelAttribute InfoProductReq infoProductReq, HttpServletRequest request,HttpServletResponse response, ModelMap modelMap){
		super.adminLoginValidate(request);
		InfoProduct infoProduct=infoProductReq.getInfoProduct();
		infoProductService.infoProductDelete(infoProduct);	
		Result r=new Result();
		r.setCode("0");
		r.setMessage("删除成功");
		return r;
	}
}
