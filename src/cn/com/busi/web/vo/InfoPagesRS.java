package cn.com.busi.web.vo;

import java.util.Date;

/**
 * ${modelFieldBean.desc}
 * @author 谢林
 */
public class InfoPagesRS implements java.io.Serializable {

	private String pageKeyWords;
	private String pageDescription;
	
		public String getPageKeyWords() {
		return pageKeyWords;
	}
	public void setPageKeyWords(String pageKeyWords) {
		this.pageKeyWords = pageKeyWords;
	}
	public String getPageDescription() {
		return pageDescription;
	}
	public void setPageDescription(String pageDescription) {
		this.pageDescription = pageDescription;
	}


		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		/**
	 * 返回 主键
	 */
	private Long id;
	/**
	 * 返回 主键
	 * @return
	 */
	public Long getId (){
	 return id;
	}
	/**
	 * 设置 主键
	 * @param id
	 */
	public void setId (Long id){
	 this.id=id;
	}
	
	
	private String name;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * 返回 类型
	 */
	private String type;
	/**
	 * 返回 类型
	 * @return
	 */
	public String getType (){
	 return type;
	}
	/**
	 * 设置 类型
	 * @param type
	 */
	public void setType (String type){
	 this.type=type;
	}
	/**
	 * 返回 内容页
	 */
	private String content;
	/**
	 * 返回 内容页
	 * @return
	 */
	public String getContent (){
	 return content;
	}
	/**
	 * 设置 内容页
	 * @param content
	 */
	public void setContent (String content){
	 this.content=content;
	}
	/**
	 * 返回 创建时间
	 */
	private Date createDate;
	/**
	 * 返回 创建时间
	 * @return
	 */
	public Date getCreateDate (){
	 return createDate;
	}
	/**
	 * 设置 创建时间
	 * @param createDate
	 */
	public void setCreateDate (Date createDate){
	 this.createDate=createDate;
	}
	/**
	 * 返回 创建人
	 */
	private String createUser;
	/**
	 * 返回 创建人
	 * @return
	 */
	public String getCreateUser (){
	 return createUser;
	}
	/**
	 * 设置 创建人
	 * @param createUser
	 */
	public void setCreateUser (String createUser){
	 this.createUser=createUser;
	}
	
}