package cn.com.base.sys.service;

import java.util.List;

import org.springframework.stereotype.Service;

import cn.com.base.sys.common.Page;
import cn.com.base.sys.model.BusinessCode;
import cn.com.base.sys.web.vo.BusinessCodeRS;

/**
 * 代码字典的业务逻辑接口
 * 
 * @author xielin
 */
public interface BusinessCodeService {
	/**
	 * 分页查询
	 */
	public Page<BusinessCodeRS> findBusinessCodeByPage(int pageNo,
			int pageSize, BusinessCodeRS businessCodeRS);

	/**
	 * 查询列表
	 * 
	 * @param businessCodeRS
	 * @return
	 */
	public List<BusinessCode> findBusinessCodeList(BusinessCodeRS businessCodeRS);

	/**
	 * 修改数据
	 */
	public BusinessCode businessCodeUpdate(BusinessCode businessCode);

	/**
	 * 新增数据
	 */
	public BusinessCode businessCodeSave(BusinessCode businessCode);

	/**
	 * 删除数据
	 */
	public void businessCodeDelete(BusinessCode businessCode);

	/**
	 * 通过主键删除数据
	 */
	// public void businessCodeDelete(BusinessCodeId businessCodeId);
	public void businessCodeDelete(Long businessCodeId);

	/**
	 * 通过对象查询数据
	 */
	public BusinessCode businessCodeGet(BusinessCode businessCode);

	/**
	 * 通过主键查询数据
	 */
	// public BusinessCode businessCodeGet(BusinessCodeId businessCodeId);
	public BusinessCode businessCodeGet(Long businessCodeId);
	

	public String translate(String codeType,String codeCode);

}
