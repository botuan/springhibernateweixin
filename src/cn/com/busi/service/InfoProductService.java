package cn.com.busi.service;

import org.springframework.stereotype.Service;

import cn.com.base.sys.common.Page;
import cn.com.busi.model.InfoProduct;
import cn.com.busi.web.vo.InfoProductRS;

/**
 * 产品的业务逻辑接口
 * 
 * @author 谢林
 */
public interface InfoProductService {
	/**
	 * 分页查询
	 */
	public Page<InfoProduct> findInfoProductByPage(int pageNo, int pageSize,
			InfoProductRS infoProductRS);

	/**
	 * 修改数据
	 */
	public InfoProduct infoProductUpdate(InfoProduct infoProduct);

	/**
	 * 新增数据
	 */
	public InfoProduct infoProductSave(InfoProduct infoProduct);

	/**
	 * 删除数据
	 */
	public void infoProductDelete(InfoProduct infoProduct);

	/**
	 * 通过主键删除数据
	 */

	public InfoProduct infoProductGet(Long id);

	/**
	 * 通过对象查询数据
	 */
	public InfoProduct infoProductGet(InfoProduct infoProduct);
	/**
	 * 通过主键查询数据
	 */
}
