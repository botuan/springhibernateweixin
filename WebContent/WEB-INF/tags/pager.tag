<%@ tag pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<c:set var="BASE" value="${pageContext.request.contextPath}"/>

<%@ attribute name="id" required="true" type="java.lang.String" %>
<%@ attribute name="pager" required="true" type="cn.com.base.sys.common.Page" %>

<c:set var="pageNumber" value="${pager.pageNo}"/>
<c:set var="pageSize" value="${pager.pageSize}"/>
<c:set var="totalRecord" value="${pager.count}"/>
<c:set var="totalPage" value="${pager.totalPageCount}"/>

<div class="css-row">
    <div class="css-right">
        <div id="${id}">
            <span>页面编号:</span>
            <input type="text" value="${pageNumber}" class="css-width-25 css-text-center ext-pager-pn">
            <span>/</span>
            <span class="ext-pager-tp">${totalPage}</span>
            <span class="css-blank-10"></span>
            <span>每页条数:</span>
            <input type="text" value="${pageSize}" class="css-width-25 css-text-center ext-pager-ps">
            <span class="css-blank-10"></span>
            <span>总条数:</span>
            <span>${totalRecord}</span>
            <span class="css-blank-10"></span>
            <div class="css-button-group ext-pager-button">
                <c:choose>
                    <c:when test="${pageNumber > 1 && pageNumber <= totalPage}">
                        <button type="button" title="首页" data-pn="1">|&lt;</button>
                        <button type="button" title="上页" data-pn="${pageNumber - 1}">&lt;</button>
                    </c:when>
                    <c:otherwise>
                        <button type="button" title="第一页" disabled>|&lt;</button>
                        <button type="button" title="上页" disabled>&lt;</button>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${pageNumber < totalPage}">
                        <button type="button" title="下一页" data-pn="${pageNumber + 1}">&gt;</button>
                        <button type="button" title="最后一页" data-pn="${totalPage}">&gt;|</button>
                    </c:when>
                    <c:otherwise>
                        <button type="button" title="下一页" disabled>&gt;</button>
                        <button type="button" title="末页" disabled>&gt;|</button>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>