package cn.com.base.sys.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;












import cn.com.base.sys.common.BaseJpaDao;
import cn.com.base.sys.common.Page;
import cn.com.base.sys.common.Query;
import cn.com.base.sys.model.SysUsers;
import cn.com.base.sys.service.UserService;
import cn.com.base.sys.util.Strings;
import cn.com.base.sys.web.vo.UsersRS;

@Service(value="userService")
public class UserServiceImpl  implements UserService {
	
	private static Logger  log= Logger.getLogger(UserServiceImpl.class);
	
	@Autowired
	BaseJpaDao baseJpaDao;

	@Override
	@Cacheable(value="serviceCache",key="#userCode + '_UserCode'")  
	public List<SysUsers> findUser(String userCode) {
		Query query=baseJpaDao.createQuery(SysUsers.class);
		query.eq("userCode",userCode);
		log.info("开始进行查询。。。。。");
		return baseJpaDao.query(query);
	}

	@Override
	@CacheEvict(value="serviceCache",key="#userCode + '_UserCode'")  
//	@CacheEvict(value="UserCache",allEntries=true)  
	public boolean deleteUser(String userCode) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public SysUsers findByUserAccount(String userName) {
		return  baseJpaDao.get(SysUsers.class,userName);
	}

	@Override
	public Page<SysUsers> getUserByPage(String userId,int pageNo,
			int pageSize, UsersRS usersRS) {
		StringBuilder sql=new StringBuilder();
		sql.append("SELECT * FROM sysusers WHERE  1=1 ");
		List<Object> params=new ArrayList<Object>();
		initCondition(sql,params,usersRS);
		sql.append("  ORDER BY subSystem ASC,userDept ASC,userDuty ASC");
		log.warn(sql);
		Page<SysUsers> page=baseJpaDao.queryPageBySql(sql.toString(),SysUsers.class,pageNo, pageSize,params.toArray(new String[0]));
		return page;
	}
	
	private void initCondition(StringBuilder condition,List<Object> params, UsersRS usersRS){
		
		if(params==null){
			params=new ArrayList<Object>();
		}
		if(condition==null){
			condition=new StringBuilder();
		}
		if(!Strings.isNullOrEmpty(usersRS.getEnabled())){
			condition.append(" AND enabled=?  ");
			params.add(usersRS.getEnabled());
		}
		if(!Strings.isNullOrEmpty(usersRS.getIssys())){
			condition.append(" AND issys=?  ");
			params.add(usersRS.getIssys());
		}
		if(!Strings.isNullOrEmpty(usersRS.getSubSystem())){
			condition.append(" AND subSystem=?  ");
			params.add(usersRS.getSubSystem());
		}
		if(!Strings.isNullOrEmpty(usersRS.getUserAccount())){
			condition.append(" AND userAccount=?  ");
			params.add(usersRS.getUserAccount());
		}
		if(!Strings.isNullOrEmpty(usersRS.getUserDept())){
			condition.append(" AND userDept=?  ");
			params.add(usersRS.getUserDept());
		}
		if(!Strings.isNullOrEmpty(usersRS.getUserDuty())){
			condition.append(" AND userDuty=?  ");
			params.add(usersRS.getUserDuty());
		}
		if(!Strings.isNullOrEmpty(usersRS.getUserName())){
			condition.append(" AND userName like ?  ");
			params.add(usersRS.getUserName());
		}
		if(!Strings.isNullOrEmpty(usersRS.getUserDesc())){
			condition.append(" AND userDesc like ?  ");
			params.add(usersRS.getUserDesc());
		}
	}

	@Override
	public SysUsers userUpdate(SysUsers sysUsers) {
		baseJpaDao.update(sysUsers);
		return sysUsers;
	}

	@Override
	public SysUsers userSave(SysUsers sysUsers) {
		baseJpaDao.save(sysUsers);
		return sysUsers;
	}

	@Override
	public void userDelete(SysUsers sysUsers) {
		List<String> ids=new ArrayList<String>();
		ids.add(sysUsers.getUserId());
		baseJpaDao.delete(SysUsers.class,ids);
		
	}

	@Override
	public SysUsers findByUserId(String userId) {
		return  baseJpaDao.get(SysUsers.class,userId);
	}

	@Override
	public Page<SysUsers> findUserByPage(String userId, int pageNo,
			int pageSize, UsersRS usersRS) {
		Query query=baseJpaDao.createQuery(SysUsers.class);
		this.initQuery(query, usersRS);
		Page page=baseJpaDao.queryPage(query, pageNo, pageSize);
		return page;
	}
	private void initQuery(Query query,UsersRS usersRS){
		if(!Strings.isNullOrEmpty(usersRS.getEnabled())){
			query.eq("enabled", usersRS.getEnabled());
		}
		if(!Strings.isNullOrEmpty(usersRS.getIssys())){
			query.eq("issys", usersRS.getIssys());
		}
		if(!Strings.isNullOrEmpty(usersRS.getSubSystem())){
			query.eq("subSystem", usersRS.getSubSystem());
		}
		if(!Strings.isNullOrEmpty(usersRS.getUserAccount())){
			query.eq("userAccount", usersRS.getEnabled());
		}
		if(!Strings.isNullOrEmpty(usersRS.getUserDept())){
			query.eq("userDept", usersRS.getEnabled());
		}
		if(!Strings.isNullOrEmpty(usersRS.getUserDuty())){
			query.eq("userDuty", usersRS.getUserDuty());
		}
		if(!Strings.isNullOrEmpty(usersRS.getUserName())){
			query.eq("userName", usersRS.getUserName());
		}
		if(!Strings.isNullOrEmpty(usersRS.getUserDesc())){
			query.eq("userDesc", usersRS.getUserDesc());
		}
	}

}
